/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MYCAMERA_H
#define MYCAMERA_H
#include <QObject>
#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QPixmap>


class MyCamera : public QObject
{
    Q_OBJECT
public:
    explicit MyCamera(QObject *parent = nullptr);
    QList<QString> getAvailableCameras(void);
    QList<QSize> getResolutions();
    bool setResolution(QSize size);
    QString getDeviceName(void);
    QSize resolution();
    int getNImages(void);
    const int MAXNIMAGES=8;
private:
    QString deviceName;
    QCamera * camera=nullptr;
    QCameraImageCapture * imageCapture=nullptr;
    bool pictureRequested=false;
    int nImages=1; ///< number of images to be averaged
    QList<QImage> images; ///< temporary store for images

signals:
    void imageCaptured(QPixmap image);
    void captureError(QString errorMessage);

public slots:
    void setCamera(QString deviceName, bool enabled=true);
    void setNImages(int n);
    void capture(void);
private slots:
    void cameraIsReady(bool ready);
    void getImage(int id, const QImage &img);
};

#endif // MYCAMERA_H
