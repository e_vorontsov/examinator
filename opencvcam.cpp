#include "opencvcam.h"
#include <QDebug>
#include <QSettings>
#include <QApplication>
#include <QElapsedTimer>

OpenCVCam::OpenCVCam(QObject *parent) : QObject(parent)
{
    QSettings settings(QSettings::IniFormat,QSettings::UserScope, qApp->organizationName(),qApp->applicationName());
    deviceId=settings.value("ocvcamera",0).toInt();
    if(settings.contains("ocvcameraresolution")){
        resolution=settings.value("ocvcameraresolution").toSize();
    }
    nImages=settings.value("ocvnimages",1).toInt();
    nIgnore=settings.value("ocvnignores",1).toInt();
    if(cap.isOpened()){
        qDebug()<<"capture opened by default?!?";
        cap.release();
    }

}

QList<int> OpenCVCam::getAvailableCameras(int cameraBackend)
{
    QList<int> lst;
    if(cap.isOpened())
        cap.release();
    for(int idx=0; idx<100; idx++){
        if(cameraBackend>0)
            cap.open(idx,cameraBackend);
        else
            cap.open(idx);
        if(cap.isOpened()){
            lst.append(idx);
            qDebug()<<"found camera"<<idx;
            cap.release();
        } else
            break;
    }
    return lst;

}

QList<QSize> OpenCVCam::getResolutions()
{
    QList<QSize> lst;
    QList<QSize> commonResolutions({{160,120},{320,240},{639,360},{640,480},{640,640},{1280,720},{1284,720},{1440,720},
                                    {1440,1080},{1920,1080},{1920,1280},{1920,1440},{2592,1458},
                                    {2592,1458},{2592,1728},{2592,1944},{2888,1440},{3264,1836},
                                    {3264,2176},{3264,2448},{4128,3096},{4128,2322},{5256,2952},{5256,3936},{10000,10000}});
    /*
    640x360	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps
    640x480	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps
    640x640	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps	30 fps
    1280x720	30 fps & 60 fps	12 fps	30 fps & 60 fps	12 fps	30 fps	30 fps	30 fps	30 fps
    1284x720	100 fps	NS	100 fps	NS	NS	NS	NS	NS
    1440x720 *	30 fps	NS	30 fps	NS	30 fps	30 fps	30 fps	30 fps
    1440x1080	30 fps	NS	30 fps	NS	30 fps	30 fps	30 fps	30 fps
    1920x1080	30 fps	NS	30 fps	NS	30 fps	30 fps	30 fps	30 fps
    1920x1280	30 fps	NS	30 fps	NS	30 fps	30 fps	30 fps	30 fps
    1920x1440	30 fps	NS	30 fps	NS	30 fps	30 fps	30 fps	30 fps
    2592x1458	25 fps	NS	25 fps	NS	25 fps	7 fps	25 fps	7 fps
    2592x1728	21 fps	NS	21 fps	NS	21 fps	7 fps	21 fps	7 fps
    2592x1944	19 fps	NS	19 fps	NS	19 fps	7 fps	19 fps	7 fps
    2880x1440 *	18 fps	NS	18 fps	NS	18 fps	7 fps	18 fps	7 fps
    3264x1836	16 fps	NS	16 fps	NS	16 fps	NS	16 fps	NS
    3264x2176	13 fps	NS	13 fps	NS	13 fps	NS	13 fps	NS
    3264x2448	12 fps	NS	12 fps	NS	12 fps	7 fps	12 fps	7 fps
    4128x2322	NS	NS	9 fps	NS	NS	NS	10 fps	NS
    4128x3096	NS	NS	7 fps	NS	NS	NS	7 fps	7 fps
    5256x2952	6 fps	NS	NS	NS	6 fps	NS	NS	NS
    5256x3936	4 fps	NS	NS	NS	4 fps	4 fps	NS	NS
    */
    if(cap.isOpened()){
        cap.release();
    }

    cap.open(deviceId);
    if(!cap.isOpened())
        return lst;
    foreach (QSize s, commonResolutions) {
        //try to set resolution to s and check if it worked
        /// @todo
        cap.set(cv::CAP_PROP_FRAME_WIDTH, s.width());
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, s.height());
        QSize newSize;
        newSize.setWidth(cap.get(cv::CAP_PROP_FRAME_WIDTH));
        newSize.setHeight(cap.get(cv::CAP_PROP_FRAME_HEIGHT));
        if(!lst.contains(newSize)){
            lst.append(newSize);
        }
        qDebug()<<s<<"->"<<newSize;
        int ex = static_cast<int>(cap.get(cv::CAP_PROP_FOURCC));
        // Transform from int to char via Bitwise operators
        char EXT[] = {(char)(ex & 0XFF),(char)((ex & 0XFF00) >> 8),(char)((ex & 0XFF0000) >> 16),(char)((ex & 0XFF000000) >> 24),0};

        qDebug()<<EXT<<
                  cap.get(cv::CAP_PROP_FPS);
    }
    cap.release();
    return lst;
}

void OpenCVCam::capture()
{
    if(!cap.isOpened())
        reopen();
    if(!cap.isOpened()){
        emit captureError("device is not open");
        return;
    }
    qint64 minCaptureTime=50;//50 ms capture time default value
    double fps=cap.get(cv::CAP_PROP_FPS);
    if(fps>0){
        minCaptureTime=1000/(2*fps);
    }
    qDebug()<<"FPS"<<cap.get(cv::CAP_PROP_FPS);
    cv::Mat m;
    for(int i=0; i<nIgnore; i++){
        cap.grab();
    }
    QElapsedTimer timer;
    int count=0;
    qint64 e=0;
    for(timer.start(); ((e=timer.restart())<minCaptureTime)&&(count<5);count++){
        cap>>m;
    }
    qDebug()<<"elapsed"<<e<<minCaptureTime;
    if(m.empty()){
        qDebug()<<"capture error"<<cap.isOpened();
    } else
    if(nImages>1){
        cv::Mat sum;
        m.convertTo(sum,CV_16UC3);
        for(int i=1; i<nImages; i++){
            cap>>m;
            cv::add(sum,m,sum,cv::noArray(),CV_16UC3);
            qDebug()<<"capture"<<i<<sum.at<cv::Vec<uint16_t, 3>>(0,0)[0];
        }
        sum /= nImages;
        sum.convertTo(m, CV_8UC3);
    }
    if(m.type()==CV_8UC3){
        //mat2QImage
        const uchar *qImageBuffer = (const uchar*)m.data;
        // Create QImage with same dimensions as input Mat
        QImage img(qImageBuffer, m.cols, m.rows, m.step, QImage::Format_RGB888);
        emit imageCaptured( img.rgbSwapped() );
        return;
    }
    emit captureError("unexpected image type");
    return;


}

void OpenCVCam::setCamera(QString deviceName, bool enabled)
{
    this->deviceName=deviceName;
    if(enabled){
        cap.open(deviceName.toLatin1().constData());
    } else {
        if(cap.isOpened())
            cap.release();
    }
}

void OpenCVCam::setCamera(int deviceId, bool enabled)
{
    deviceName.clear();
    qDebug()<<"setCamera"<<deviceId<<enabled<<cap.isOpened();
    this->deviceId=deviceId;
    if(enabled){
        if(cap.isOpened())
            cap.release();
        reopen();
    } else {
        if(cap.isOpened())
            cap.release();
    }
}

int OpenCVCam::getDeviceId() const
{
    return deviceId;
}

int OpenCVCam::getNIgnore() const
{
    return nIgnore;
}

void OpenCVCam::setNIgnore(int value)
{
    nIgnore = value;
}

int OpenCVCam::getNImages() const
{
    return nImages;
}

void OpenCVCam::setNImages(int value)
{
    nImages = value;
}

QSize OpenCVCam::getResolution() const
{
    return resolution;
}

void OpenCVCam::setResolution(const QSize &value)
{
    qDebug()<<"setResolution"<<value;
    resolution = value;
    if(cap.isOpened()){
        cap.set(cv::CAP_PROP_FRAME_WIDTH, resolution.width());
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, resolution.height());
    }
    //cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('Y','U','Y','2'));
    //cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('M','J','P','G'));
    //open windows camera settings dialog
    cap.set(cv::CAP_PROP_SETTINGS,1);
    qDebug()<<"exposure"<<cap.get(cv::CAP_PROP_EXPOSURE)
           <<"gain"<<cap.get(cv::CAP_PROP_GAIN)
          <<"autoexposure"<<cap.get(cv::CAP_PROP_AUTO_EXPOSURE);
}

bool OpenCVCam::reopen()
{
    bool opened=false;
    if(!deviceName.isEmpty())
        opened = cap.open(deviceName.toLatin1().constData());
    else
        opened = cap.open(deviceId);
    if(!opened)
        return false;
    cap.set(cv::CAP_PROP_FRAME_WIDTH, resolution.width());
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, resolution.height());
    cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('Y','U','Y','2'));
    //cap.set(cv::CAP_PROP_FOURCC,cv::VideoWriter::fourcc('B','G','R','A'));
    qDebug()<<"buffer"<<cap.get(cv::CAP_PROP_BUFFERSIZE);
    return opened;
}
