/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "componentpathcorrection.h"
#include "ui_componentpathcorrection.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QFileDialog>
#include <QDebug>

ComponentPathCorrection::ComponentPathCorrection(long long imageId, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ComponentPathCorrection)
{
    ui->setupUi(this);
    ui->newPathEdit->setFocus();
    QSqlQuery query;
    QString sql="SELECT * FROM compdefimages WHERE id="+QString::number(imageId);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    if(query.next()){
        ui->oldPathEdit->setText(query.value("imagepath").toString());
    } else {
        qDebug()<<"id not found in database"<<query.lastQuery();
    }
    this->imageId=imageId;
}

ComponentPathCorrection::~ComponentPathCorrection()
{
    delete ui;
}

QString ComponentPathCorrection::getNewPath()
{
    return ui->newPathEdit->text();
}

void ComponentPathCorrection::on_updateBtn_clicked()
{
    /// @todo validate input
    //QFile file(ui->newPathEdit->text());
    QFileInfo info(ui->newPathEdit->text());

    if(info.exists()&&info.isFile()){
        ui->newPathEdit->setText(info.absoluteFilePath());
        QSqlQuery query;
        QString sql="UPDATE compdefimages SET imagepath='"+info.absoluteFilePath()+"' WHERE id="+QString::number(imageId);
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError();
            return;
        }
        this->accept();
        return;
    }
    qDebug()<<"nu exista fisierul"<<info.absoluteFilePath();
}

void ComponentPathCorrection::on_toolButton_clicked()
{
    QString newPath=QFileDialog::getOpenFileName(this, "locație nouă "+ui->oldPathEdit->text(),
                                 ui->newPathEdit->text(),
                                 tr("Fisiere Imagine(*.png *.jpg *.bmp *.jpeg);;Toate fișierele(*.*)"));
    if(!newPath.isEmpty())
        ui->newPathEdit->setText(newPath);
}
