#-------------------------------------------------
#
# Project created by QtCreator 2015-11-06T23:35:28
#
#
# Copyright 2015, 2016, 2017, 2018, 2019, 2020 Günter Neustädter
#
# This file is part of examinator.
#
# Examinator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets concurrent

QT += multimedia multimediawidgets network charts serialport
#charts needs libqt5charts5-dev
QMAKE_VERS=$$[QMAKE_VERSION]
greaterThan(QMAKE_VERS,3.0): {
CONFIG += c++17
QMAKE_CXXFLAGS += -std=c++17
} else {
QMAKE_CXXFLAGS += -std=c++17
}

#CONFIG += c++11
CONFIG += console

CONFIG += lrelease

CONFIG += OPENCVCAM
CONFIG += BARCODEREAD

TARGET = examinator
TEMPLATE = app

VERSION = 0.4
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

DEFINES += DLIB_JPEG_SUPPORT
DEFINES += DLIB_PNG_SUPPORT

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3

#add debug info to release build
#QMAKE_CXXFLAGS_RELEASE += -g
#QMAKE_CFLAGS_RELEASE += -g
#QMAKE_LFLAGS_RELEASE =

contains(QMAKE_HOST.arch, arm.*):{
#march=native does not work on raspberry pi
} else {
win32{
#dlib nu merge pe mingw32 cu sse
#QMAKE_CXXFLAGS_RELEASE += -march=native
#QMAKE_CXXFLAGS_RELEASE += -march=sandybridge
#QMAKE_CXXFLAGS_RELEASE += -march=core2
#QMAKE_CXXFLAGS_RELEASE += -march=prescott
#QMAKE_CXXFLAGS_RELEASE += -msse2
#QMAKE_CXXFLAGS_RELEASE += -msse3
QMAKE_CXXFLAGS_RELEASE += -msse4
#QMAKE_CXXFLAGS_DEBUG += -march=native
DEFINES += DLIB_HAVE_SSE41
#DEFINES += DLIB_HAVE_SSE2
LIBS += -lpsapi

} else {
QMAKE_CXXFLAGS_RELEASE += -march=native
}
}

#opencv libraries and support
win32{
INCLUDEPATH += C:\source\opencv\install\include
LIBS += -L"C:\\source\\opencv\\install\\x64\\mingw\\lib"
#LIBS += -lopencv_core242 -lopencv_highgui242 -lopencv_imgproc242 -lopencv_calib3d242 -lopencv_legacy242
LIBS += -lopencv_core411 -lopencv_imgproc411 -lopencv_calib3d411 -lopencv_features2d411 -lopencv_video411
} else {
#video for estimateRigidTransform
LIBS += -lopencv_core -lopencv_imgproc -lopencv_calib3d -lopencv_features2d -lopencv_video
}

#dlib library and support
LIBS += -ldlib
win32{
#include path and support dll's for dlib
INCLUDEPATH += c:/source/dlib
LIBS += -L"c:\\source\\dlib\\dlib\\build"
LIBS += -luser32 -lws2_32 -lgdi32 -lcomctl32 -limm32 -lwinmm
#SOURCES += "E:/source/dlib/dlib/all/source.cpp"
} else {
}

unix:{
    #blas and lapack
    packagesExist(blas){
        message("blas")
        LIBS += -lblas
        DEFINES += DLIB_USE_BLAS
    } else {
        exists("/usr/lib/libblas.so"){
            message("blas exists")
            LIBS += -lblas
            DEFINES += DLIB_USE_BLAS
        }
    }
    packagesExist(lapack){
        message("lapack")
        LIBS += -llapack
    }
    exists("/usr/lib/x86_64-linux-gnu/libcuda.so")|exists("/usr/local/cuda/lib64"){
        exists("/usr/local/cuda/lib64"){
            LIBS += -L/usr/local/cuda/lib64
        }
        LIBS += -lcuda -lcudart -lcublas -lcudnn -lcusolver -lcurand
    }
}
win32{
    packagesExist(blas){
        message("blas")
        LIBS += -lblas
        DEFINES += DLIB_USE_BLAS
    } else{
        exists("C:/source/OpenBLAS-0.3.6-x64/lib"){
            LIBS += -LC:/source/OpenBLAS-0.3.6-x64/lib
            LIBS += -lopenblas
            DEFINES += DLIB_USE_BLAS
        }
    }
}
#packagesExist(png){
#message("png")
#LIBS += -lpng -lX11
#}
CONFIG += link_pkgconfig
win32{
} else {
PKGCONFIG += x11
PKGCONFIG += libpng
PKGCONFIG += libjpeg
}

BARCODEREAD:{
    DEFINES += BARCODEREAD
    HEADERS  += barcode.h
    SOURCES  += barcode.cpp
    unix:{
        packagesExist(zbar){
            PKGCONFIG += zbar
            DEFINES += ZBAR
        }

        packagesExist(libdmtx):{
            PKGCONFIG += libdmtx
            DEFINES += DMTX
        }
        message($$DEFINES)
    } else
    win32:{
        #@todo
        exists("c:/source/zbar/include/zbar.h"){
            DEFINES += ZBAR
            INCLUDEPATH += c:/source/zbar/include
            LIBS += -LC:/source/zbar/zbar/.libs
            LIBS += -lzbar
        }
        exists("c:/source/libdmtx/dmtx.h"){
            DEFINES += DMTX
            INCLUDEPATH += c:/source/libdmtx
            #LIBS += -LC:/source/libdmtx/build
            LIBS += -LC:/source/libdmtx/.libs
            LIBS += -ldmtx
        }

    }
}

SOURCES += main.cpp\
    filedeleter.cpp \
    iocontrol.cpp \
    testdebugwidget.cpp \
        widget.cpp \
    mscene.cpp \
    cornergrabber.cpp \
    compdefdialog.cpp \
    boarddef.cpp \
    boardcomptype.cpp \
    boardcomp.cpp \
    boardconfig.cpp \
    boardcomptypeconfig.cpp \
    boardcompaltdialog.cpp \
    detectedcomponent.cpp \
    testinterface.cpp \
    boardselect.cpp \
    compdefrect.cpp \
    compdefimage.cpp \
    compdef.cpp \
    myftp.cpp \
    checksn.cpp \
    cameraconfig.cpp \
    ipcamera.cpp \
    mycamera.cpp \
    componentpathcorrection.cpp \
    logger.cpp \
    compspparams.cpp \
    compdnnparams.cpp \
    compstatmodel.cpp


HEADERS  += widget.h \
    compdef_dnn.h \
    filedeleter.h \
    iocontrol.h \
    mscene.h \
    cornergrabber.h \
    compdefdialog.h \
    boarddef.h \
    boardcomptype.h \
    boardcomp.h \
    boardconfig.h \
    boardcomptypeconfig.h \
    boardcompaltdialog.h \
    detectedcomponent.h \
    testdebugwidget.h \
    testinterface.h \
    boardselect.h \
    compdefrect.h \
    compdefimage.h \
    compdef.h \
    checksn.h \
    myftp.h \
    cameraconfig.h \
	ipcamera.h \
    mycamera.h \
    componentpathcorrection.h \
    logger.h \
    compspparams.h \
    compdnnparams.h \
    compstatmodel.h

FORMS    += widget.ui \
    compdefdialog.ui \
    boardconfig.ui \
    boardcomptypeconfig.ui \
    boardcompaltdialog.ui \
    filedeleter.ui \
    testdebugwidget.ui \
    testinterface.ui \
    boardselect.ui \
    cameraconfig.ui \
    componentpathcorrection.ui


OPENCVCAM {
    DEFINES += OCVCAM
    HEADERS  += opencvcam.h
    SOURCES  += opencvcam.cpp
    win32{
    LIBS += -lopencv_videoio411
    } else {
    LIBS += -lopencv_videoio
    }
}

DISTFILES += \
    doc/Doxyfile \
    mainpage.dox \
    doc/examinator_board_select.png \
    doc/examinator_edit_board_admin.png \
    doc/examinator_edit_board_comp.png \
    doc/examinator_edit_board_comp_type.png \
    doc/examinator_edit_board_config.png \
    doc/examinator_edit_board_ref.png \
    doc/examinator_editor_componente.png \
    doc/examinator_editor_comp_varianta.png \
    doc/examinator_main.png \
    doc/examinator_main_fail.png \
    doc/examinator_main_new.png \
    doc/examinator_train_classifier.png \
    netconfig.sh \
    COPYING \
    examinator.ts


# dox.target = doc
# dox.commands = doxygen doc/Doxyfile
# dox.depends =

# somewhere else in the *.pro file
# QMAKE_EXTRA_UNIX_TARGETS += dox
unix:{
docs.depends = $(SOURCES)
#docs.commands = (cat doc/Doxyfile; echo "INPUT = $?") | doxygen -
docs.commands = cd $$PWD; doxygen $$PWD/doc/Doxyfile
QMAKE_EXTRA_TARGETS += docs
PRE_TARGETDEPS += docs
}
win32:{
#docs.depends = $(SOURCES)
#docs.commands = (cat doc/Doxyfile; echo "INPUT = $?") | doxygen -
#docs.commands = cd $$PWD; "C:\Program Files\doxygen\bin\doxygen.exe" doc\\Doxyfile
docs.commands = cd $$PWD && doxygen.exe doc\\Doxyfile
QMAKE_EXTRA_TARGETS += docs
PRE_TARGETDEPS += docs
}




TRANSLATIONS += $$PWD/examinator.ts

#generates *.qm files from TRANSLATIONS= to the directory builddir/.qm/
#adds them as qrc resources
CONFIG+=embed_translations

#QM_FILES_RESOURCE_PREFIX=/qmfiles/
#QM_FILES_INSTALL_PATH = translations

RESOURCES += icons.qrc
    #qmake_qmake_qm_files.qrc
unix:{
    RESOURCES += tr.qrc
QMAKE_EXTRA_COMPILERS += lrelease
lrelease.input         = TRANSLATIONS
lrelease.output        = $${IN_PWD}/${QMAKE_FILE_BASE}.qm
lrelease.commands      = $$[QT_INSTALL_BINS]/lrelease ${QMAKE_FILE_IN} -qm $${IN_PWD}/${QMAKE_FILE_BASE}.qm
lrelease.CONFIG       += no_link target_predeps
}



message($$TRANSLATIONS)

