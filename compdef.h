/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPDEF_H
#define COMPDEF_H
#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include "compdef_dnn.h"
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QImage>
#include "compdefimage.h"
#include "compdefrect.h"
#include "detectedcomponent.h"
#include "compspparams.h"
#include "compdnnparams.h"

#include <dlib/opencv/cv_image.h>
//#include <dlib/svm_threaded.h>
//#include <dlib/string.h>
//#include <dlib/gui_widgets.h>


#define PARALLELCOMPDET 1

const int scannerPyramid=8;

class CompDefImage;
class CompDefRect;
typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<scannerPyramid> > image_scanner_type;

//dlib::array<dlib::array2d<unsigned char> > images;
//std::vector<std::vector<dlib::rectangle> > object_locations, ignore;


/** class containing a component definition loaded into memory
 *
 * The component definition consists mainly of a list of annotated images, which together form the
 * dataset needed to train the classifier to recognize one component type.
 * Besides the dataset, the parameters used to train the classifiers are also stored in this class,
 * as well as the resulting classifiers.
*/
class CompDef : public QObject
{
    Q_OBJECT
public:
    CompDef();
    /// flag indicating that the component definition has been modified, and needs to be saved
    bool dirty;
    /// list of annotated images
    QList<CompDefImage> images;
    /// method to add a new image to the list of annotated images, initially with empty annotation
    CompDefImage *addImage(QString filename);
    /// aspect ratio enforced on all the positive samples
    qreal aspectRatio;
    /// selector indicating the possible component orientations allowed for this definition
    CompDefRect::OrientationMode orientationMode;
    /// save component definition, including the annotated image list, to the database
    /// this will generate an component id if it's the first time this component is saved for this component
    void saveToDB();
    /// search in the database if the name used for this component is not already used, and alter the name of the current component if the name is in use
    /// this alteration is done only in memory, so the component needs to be saved in order to modify the name permanently
    QString makeNameUnique(void);
    /// save a copy of the component definition
    long long saveToDBAs(QString newName);
    /// remove component definition, including the annotated image list, from the database
    bool removeFromDB();
    /// load the component definition, without the associated classifiers, from the database
    /// this will just create the datastructures, without displaying anything
    void loadFromDB(const long long id, bool checkImageFiles=false);
    /// component definition name
    QString name;
    /// component definition id, used in the database
    long long id;
    double C;
    double eps;
    /// minimum area for a component rectangle. If upsamplig is allowed, the minimum area is considered after upsampling
    int minArea;
    /// minimum area for background rectangles - multiplier applied to minArea
    const double backgroundMultiplier=10.0;
    int upsample;
    int numThreads;
    bool horizontalFlip;
    bool verticalFlip;
    bool trainedFHOG=false;
    bool oldFHOG=true;
    bool trainedDNN=false;
    bool oldDNN=true;
    double fhogThreshold=-0.1;
    //number of orientations for which a fhog classifier will be trained. The actual value is 2**fhogNOrient.
    int fhogNOrient=0;
    /**
      [0] - enabled, and percentage of the rectangle width and height .
            If width==0, validation is not active.

      [1]...[6] - min and max value for R, G, B, H, S, V as calculated by qimage.
    */
    std::array<std::array<double, 2>,7>colorValidator={};
    CompDefRect recentRect;
    static unsigned maxDNNArea;
    void trainFHOG(QString databaseName);
    void trainShapePredictor(QString databaseName);
    void trainDNN(QString databaseName);
    ///load a trained dnn from disk into memory and store it in the database
    bool importDNN(QString fileName);
    bool exportDNN(QString fileName);
    //run the shape predictor on each image containing rectangles and report the average positional error, in pixels
    double testShapePredictor(const dlib::array<dlib::array2d<dlib::rgb_pixel> > &imageArray, const std::vector<std::vector<dlib::full_object_detection> > &fullObjects, int spIdx);
    bool importAutoinspect(QString dirName);
    /*template<typename image_type1>
    QList<DetectedComponent> detectObjects(const image_type1 &img);
    */
    ///
    /// \brief detectObjects - this function converts the opencv image to dlib::array2d and calls the array2d version of this function
    /// \param img
    /// \param displaySearchFrames
    /// \return list of detected components (of type DetectedComponent) ordered by score
    ///
    QList<DetectedComponent> detectObjects(const dlib::cv_image<dlib::rgb_pixel> &img, bool displaySearchFrames);
    ///
    /// \brief detectObjects - search for instances of the objects recognized by the classifier in the input image
    /// \param img - input image
    /// \param displaySearchFrames flag requesting the display of the search ROI's
    /// \param qImg copy of the input image, in QImage format, used for the color validator
    /// \param adjustThreshold additive threshold adjustment, used to lower the threshold for testing purposes in order to assess if the threshold is good
    /// \return
    ///
    QList<DetectedComponent> detectObjects(const dlib::array2d<dlib::rgb_pixel> &img, bool displaySearchFrames, const QImage qImg=QImage(), double adjustThreshold=0);
    QList<DetectedComponent> detectObjectsFHOG(const dlib::array2d<dlib::rgb_pixel> &img, const QImage & qImg, double adjustThreshold);
    QList<DetectedComponent> detectObjectsDNN(const dlib::array2d<dlib::rgb_pixel> &img, const QImage & qImg, double adjustThreshold);
    /// recursive function used to divide and conquer images that don't fit into the GPU memory
    /// for images that are bigger than what fits into the GPU memory, first the image is divided into two (if these already fit into memory) or four partially overlapping tiles.
    /// in order to get also the objects crossing the boundaries, the network is also run on the entire image, shrunken by a factor of two for each direction.
    /// the detections are simply concatenated into the result vector. Afterwards, non maxima supression is done in order to keep only the relevant detections.
    template <
            typename net_type
            >
    std::vector<dlib::mmod_rect> detectObjectsDNNBig(const dlib::array2d<dlib::rgb_pixel> &img, net_type &net, double threshold, bool toplevel);
    /// load fhog classifier and shape predictor if needed
    void prepareToRun(void);
    void loadFHOGDetector(void);
    void loadShapePredictor(void);
    void loadDNN(void);
    /**
     * calculate the average channel value for the RGB channels, and the HSV values corresponding to these average
     * values using QColor::getHsvF
    */
    void averageColor(const QImage img, double &r, double &g, double &b, double &h, double &s, double &v);
    /**
     * @brief calculate the average color of the central area of the image using averageColor, on the portion of
     * the image defined by the color validator for this component definition
     * @param img input image
     * @param r reference to red component
     * @param g reference to green component
     * @param b reference to blue component
     * @param h reference to hue component
     * @param s reference to saturation component
     * @param v reference to value component
     */
    void averageColorCenter(const QImage img, double &r, double &g, double &b, double &h, double &s, double &v);
    /**
     * @brief calculate the average color and standard deviation of the central area of all positive samples
     * @param r
     * @param rSigma
     * @param g
     * @param gSigma
     * @param b
     * @param bSigma
     * @param h
     * @param hSigma
     * @param s
     * @param sSigma
     * @param v
     * @param vSigma
     */
    void calcAverageColor(double &r, double &rSigma, double &g, double &gSigma, double &b, double &bSigma,
                          double &h, double &hSigma, double &s, double &sSigma, double &v, double &vSigma);

    bool validateColor(QImage img, DetectedComponent &dc);
    template <
            typename pyramid_type,
            typename image_array_type
            >
    void conditionalUpsampleImageDataset(image_array_type &images,
                                         std::vector<std::vector<dlib::rectangle> > &objects,
                                         std::vector<std::vector<dlib::rectangle> > &objects2,
                                         unsigned long minRectArea,
                                         unsigned long max_image_size = std::numeric_limits<unsigned long>::max());
    template <
            typename pyramid_type,
            typename image_array_type
            >
    void conditionalDownsampleImageDataset(image_array_type &images,
                                         std::vector<std::vector<dlib::rectangle> > &objects,
                                         std::vector<std::vector<dlib::rectangle> > &objects2,
                                         unsigned long maxRectArea);
    bool usesImage(QString fileName);
    bool saveDlibDataset(QString fileName);
    CompSpParams compSpParams;
    CompDNNParams compDNNParams;
    inline unsigned numFHOGDetectors(){
        unsigned i=0;
        for(;i<4; i++){
            if(fhogDetector[i].num_detectors()==0)
                break;
        }
        return i;
    }
    inline unsigned numShapePredictors(){
        unsigned i=0;
        for(;i<4; i++){
            if(shapePredictor[i].num_parts()==0)
                break;
        }
        return i;
    }
private:
    /// reference rectangle, for future use. Not used at the moment.
    CompDefRect primaryRect;
    dlib::rectangle minRect();
    std::array<dlib::object_detector<image_scanner_type>,4> fhogDetector;
    std::array<dlib::shape_predictor,4> shapePredictor;
    anet_type1 dnnDetector;
    bool spLoaded=false;
    bool fhogLoaded=false;
    bool dnnLoaded=false;
    const double chipMargin=1; ///< margins around the actual component, as fraction of the component dimensions, used to create the training dataset.
    /**
     * for the current component definition, loaded into this object, generate a in-memory dlib training dataset, consisting of
     * a list of images as dlib arrays, a list of object locations for each image, and a lis of ignores for each image.
     * As opposed to the way dlib usually work, this creates at most one object location per image, cropping the images around the
     * object markings with some margins.
     * Data augmentation by horizontal and vertical mirrong is applied as configured.
     * Negative markings generate images with object locations, only objects within the negative image having the proper orientation being marked.
     * This leads to duplicating these positive samples, but has been added in order to try to add some context around object margins,
     * because MMOD tends to reject objects placed close to each other if only trained without any context around the objects.
     */
    template <
        typename array_type
        >
    std::vector<std::vector<dlib::rectangle> > createDlibDataset(dlib::array<array_type >& imageArray,
                       std::vector<std::vector<dlib::rectangle> > &objectLocations, bool growAndShrink=false);
    /**
    For one negative marking of an image from the list of images belonging to a component definition, extract all
    the parts to be ignored, either because labeled as positives, or as ignores.
    */
    template <
        typename array_type
        >
    std::vector<dlib::rectangle> createNegativeMarking(const CompDefImage & image,
                                                                     const array_type &bigImage, array_type &imageChip,
                                                                     double x, double y, double w, double h,
                                                       CompDefRect::Orientation orientation,
                                                       std::vector<dlib::rectangle> &objects);
    /// function based on the dlib function flip_image_dataset_left_right
    template<typename image_array_type>
    void flipImageDatasetUpDown(image_array_type &images, std::vector<std::vector<dlib::rectangle> > &objects, std::vector<std::vector<dlib::rectangle> > &objects2);
    /// helper function written based on the flip_image_left_right function in dlib
    template<typename image_type1, typename image_type2>
    dlib::point_transform_affine flipImageUpDown(const image_type1 &in_img, image_type2 &out_img);
    /// helper function based on flip_rect_left_right from dlib
    dlib::rectangle flipRectUpDown(const dlib::rectangle &rect, const dlib::rectangle &window);
    ///
    template<typename image_array_type, typename T, typename U>
    void addImageUpDownFlips(image_array_type &images, std::vector<std::vector<T> > &objects, std::vector<std::vector<U> > &objects2);
    void saveFHOGDetector(QString databaseName);
    void saveShapePredictor(QString databaseName);
    void saveDNN(QString databaseName, net_type1 net);
    /// helper function used when importing autoinspect files
    /// This function works as the QString::split function, but leaves portions surrounded by double quotes in one split
    QStringList quotedSplit(QString text);
    QString unquote(QString str);
    CompDefImage * getImage(const QString & imagePath);///< find the image object referring to the given image file, and return a pointer to it, or nullptr no match is found
    ///calculate ratio of rectangle r1 covered by r2
    static double percentCoveredBy(dlib::rectangle &r1, dlib::rectangle &r2);
    bool loadDNN(long long compId, net_type1 &net, QString databaseName);
};

template <typename image_array_type>
void rotate_image_dataset (
    double angle,
    image_array_type& images,
    std::vector<std::vector<dlib::full_object_detection> >& objects
)
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size(),
        "\t void rotate_image_dataset()"
        << "\n\t Invalid inputs were given to this function."
        << "\n\t images.size():   " << images.size()
        << "\n\t objects.size():  " << objects.size()
        );

    typename image_array_type::value_type temp;
    for (unsigned long i = 0; i < images.size(); ++i)
    {
        const dlib::point_transform_affine tran = dlib::rotate_image(images[i], temp, angle);
        swap(temp, images[i]);
        for (unsigned long j = 0; j < objects[i].size(); ++j)
        {
            dlib::full_object_detection d=dlib::impl::tform_object(tran,objects[i][j]);
            //const dlib::rectangle rect = objects[i][j];
            objects[i][j] = d;
        }
    }
}


#endif // COMPDEF_H
