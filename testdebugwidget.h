#ifndef TESTDEBUGWIDGET_H
#define TESTDEBUGWIDGET_H

#include <QWidget>
#include <QDir>
#include "detectedcomponent.h"
#include <QList>
#include <QAbstractListModel>
#include "boarddef.h"
#include <QTimer>

namespace Ui {
class testDebugWidget;
}

class ImageResults{
public:
    QList<DetectedComponent> boards;
    QString imageName;
    QString resultsName;
    int decision=-1;
    bool unknown=true;
    bool dirty=false;
    void calculateDecision(void);
};

class BoardStatModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit BoardStatModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags (const QModelIndex& index) const override;
    void setImages(QList<ImageResults> * images);
    void prepareForUpdate(void);
    void updateFinished(void);
    void update(const QModelIndex &index){
        emit dataChanged(index, index);
    }
    void update(const QModelIndex &index,const QModelIndex &index2){
        emit dataChanged(index, index2);
    }
    bool setData(const QModelIndex &index,
                                    const QVariant &value, int role) override{

        if(!index.isValid() || role != Qt::CheckStateRole)
            return false;
        (*images)[index.row()].dirty=true;
        //qDebug()<<"setData"<<value<<(*images)[index.row()].decision;
        if(value.toInt() == Qt::Checked){
            (*images)[index.row()].decision= ((*images)[index.row()].decision==0)?-1:1;
        }else if(value.toInt() == Qt::Unchecked){
            (*images)[index.row()].decision=0;
        } else
        (*images)[index.row()].decision=-1;
        if((*images)[index.row()].boards.count()>0){
            auto &components = (*images)[index.row()].boards[0].parts;
            if((*images)[index.row()].decision==1){
                for(int i=0; i<components.count(); i++)
                    components[i].decision=1;
            } else if((*images)[index.row()].decision==0){
                bool ok=false;
                for(int i=0; i<components.count(); i++)
                    if(components[i].decision!=1)
                        ok=true;
                if(!ok)
                    for(int i=0; i<components.count(); i++)
                        components[i].decision=-1;
            } else {
                bool ok=false;
                for(int i=0; i<components.count(); i++){
                    if(components[i].decision==-1)
                        ok=true;
                    else if(components[i].decision==0){
                        components[i].decision=-1;
                        ok=true;
                    }
                }
                if(!ok)
                    for(int i=0; i<components.count(); i++)
                        components[i].decision=-1;
            }
        }
        emit dataChanged(index, index);
        emit dataModified(index.row());
        return true;
    }
private:
    QList<ImageResults> *images=nullptr;
signals:
    void dataModified(int position);
};

class ImageStatModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ImageStatModel(QObject *parent = nullptr){
        Q_UNUSED(parent);
    };

    // Header:
    //QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags (const QModelIndex& index) const override;
    void setImages(QList<ImageResults> * images);
    ///sets the current image to be shown and updates the model
    void prepareForUpdate(void);
    void updateFinished(void);
    void update(const QModelIndex &index){
        emit dataChanged(index, index);
    }
    void update(const QModelIndex &index,const QModelIndex &index2){
        emit dataChanged(index, index2);
    }
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override
    {
       if (role != Qt::DisplayRole)
            return QVariant();
       switch(section){
       case 0:
           return tr("nume");
       case 1:
           return tr("scor");
       case 2:
           return tr("x");
       case 3:
           return tr("y");
       case 4:
           return tr("scala");
       default:
           return QVariant();
       }
    }
    bool setData(const QModelIndex &index,
                                    const QVariant &value, int role) override{

        if(!index.isValid() || (role != Qt::CheckStateRole))
            return false;
        (*images)[imageNum].dirty=true;
        auto &components=(*images)[imageNum].boards[0].parts;
        qDebug()<<"setData"<<value.toInt()<<components[index.row()].decision;
        if(value.toInt() == Qt::Checked){
            components[index.row()].decision= (components[index.row()].decision==0)?-1:1;
        }else if(value.toInt() == Qt::Unchecked){
            components[index.row()].decision=0;
        } else
            components[index.row()].decision=-1;
        qDebug()<<"setData"<<value.toInt()<<components[index.row()].decision;

        emit dataChanged(index, index);
        emit dataModified(imageNum, components[index.row()].decision);
        return true;
    }
private:
    int imageNum;
    QList<ImageResults> *images=nullptr;
public slots:
    void setImage(int image);
signals:
    void dataModified(uint image, int decision);

};

class TestDebugWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TestDebugWidget(QWidget *parent = nullptr);
    ~TestDebugWidget();
    void setBoardDefPtr(BoardDef ** boardDefPtr);

private slots:
    void on_loadDirBtn_clicked();

    void on_boardTestBtn_clicked();

    void on_analyzeBtn_clicked();

    void on_delBtn_clicked();

    void on_copyAndOpenBtn_clicked();
    void masterViewCurrentChanged(const QModelIndex&idxTo, const QModelIndex&idxFrom);

private:
    QList<ImageResults> images;
    QDir dir;
    Ui::testDebugWidget *ui;
    BoardStatModel boardStatModel;
    ImageStatModel imageStatModel;
    //this is only safe if the testDebugWidget does not lives longer than the
    //testInterface. This should not be a problem, as the widget is embedded into the testInterface
    BoardDef ** boardDefPtr=nullptr;
    /** run the currently loaded board test on one image and store the results
    returns PASS status
    */
    bool processOneImage(ImageResults & res);
    QTimer timer;
signals:
    void imageChanged(QString imageName, QList<DetectedComponent> & boardRects, bool imageOk);
    /// Request to add the image to the specified classifier
    /// Used usually for failing components in one image, the classifier beine the one used for that component
    void imageForClassifier(long long classifierId, const QString & imagePah);
    void showBoard(long long boardId);
    void centerAt(double x, double y);
private slots:
    void updateDetail(int position);
    void updateMaster(unsigned image, int decision);
    void on_openBoardBtn_clicked();
    void on_saveBtn_clicked();
    void on_detailTreeView_doubleClicked(const QModelIndex &index);
};



#endif // TESTDEBUGWIDGET_H
