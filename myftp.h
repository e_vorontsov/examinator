/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MYFTP_H
#define MYFTP_H

#include <QNetworkAccessManager>
#include <QUrl>
#include <QDir>
#include <QSettings>
#include <QTimer>
#include "logger.h"


/**
 * @brief The MyFtp class
 */
class MyFtp : public QObject
{
Q_OBJECT
public:
    ///
    /// \brief MyFtp
    /// \param _parent
    /// \param inifile
    ///
    MyFtp(QObject *_parent = 0, QString inifile="");

    ~MyFtp();
    QSettings * settings;
    void startUpload(void);

public slots:
    /**
     * @brief cmdFinishedSlot
     */
    void cmdFinishedSlot();
    void createConfigItem(QString item, QString str, QSettings *mySettings);
    void createConfigItem(QString item, int val, QSettings *mySettings);

    void ftpUpload(void);
private slots:
    void watchdogSlot(void);
    void errorSlot(void);
signals:
    void sendToLog(QString message, LOGVERBOSITY level);

private:
    QObject *parent;
    int startId;
    QUrl url;
    QNetworkAccessManager nam;

    QNetworkReply * volatile reply;


    QDir leikaFiles;
    QFile file;
    QString ftpServer;
    QString ftpPath;
    QString ftpUser;
    QString ftpPassword;
    void configItem(QString item, QString str);
    void configItem(QString item, int val);
    void defaultConfig();
    void cleanUpFile(QFile &file);
    int busy=false;
    QTimer watchdog;
    QTimer uploadTimer;
};

#endif // MYFTP_H
