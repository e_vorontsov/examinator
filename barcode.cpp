#include "barcode.h"

const QStringList Barcode::bcTypes=QStringList()<<"toate"<<"datamatrix"<<"QR code"<<"code39"<<"code128";

Barcode::Barcode()
{

}

QString Barcode::decodeBC(const QImage image, int & errorCode, Barcode::BCType symbology)
{
    qDebug()<<"decodeBC"<<symbology;
    //adjust width in order to be a multiple of 4, because QImage has 32 bits aligned scanlines, and libdmtx is not using this padding.
    int width4=((image.width()+3)/4)*4;
    QImage tmp=image.convertToFormat(QImage::Format_Grayscale8).copy(0,0,width4,image.height());
    QString result;
    uchar * dataPtr=tmp.bits();
#ifdef ZBAR
    if(symbology!=DATAMATRIX){
        result=decodeBCZbar(dataPtr, tmp.width(), tmp.height(), tmp.byteCount(), symbology, errorCode);
        if(!result.isEmpty())
            return result;
    }
#endif
    errorCode=-21;
#ifdef DMTX
    result=decodeBCDmtx(dataPtr, tmp.width(), tmp.height(), errorCode);
#endif
    return result;
}

#ifdef ZBAR
QString Barcode::decodeBCZbar(const uchar *dataPtr, const int w, const int h, const int l, const Barcode::BCType symbology, int &errorCode)
{
    errorCode=-1;
    zbar::ImageScanner scanner;
    //enable all symbologies
    auto zbarSymbology=zbar::ZBAR_NONE;
    QString expectedType;
    switch(symbology){
    case QRCODE:
        zbarSymbology=zbar::ZBAR_QRCODE;
        break;
    case CODE39:
        zbarSymbology=zbar::ZBAR_CODE39;
        break;
    case CODE128:
        zbarSymbology=zbar::ZBAR_CODE128;
        expectedType="CODE-128";
        break;
    default:
        //enable all
        zbarSymbology=zbar::ZBAR_NONE;
    }
    scanner.set_config(zbar::ZBAR_NONE, zbar::ZBAR_CFG_ENABLE, 0);
    scanner.set_config(zbarSymbology, zbar::ZBAR_CFG_ENABLE, 1);
    zbar::Image img(w, h, "Y800",dataPtr, l);
    qDebug()<<l<<dataPtr<<w<<h<<
              img.get_data_length()<<img.get_width()<<img.get_height()<<QString::number(img.get_format(),16);
    scanner.scan(img);
    QString msg;
    for(zbar::Image::SymbolIterator symbol = img.symbol_begin();
        symbol != img.symbol_end();
        ++symbol) {

        // do something useful with results
        QString symbology=QString::fromStdString( symbol->get_type_name());
        qDebug() << "decoded " << symbology
             << " symbol " << QString::fromStdString(symbol->get_data());
        msg=QString::fromStdString(symbol->get_data());
        if(!msg.isEmpty()){
            if((expectedType.isEmpty()) || (expectedType.compare(symbology)==0)){
                //use this result
                qDebug()<<"using"<<symbology<<"expected"<<expectedType;
                errorCode=0;
                break;
            } else {
                //ignore this result
                qDebug()<<"ignoring"<<symbology<<expectedType;
                msg.clear();
            }
        }
//        if(!msg.isEmpty())
//            msg+="\n";
//        msg += QString::fromStdString( symbol->get_type_name())+":"+QString::fromStdString(symbol->get_data());

    }
    img.set_data(nullptr, 0);
    return msg;
}
#endif


#ifdef DMTX
QString Barcode::decodeBCDmtx(uchar *dataPtr, const int w, const int h, int &errorCode)
{
    QString result;
    DmtxImage * dmtxImage = dmtxImageCreate(dataPtr, w, h, DmtxPack8bppK);
    if(!dmtxImage){
        qDebug()<<"can't create image";
        errorCode=-11;
        return "";
    }
    //dmtxImageSetProp(dmtxImage,DmtxPropImageFlip,DmtxFlipY);
    qDebug()<<dmtxImage->imageFlip<<dmtxImage->channelCount<<dmtxImage->bytesPerPixel<<dmtxImage->rowPadBytes;
    DmtxDecode * dec= dmtxDecodeCreate(dmtxImage,1);
    if(!dec){
        qDebug()<<"can't create dmtx decoder";
        errorCode=-12;
        return "";
    }
    DmtxRegion * reg = dmtxRegionFindNext(dec, NULL);
    if(reg != NULL) {
        DmtxMessage * message = dmtxDecodeMatrixRegion(dec, reg, DmtxUndefined);
        //DmtxMessage * message = dmtxDecodeMosaicRegion(dec, reg, DmtxUndefined);
       if(message != NULL) {
          //fputs("output: \"", stdout);
          //fwrite(message->output, sizeof(unsigned char), message->outputIdx, stdout);
          //fputs("\"\n", stdout);
          result=QString((const char *)(message->output));
          qDebug()<<"dmtx:"<<result;
          dmtxMessageDestroy(&message);
          errorCode=0;
       } else {
           errorCode=-14;
       }
       dmtxRegionDestroy(&reg);
    } else {
        qDebug()<<"no region found";
        errorCode=-13;
    }

    dmtxImageDestroy(&dmtxImage);
    return result;
}
#endif

