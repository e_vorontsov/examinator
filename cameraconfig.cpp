/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "cameraconfig.h"
#include "ui_cameraconfig.h"
#include "testinterface.h"

#define QCAMERAIDX 0
#define IPCAMERAIDX 1
#define SIMCAMERAIDX 2
#define OCVCAMERAIDX 3

CameraConfig::CameraConfig(QSettings *settings, MyCamera * myCamera, IpCamera * ipCamera,
#ifdef OCVCAM
                           OpenCVCam * ocvCamera,
#endif
                           QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CameraConfig)
{
    TestInterface * realParent=dynamic_cast<TestInterface *>(parent);
    ui->setupUi(this);
    ui->cameraTypeBox->blockSignals(true);
    this->settings=settings;
    this->myCamera=myCamera;
    this->ipCamera=ipCamera;
#ifdef OCVCAM
    this->ocvCamera=ocvCamera;
#endif
    QString ipcam=settings->value("ipwebcam").toString();
    if(!ipcam.isEmpty())
        ui->ipCamEdit->setText(ipcam);

    ui->cameraTypeBox->clear();
    ui->cameraTypeBox->addItem(tr("camera web"));//qcamera
    ui->cameraTypeBox->addItem(tr("IP Webcam"));//ipcamera
    ui->cameraTypeBox->addItem(tr("imagini disc"));//simcamera
#ifdef OCVCAM
    ui->cameraTypeBox->addItem(tr("camera OPENCV"));
#endif

    ui->cameraTypeBox->blockSignals(false);
    if(settings->value("cameratype").toString()=="qcamera"){
        qCameraInitialize();
        qDebug()<<"qcamera"<<settings->value("cameratype").toString();
        ui->cameraTypeBox->setCurrentIndex(QCAMERAIDX);
        //workaround, this is not always triggered
        ui->stackedWidget->setCurrentIndex(QCAMERAIDX);
    } else if(settings->value("cameratype").toString()=="ipcamera"){
        qDebug()<<"ipcamera"<<settings->value("cameratype").toString();
        ui->cameraTypeBox->setCurrentIndex(IPCAMERAIDX);
    } else if(settings->value("cameratype").toString()=="simcamera"){
        qDebug()<<"ipcamera"<<settings->value("cameratype").toString();
        ui->cameraTypeBox->setCurrentIndex(SIMCAMERAIDX);
    } else {
#ifdef OCVCAM
        qDebug()<<"ocvcamera"<<settings->value("cameratype").toString();
        ui->cameraTypeBox->setCurrentIndex(3);
#endif
    }

    connect(ipCamera, SIGNAL(imageCaptured(QPixmap)), this, SLOT(showPixmap(QPixmap)));
    connect(myCamera, SIGNAL(imageCaptured(QPixmap)), this, SLOT(showPixmap(QPixmap)));
#ifdef OCVCAM
    connect(ocvCamera, SIGNAL(imageCaptured(QImage)), this, SLOT(showImage(QImage)));
#endif
    connect(realParent, SIGNAL(imageLoaded(QImage)), this, SLOT(showImage(QImage)));
}

CameraConfig::~CameraConfig()
{
    delete ui;
}


void CameraConfig::on_tryBtn_clicked()
{
    if(ui->stackedWidget->currentIndex()==IPCAMERAIDX){
        ipCamera->setUrl(ui->ipCamEdit->text());
        ipCamera->capture();
        settings->setValue("ipwebcam",ui->ipCamEdit->text());
    } else if(ui->stackedWidget->currentIndex()==QCAMERAIDX){
        myCamera->capture();
#ifdef OCVCAM
    } else if(ui->stackedWidget->currentIndex()==OCVCAMERAIDX){
        ocvCamera->capture();
#endif
    } else {
        emit loadImageRequest();
    }
}

void CameraConfig::showPixmap(QPixmap pixmap)
{
    ui->imageLbl->setPixmap(pixmap);
}
void CameraConfig::showImage(QImage image)
{
    ui->imageLbl->setPixmap(QPixmap::fromImage(image));
}

void CameraConfig::on_qCameraBox_currentIndexChanged(int index)
{
    QString newCamera=ui->qCameraBox->itemData(index).toString();
    QString oldCamera=myCamera->getDeviceName();
    qDebug()<<"qCameraBox"<<oldCamera<<newCamera;
    if(newCamera!=oldCamera)
        myCamera->setCamera(newCamera);
    settings->setValue("qcamera",newCamera);
    QList<QSize> resolutions = myCamera->getResolutions();
    ui->qCameraResolutionBox->blockSignals(true);
    QSize resolution=myCamera->resolution();
    qDebug()<<"resolution"<<resolution;
    ui->qCameraResolutionBox->clear();
    foreach(const QSize & size, resolutions){
        ui->qCameraResolutionBox->addItem(QString("(%1,%2)").arg(size.width()).arg(size.height()),size);
        if(size==resolution){
            ui->qCameraResolutionBox->setCurrentIndex(ui->qCameraResolutionBox->count()-1);
            settings->setValue("qcameraresolution",myCamera->resolution());
        }
    }
    ui->qCameraResolutionBox->blockSignals(false);
}

void CameraConfig::on_qCameraResolutionBox_currentIndexChanged(int index)
{
    QSize size=ui->qCameraResolutionBox->itemData(index).toSize();
    myCamera->setResolution(size);
    settings->setValue("qcameraresolution",myCamera->resolution());
    qDebug()<<"saved resoluion as"<<myCamera->resolution();
}

void CameraConfig::on_cameraTypeBox_currentIndexChanged(int index)
{
    qDebug()<<"cameratypebox_"<<index;
    settings->setValue("cameratype",(index==0)?"qcamera":((index==1)?"ipcamera":((index==2)?"simcamera":"ocvcamera")));
    switch(index){
    case QCAMERAIDX:
#ifdef OCVCAM
        if(ocvCamera)
            ocvCamera->setCamera(ocvCamera->getDeviceId(),false);
#endif
        if(!qCameraInitialized)
            qCameraInitialized = qCameraInitialize();
        break;
    case IPCAMERAIDX:
        //disable camera
        myCamera->setCamera("",false);
#ifdef OCVCAM
        if(ocvCamera)
            ocvCamera->setCamera(ocvCamera->getDeviceId(),false);
#endif
        break;
    case SIMCAMERAIDX:
        //disable camera
        myCamera->setCamera("",false);
#ifdef OCVCAM
        if(ocvCamera)
            ocvCamera->setCamera(ocvCamera->getDeviceId(),false);
#endif
        break;
    case OCVCAMERAIDX:
        //disable camera
        myCamera->setCamera("",false);
#ifdef OCVCAM
        if(!ocvCameraInitialized)
            ocvCameraInitialized = ocvCameraInitialize();
#endif
    }
}

void CameraConfig::on_qNImagesSpin_valueChanged(int arg1)
{
    settings->setValue("qnimages",arg1);
}

bool CameraConfig::qCameraInitialize()
{
    if(!myCamera){
        return false;
    }
    cameras=myCamera->getAvailableCameras();
    ui->qCameraBox->clear();
    if(cameras.count()>0){
        ui->qCameraBox->blockSignals(true);
        foreach (const QString cam, cameras) {
            ui->qCameraBox->addItem(cam, cam);
            qDebug()<<cam;
        }
        QString device=myCamera->getDeviceName();
        if(device.isNull())
            device=QCameraInfo::defaultCamera().deviceName();
        ui->qCameraBox->setCurrentIndex(ui->qCameraBox->findText(device));
        ui->qCameraBox->blockSignals(false);
        //add resolutions to the combobox
        on_qCameraBox_currentIndexChanged(ui->qCameraBox->currentIndex());
    } else {
        qDebug()<<"no cameras found";
        ui->cameraTypeBox->setCurrentIndex(IPCAMERAIDX);
    }
    ui->qNImagesBox->blockSignals(true);
    ui->qNImagesBox->setValue(settings->value("qnimages",1).toInt());
    ui->qNImagesBox->blockSignals(false);
    return true;
}

#ifdef OCVCAM
bool CameraConfig::ocvCameraInitialize()
{
    if(!ocvCamera){
        //camera object not created
        qDebug()<<"ocv camera object not initialized";
        return false;
    }
    ui->ocvCameraBox->blockSignals(true);
    ui->ocvCameraBox->clear();
    QList<int> ocvCameras=ocvCamera->getAvailableCameras();
    if(ocvCameras.count()>0){
        int storedCamera=settings->value("ocvcamera",0).toInt();
        int camera=0;
        int cameraIndex=0;
        foreach (const int cam, ocvCameras) {
            ui->ocvCameraBox->addItem(QString::number(cam), cam);
            if(cam==storedCamera){
                camera=cam;
                cameraIndex=ui->ocvCameraBox->count()-1;
            }
            qDebug()<<cam;
        }
        ui->ocvCameraBox->setCurrentIndex(cameraIndex);
        //activate camera
        on_ocvCameraBox_currentIndexChanged(cameraIndex);
    }
    ui->ocvCameraBox->blockSignals(false);
    ui->ocvNImagesSpin->blockSignals(true);
    ui->ocvNImagesSpin->setValue(settings->value("ocvnimages",1).toInt());
    ui->ocvNImagesSpin->blockSignals(false);
    ui->ocvNIgnoreSpin->blockSignals(true);
    ui->ocvNIgnoreSpin->setValue(settings->value("ocvnignores",1).toInt());
    ui->ocvNIgnoreSpin->blockSignals(false);

    return true;
}
#endif
void CameraConfig::on_ocvCameraBox_currentIndexChanged(int index)
{
    //ocvCamera->setCamera(camera);
    //qDebug()<<
    //           ocvCamera->getResolutions();
#ifdef OCVCAM
    int newCamera=ui->ocvCameraBox->itemData(index).toInt();
    int oldCamera=ocvCamera->getDeviceId();
    qDebug()<<"ocvCameraBox"<<oldCamera<<newCamera;
    if(newCamera!=oldCamera)
        ocvCamera->setCamera(newCamera);
    settings->setValue("ocvcamera",newCamera);
    QList<QSize> resolutions = ocvCamera->getResolutions();
    ui->ocvCameraResolutionBox->blockSignals(true);
    QSize resolution=ocvCamera->getResolution();
    qDebug()<<"old resolution"<<resolution;
    ui->ocvCameraResolutionBox->clear();
    int idx=-1;
    foreach(const QSize & size, resolutions){
        ui->ocvCameraResolutionBox->addItem(QString("(%1,%2)").arg(size.width()).arg(size.height()),size);
        if(size==resolution){
            ui->ocvCameraResolutionBox->setCurrentIndex(ui->ocvCameraResolutionBox->count()-1);
            idx=ui->ocvCameraResolutionBox->count()-1;
            settings->setValue("ocvcameraresolution",ocvCamera->getResolution());
        }
    }
    if(idx<0){
        idx=ui->ocvCameraResolutionBox->count()-1;
        ui->ocvCameraResolutionBox->setCurrentIndex(ui->ocvCameraResolutionBox->count()-1);
    }
    on_ocvCameraResolutionBox_currentIndexChanged(idx);
    ui->ocvCameraResolutionBox->blockSignals(false);
#endif
}

void CameraConfig::on_ocvCameraResolutionBox_currentIndexChanged(int index)
{
#ifdef OCVCAM
    QSize size=ui->ocvCameraResolutionBox->itemData(index).toSize();
    ocvCamera->setResolution(size);
    settings->setValue("ocvcameraresolution",ocvCamera->getResolution());
#endif
}

void CameraConfig::on_ocvNIgnoreSpin_valueChanged(int arg1)
{
#ifdef OCVCAM
    settings->setValue("ocvnignores",arg1);
    ocvCamera->setNIgnore(arg1);
#endif
}

void CameraConfig::on_ocvNImagesSpin_valueChanged(int arg1)
{
#ifdef OCVCAM
    settings->setValue("ocvnimages",arg1);
    ocvCamera->setNImages(arg1);
#endif
}

