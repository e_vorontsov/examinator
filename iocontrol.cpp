#include "iocontrol.h"
#include <QSerialPortInfo>
#include <QDebug>

IOControl::IOControl(QObject *parent) : QObject(parent)
{
    connect(&port, SIGNAL(readyRead()), this, SLOT(readData()));
}

bool IOControl::setPort(QString portName)
{
    qDebug()<<"setPort"<<portName;
    auto lst=QSerialPortInfo::availablePorts();
    if(portName.isEmpty()){
        qDebug()<<"no port name provided, detected"<<lst.count()<<"ports";
        foreach (auto item, lst) {
           qDebug()<<"detected"<<item.portName()<<item.description()<<item.manufacturer()<<item .systemLocation();
        }
        return false;
    }
    port.setPortName(portName);
    bool ok = port.open(QIODevice::ReadWrite);
    if(!ok){
        foreach (auto item, lst) {
            if((item.portName().contains(portName, Qt::CaseInsensitive))||
                    (item.description().contains(portName, Qt::CaseInsensitive))||
                    (item.manufacturer().contains(portName, Qt::CaseInsensitive))||
                    (item.systemLocation().contains(portName, Qt::CaseInsensitive)))
            {
                qDebug()<<"using"<<item.portName()<<item.description()<<item.manufacturer()<<item .systemLocation();
                port.setPortName(item.portName());
                break;
            }
            qDebug()<<"not using"<<item.portName()<<item.description()<<item.manufacturer()<<item .systemLocation();
        }
        port.setBaudRate(QSerialPort::Baud9600);
        ok = port.open(QIODevice::ReadWrite);
        if(!ok)
            qDebug()<<"can't open port"<<port.portName()<<port.error()<<port.isOpen()<<port.baudRate()<<port.dataBits()<<port.openMode();
    }
    return ok;
}

void IOControl::readData()
{
    if(port.bytesAvailable()<=0)
        return;
    QByteArray data;
    data=port.read(1);
    qDebug()<<"readData"<<data;
    if(data=="\n"){
        if(readBuffer=="T")

            emit start();
        else
            emit newData(readBuffer);
        readBuffer.clear();
    } else {
        readBuffer.append(data);
    }
    //maybe there's more data in the buffer - retrieve it
    readData();
}
