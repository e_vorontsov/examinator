/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LOGGER_H
#define LOGGER_H

#include <QFile>
#include <QMessageBox>
#include <QObject>
enum LOGVERBOSITY {FATAL,CRITICAL,IMPORTANT,INFO,VERBOSE};

class Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = nullptr);
    ~Logger();
    bool setLogFile(QString fileName);
    void setLogVerbosity(LOGVERBOSITY verbosity);
signals:

public slots:
    void logMessage(QString message, LOGVERBOSITY criticality);
private:
    QFile logFile;
    LOGVERBOSITY logVerbosity=INFO;
};

#endif // LOGGER_H
