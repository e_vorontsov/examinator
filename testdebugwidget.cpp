#include "testdebugwidget.h"
#include "ui_testdebugwidget.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QClipboard>
#include <QElapsedTimer>

TestDebugWidget::TestDebugWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::testDebugWidget)
{
    ui->setupUi(this);
    boardStatModel.setImages(&images);
    imageStatModel.setImages(&images);
    ui->masterListView->setModel(&boardStatModel);
    ui->detailTreeView->setModel(&imageStatModel);
    //ui->detailTreeView->header()->hide();
    ui->detailTreeView->setRootIsDecorated(false);
    imageStatModel.setHeaderData(0, Qt::Horizontal, tr("componenta"));
    imageStatModel.setHeaderData(1, Qt::Horizontal, tr("scor"));
    //ui->masterListView->selectionModel()->currentChanged();
    connect(ui->masterListView->selectionModel(),SIGNAL(currentChanged(const QModelIndex&,const QModelIndex&)),
            this, SLOT(masterViewCurrentChanged(const QModelIndex&,const QModelIndex&)));
    connect(&boardStatModel, SIGNAL(dataModified(int)), this, SLOT(updateDetail(int)));
    connect(&imageStatModel, SIGNAL(dataModified(uint,int)), this, SLOT(updateMaster(uint,int)));
    timer.setInterval(2000);
    timer.setSingleShot(true);
    connect(&timer, SIGNAL(timeout()), this, SLOT(on_saveBtn_clicked()));

}

TestDebugWidget::~TestDebugWidget()
{
    delete ui;
}

void TestDebugWidget::on_loadDirBtn_clicked()
{
    QString fname=QFileDialog::getExistingDirectory(this, tr("director cu imagini de analizat"),dir.absolutePath());
    qDebug()<<"fname"<<fname;
    if(fname.isEmpty())
        return;
    boardStatModel.prepareForUpdate();
    images.clear();
    dir.setPath(fname);
    dir.setFilter(QDir::Files);
    ui->dirSelEdit->setText(fname);
    QStringList files=dir.entryList();
    QStringList imageExtensions;
    imageExtensions<<"png"<<"jpg"<<"jpeg"<<"bmp";
    qDebug()<<files;
    for(unsigned i=0; i<dir.count(); i++){
        QString fname=dir[i];
        QFileInfo info(fname);
        //qDebug()<<dir.absolutePath()<<fname<<info.fileName()<<info.baseName()<<info.suffix();
        if(!imageExtensions.contains(info.suffix(),Qt::CaseInsensitive))
            continue;
        QString fileName=dir.absolutePath()+"/"+fname;
        QString rName=info.completeBaseName()+".result";
        bool hasResults=false;
        foreach(QString name, files){
            if(name.compare(rName,Qt::CaseInsensitive)==0){
                rName=name;
                hasResults=true;
                break;
            }
        }
        ImageResults res;
        res.resultsName=dir.absolutePath()+"/"+rName;
        res.imageName=fileName;
        if(hasResults){
            std::ifstream in((res.resultsName).toStdString(), std::ifstream::in|std::ifstream::binary);
            res.unknown=false;
            try {
                deserialize(res.boards,in);
                try {
                    dlib::deserialize(res.decision, in);
                } catch(...){
                    //no decision in file for board
                    res.calculateDecision();
                }
            } catch(...){
                qDebug()<<"deserialize of saved results failed";
                res.boards.clear();
                res.unknown=true;
            }

        }
        images.append(res);
    }
    boardStatModel.updateFinished();
    if(!ui->masterListView->currentIndex().isValid()){
        if(images.count()>0)
            ui->masterListView->setCurrentIndex(boardStatModel.index(0));
    }
}

BoardStatModel::BoardStatModel(QObject *parent)
{

}

QVariant BoardStatModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // not needed
    Q_UNUSED(section);
    Q_UNUSED(orientation);
    Q_UNUSED(role);
    return QVariant();
}

int BoardStatModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    if(images==nullptr)
        return 0;
    return images->count();

}

int BoardStatModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 2;

}

QVariant BoardStatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    if(index.row()<images->count()){
        if(role == Qt::DisplayRole){
            return (*images)[index.row()].imageName;
        } else if(role==Qt::BackgroundRole){
            if((*images)[index.row()].unknown)
                return QVariant();
            if((*images)[index.row()].boards.count()==0){
                //board not found
                switch((*images)[index.row()].decision){
                case 0:
                    return QColor(Qt::red);
                case 1:
                    return QColor(Qt::yellow);
                default:
                     return QColor(Qt::red).light(175);
                }
            }
            bool imageOk=true;
            for(int i=0; i<(*images)[index.row()].boards.count(); i++){
                if((*images)[index.row()].boards[i].status!=DetectedComponent::Detected)
                    imageOk=false;
            }
            if(imageOk){
                switch((*images)[index.row()].decision){
                case 0:
                    return QColor(Qt::cyan);
                case 1:
                    return QColor(Qt::green);
                default:
                     return QColor(Qt::green).light(175);
                }
            }
            switch((*images)[index.row()].decision){
            case 0:
                return QColor(Qt::red);
            case 1:
                return QColor(Qt::yellow);
            default:
                 return QColor(Qt::red).light(175);
            }

        } else if(role == Qt::CheckStateRole){
            if(index.column()==0){
                switch ( (*images)[index.row()].decision){
                case 1: return(Qt::Checked);
                case 0: return(Qt::Unchecked);
                default: return(Qt::PartiallyChecked);
                }
            }
        }

    } else {
        qDebug()<<"invalid index row"<<index.row();
    }
    return QVariant();

}

Qt::ItemFlags BoardStatModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);
     if (index.isValid()){
         if(index.column()==0)
            return defaultFlags | Qt::ItemIsUserCheckable | Qt::ItemIsTristate;
     }
     return defaultFlags;
}

void BoardStatModel::setImages(QList<ImageResults> *images)
{
    this->images=images;
}

void TestDebugWidget::setBoardDefPtr(BoardDef **boardDefPtr)
{
    this->boardDefPtr=boardDefPtr;
}

void BoardStatModel::prepareForUpdate()
{
    beginResetModel();
}

void BoardStatModel::updateFinished()
{
    endResetModel();
}

void TestDebugWidget::on_boardTestBtn_clicked()
{
    if(boardDefPtr==nullptr)
        return;
    if((*boardDefPtr)==nullptr)
        return;
    auto lst=ui->masterListView->selectionModel()->selectedIndexes();
    if(lst.isEmpty())
        lst<<ui->masterListView->currentIndex();

    foreach(auto &idx, lst){
        if(!idx.isValid()){
            qDebug()<<"invalid index";
            continue;
        }
        if(idx.row()>=images.count()){
            qDebug()<<"invalid index";
            continue;
        }
        ImageResults& res=images[idx.row()];
        bool testOk=processOneImage(res);
        emit imageChanged(res.imageName, res.boards, testOk);
        boardStatModel.update(idx);
        imageStatModel.setImage(idx.row());
        qApp->processEvents();
    }
}

bool TestDebugWidget::processOneImage(ImageResults &res)
{
    if((boardDefPtr==nullptr)||((*boardDefPtr)==nullptr))
        return false;
    QImage img;
    img.load(res.imageName);
    bool testOk=false;
    res.boards=(*boardDefPtr)->runOnImage(img,false, testOk, res.resultsName,
                                          res.boards, &(res.decision));
    res.unknown=false;
    return testOk;
}

void TestDebugWidget::updateDetail(int position)
{
    if(!ui->masterListView->currentIndex().isValid())
        return;
    timer.start();
    if(position==ui->masterListView->currentIndex().row())
        imageStatModel.setImage(position);
}

void TestDebugWidget::updateMaster(unsigned image, int decision)
{
    if(images.count()<=image){
        //illegal value
        return;
    }
    timer.start();
    if(decision==0){
        //wrong detection, so entire image is wrongly assesed
        images[image].decision=0;

    } else {
        images[image].calculateDecision();
    }
    boardStatModel.update(boardStatModel.index(image));
    return;
}

void TestDebugWidget::on_analyzeBtn_clicked()
{
    QElapsedTimer timer;
    timer.start();
    if(boardDefPtr==nullptr)
        return;
    if((*boardDefPtr)==nullptr)
        return;
    for(int i=0; i<images.count(); i++){
        images[i].unknown=true;
    }
    for(int i=0; i<images.count(); i++){
        ImageResults& res=images[i];
        processOneImage(res);
        boardStatModel.update(boardStatModel.index(i));
        qApp->processEvents();
    }
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid()){
        if(images.count()>0){
            idx=boardStatModel.index(0);
        }else
            return;
        ui->masterListView->setCurrentIndex(idx);
    }
    qDebug()<<"analysis took: "<<timer.elapsed();
    if(idx.row()>=images.count()){
        qDebug()<<"invalid index";
        return;
    }
    masterViewCurrentChanged(idx,idx);
}

void TestDebugWidget::on_delBtn_clicked()
{
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid())
        return;
    int i=idx.row();
    auto btn=QMessageBox::question(this, tr("Confirmare ștergere fișier"), tr("Sigur doriți ștergerea definitivă a fișierului ")+images[i].imageName+" ?");
    if(btn!=QMessageBox::Yes)
        return;
    boardStatModel.prepareForUpdate();
    if(!QFile::remove(images[i].imageName))
        qDebug()<<"Can't delete file"<<images[i].imageName;
    QFile::remove(images[i].resultsName);
    images.removeAt(i);
    if(images.count()>i)
        idx=boardStatModel.index(i);
    else
        if(i>0)
            idx=boardStatModel.index(i-1);
        else
            i=-1;
    boardStatModel.updateFinished();
    if(i>=0){
        ui->masterListView->setCurrentIndex(idx);
        masterViewCurrentChanged(idx,idx);

    }
}

void TestDebugWidget::on_copyAndOpenBtn_clicked()
{
    static QString destination;
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid())
        return;
    QFile file(images[idx.row()].imageName);
    if(!file.exists())
        return;
    QFileInfo info(file);
    QString srcFile=info.completeBaseName();

    auto idx2=ui->detailTreeView->currentIndex();
    long long classifierId=-1;
    if(idx2.isValid()){
        if((images[idx.row()].boards.count()>0)&&(images[idx.row()].boards[0].parts.count()>idx2.row())){
            long long compId=images[idx.row()].boards[0].parts[idx2.row()].boardComp.id;
            if((boardDefPtr!=nullptr)&&((*boardDefPtr)!=nullptr)){
                auto lst=(*boardDefPtr)->classifierFromComponent(compId);
                if(lst.count()>0){
                    classifierId=lst[0]->id;
                    //qDebug()<<"classifiers"<<lst.count();
                    //qDebug()<<"images"<<lst[0]->images.count();
                    int n=0;
                    if((n=lst[0]->images.count())>0){
                        QString fname=lst[0]->images[n-1].imagePath;
                        QFileInfo dstInfo(fname);
                        destination=dstInfo.absoluteDir().absolutePath();
                    }
                }
            }
        }
    }

    QString destFile=QFileDialog::getSaveFileName(this, tr("Salvare copie imagine. Apăsați CANCEL pentru a nu copia nimic"),
                                                  destination+"/"+srcFile, tr("Imagini (*.png *.jpg *.jpeg);; Toate fișierele (*.*)"));
    if(!destFile.isEmpty()){
        QFileInfo dstInfo(destFile);
        qDebug()<<destFile<<dstInfo.absoluteDir().absolutePath();
        destination=dstInfo.absoluteDir().absolutePath();
        if(dstInfo.exists()){
            QMessageBox::information(this, tr("copiere abandonată"), tr("Nu se poate copia fișierul, deoarece există deja un fișier cu același nume") );
        } else {
            bool ok=file.copy(destFile);
            if(!ok)
                QMessageBox::information(this, tr("copiere nereușită"), tr("Nu s-a putut copia fișierul") );
        }
        QClipboard *clipboard = QGuiApplication::clipboard();
        clipboard->setText(destFile);
    }
    emit imageForClassifier(classifierId, destFile);
}

void TestDebugWidget::masterViewCurrentChanged(const QModelIndex &idxTo, const QModelIndex & idxFrom)
{
    Q_UNUSED(idxFrom);
    if(idxTo.row()>=images.count()){
        qDebug()<<"invalid index received";
        return;
    }
    qDebug()<<idxTo.row()<<images[idxTo.row()].imageName<<ui->masterListView->currentIndex().row();
    bool imageOk=images[idxTo.row()].boards.count()>0;
    for(int i=0; i<images[idxTo.row()].boards.count(); i++){
        if(images[idxTo.row()].boards[i].status!=DetectedComponent::Detected)
            imageOk=false;
    }
    imageStatModel.setImage(idxTo.row());
    emit imageChanged(images[idxTo.row()].imageName, images[idxTo.row()].boards, imageOk);
}

int ImageStatModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;
    if(images==nullptr)
        return 0;
    if(images->count()==0)
        return 0;
    if(images->count()<=imageNum)
        return 0;
    if((*images)[imageNum].boards.count()<=0)
        return 0;
    return (*images)[imageNum].boards[0].parts.count();
}

int ImageStatModel::columnCount(const QModelIndex &parent) const
{
    if(parent.isValid())
        return 0;
    return 5;
}

QVariant ImageStatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    QList<DetectedComponent> &components=(*images)[imageNum].boards[0].parts;
    //qDebug()<<"data"<<index.row()<<index.column()<<components.count()<<components[index.row()].boardComp.name;
    if(index.row()<components.count()){
        //qDebug()<<"data"<<index.row()<<index.column()<<components.count()<<components[index.row()].boardComp.name;
        if(role == Qt::DisplayRole){
            switch(index.column()){
            case 0:
                return components[index.row()].boardComp.name;
            default:
            case 1:
                return components[index.row()].score[0];
            case 2:
                return components[index.row()].xerr*100;
            case 3:
                return components[index.row()].yerr*100;
            case 4:
                return components[index.row()].scaleerr*100;
            }
        } else if(role == Qt::CheckStateRole){
            if(index.column()==0){
                switch ( components[index.row()].decision){
                case 1: return(Qt::Checked);
                case 0: return(Qt::Unchecked);
                default: return(Qt::PartiallyChecked);
                }
            }
        } else if(role==Qt::BackgroundRole){
            if(index.column()==0){
               if( (components[index.row()].status==DetectedComponent::Detected) ||
                       (components[index.row()].status==DetectedComponent::DetectedFiducial) ||
                       (components[index.row()].status==DetectedComponent::DetectedBarcode)){
                   switch(components[index.row()].decision){
                   case 0:
                       return QColor(Qt::cyan);
                   case 1:
                       return QColor(Qt::green);
                   default:
                        return QColor(Qt::green).light(175);
                   }
                }
               switch(components[index.row()].decision){
               case 0:
                   return QColor(Qt::red);
               case 1:
                   return QColor(Qt::yellow);
               default:
                    return QColor(Qt::red).light(175);
               }
            }
        }
    }
    return QVariant();
}

Qt::ItemFlags ImageStatModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);
     if (index.isValid()){
         if(index.column()==0)
            return defaultFlags | Qt::ItemIsUserCheckable | Qt::ItemIsTristate;
     }
     return defaultFlags;
}

void ImageStatModel::setImages(QList<ImageResults> *images)
{
    this->images=images;
}

void ImageStatModel::setImage(int image)
{
    this->beginResetModel();
    imageNum=image;
    this->endResetModel();
}

void TestDebugWidget::on_openBoardBtn_clicked()
{
    qDebug()<<"showBoard"<<boardDefPtr<<*boardDefPtr;
    if((boardDefPtr==nullptr)||((*boardDefPtr)==nullptr))
        return;
    long long boardId=(*boardDefPtr)->id;
    emit showBoard(boardId);

}

void TestDebugWidget::on_saveBtn_clicked()
{
    for(int i=0; i<images.count(); i++){
        if(!images[i].dirty)
            continue;
        std::ofstream out((images[i].resultsName).toStdString(), std::ofstream::out|std::ofstream::binary);
        if(out.is_open()){
            qDebug()<<"saving"<<images[i].resultsName;
            serialize(images[i].boards, out);
            dlib::serialize(images[i].decision, out);
            out.close();
            images[i].dirty=false;
        }
    }
}

void ImageResults::calculateDecision()
{
    decision=BoardDef::calculateDecision(this->boards);
}

void TestDebugWidget::on_detailTreeView_doubleClicked(const QModelIndex &index)
{
    if(!index.isValid())
        return;
    auto idx=ui->masterListView->currentIndex();
    if(!idx.isValid())
        return;
    if((images[idx.row()].boards.count()>0)&&(images[idx.row()].boards[0].parts.count()>index.row())){
        auto comp=images[idx.row()].boards[0].parts[index.row()];
        double x, y;
        qDebug()<<"comp"<<comp.x<<comp.y<<comp.w<<comp.h<<comp.orientation;
        switch(comp.orientation){
        case CompDefRect::E:
        case CompDefRect::W:
        case CompDefRect::EW:
            x=comp.x+comp.h/2;
            y=comp.y+comp.w/2;
            break;
        default:
            x=comp.x+comp.w/2;
            y=comp.y+comp.h/2;
            break;
        }
        emit centerAt(x, y);
    }
}
