/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "boardcomptype.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

BoardCompType::BoardCompType()
{
    id=-1;
    componentType=COMPONENT;
}

BoardCompType::~BoardCompType()
{
    /* not needed for shared pointer
    foreach(QSharedPointer<MemCompDef>c, classifiers){
        delete c;
    }
    */
}

BoardCompType::BoardCompType(QSqlRecord record, bool loadForTest)
{
    id=record.value("id").toLongLong();
    name=record.value("name").toString();
    componentType=static_cast<BoardCompType::ComponentType>(record.value("comptype").toInt());
    if((componentType>=MAXCOMPONENTTYPE)||(componentType<0))
        componentType=COMPONENT;
    QSqlQuery query;
    QString sql;
    sql="SELECT * FROM boardcomponents WHERE comptype="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    while(query.next()){
        components.append(BoardComp(query.record()));
    }
    sql="SELECT * FROM boardcompclassifiers WHERE comptype="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    double classifierAspectRatio=-1;
    while(query.next()){
        QSharedPointer<CompDef> c(new CompDef);
        long long cid=query.value("id").toLongLong();
        long long classifier=query.value("classifier").toLongLong();
        c->loadFromDB(classifier,false);
        if(loadForTest)
            c->prepareToRun();
        if(classifierAspectRatio<0){
            classifierAspectRatio=c->aspectRatio;
        } else {
            if(classifierAspectRatio!=c->aspectRatio){
                //just don't keep the classifier. It will be deleted when the board definition is saved.
                qDebug()<<"ignoring classifier"<<c->name<<"having aspect ratio"<<c->aspectRatio<<"instead of"<<classifierAspectRatio;
                continue;
            }
        }
        c->loadFHOGDetector();
        classifiers.insert(cid,c);
    }
    if(classifierAspectRatio>0){
        for(int i=0; i<components.count(); i++){
            components[i].applySettings(components[i].orientation, components[i].N_E_S_V, classifierAspectRatio, components[i].rectangleType);
        }
    }
}

void BoardCompType::saveToDB(long long boardDef)
{
    QSqlQuery query;
    QString sql;
    bool newId=false;
    //save component type
    if(id>=0){
        //update already existing board component type
        sql="UPDATE boardcomptypes SET name='"+name+
                "', comptype="+QString::number(componentType)+
                " WHERE id="+ QString::number(id);
    } else {
        sql="INSERT INTO boardcomptypes (name, comptype, board)"
                "VALUES ('"+name+"', "+QString::number(componentType)+
                ", "+QString::number(boardDef)+")";
    }
    qDebug()<<"saving boardcomptype"<<sql;
    if(!query.exec(sql)){
        qDebug()<<query.lastError();
        return;
    }
    //keep id if it's new
    if(id<0){
        newId=true;
        id=query.lastInsertId().toLongLong();
    } else {
        //delete deleted rects and alternative classifiers from database
        QString idList;
        for(int i=0; i<components.count(); i++) {
            if(components[i].id<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(components[i].id));
        }
        sql="DELETE FROM boardcomponents WHERE comptype="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        idList.clear();
        foreach(long long cid, classifiers.keys()) {
            if(cid<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(cid));
        }
        sql="DELETE FROM boardcompclassifiers WHERE comptype="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
    }
    // only new classifiers have to be saved, as there is no way to modify existing classifier alternatives
    qDebug()<<"saving classifiers";
    if(newId){
        for(;;){
            auto it=classifiers.begin();
            while(it.key()==-1){
                it++;
                if(it==classifiers.end())
                    break;
            }
            if(it==classifiers.end())
                break;
            QSharedPointer<CompDef> comp(it.value());
            classifiers.insertMulti(-1,comp);
            classifiers.erase(it);
        }
    }
    for(;;){
        auto it=classifiers.find(-1);
        if(it==classifiers.end())
            break;
        QSharedPointer<CompDef> comp(it.value());
        long long classifier=comp->id;
        sql="INSERT INTO boardcompclassifiers (classifier, comptype)"
                "VALUES("+QString::number(classifier)+","+
                QString::number(id)+")";
        qDebug()<<sql;
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        long long altid=query.lastInsertId().toLongLong();
        classifiers.insert(altid,comp);
        classifiers.erase(it);
    }
    for(int i=0; i<components.count(); i++){
        if(newId)
            components[i].id=-1;
        components[i].saveToDB(id);
    }
}

BoardComp *BoardCompType::addNewComp(const double x, const double y, const BoardComp &recentComp)
{
    components.append(recentComp);
    BoardComp * comp = &(components.last());
    comp->x=x;
    comp->y=y;
    comp->id=-1;
    return comp;
}

double BoardCompType::aspectRatio()
{
    if(classifiers.count()>0)
        return classifiers.first().data()->aspectRatio;
    return -1;
}

double BoardCompType::minArea() const
{

    if(classifiers.count()>0){
        double minArea=classifiers.first().data()->minArea;
        for(int upsample=classifiers.first().data()->upsample; upsample>0; upsample--){
            minArea /= 4;
        }
        return minArea;
    }
    return 6400;
}

unsigned BoardCompType::upsample() const
{
    if(classifiers.count()>0){
        return classifiers.first().data()->upsample;
    }
    return 0;
}
