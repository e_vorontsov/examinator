/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "compdefrect.h"
#include <cmath>
#include <QDebug>
#include <algorithm>
#include <assert.h>

CompDefRect::CompDefRect()
{
    id=-1;
    rectangleType=POSITIVE;
    x=y=w=h=0;
    orientation=N;
}

CompDefRect::CompDefRect(double x, double y, double w, double h, Orientation orientation, RectangleType rType)
{
    id=-1;
    this->x=x;
    this->y=y;
    this->w=w;
    this->h=h;
    this->orientation=orientation;
    rectangleType=rType;
}

CompDefRect::CompDefRect(QSqlRecord record)
{
    id=record.value("id").toLongLong();
    orientation=static_cast<CompDefRect::Orientation>(record.value("orientation").toInt());
    rectangleType=static_cast<CompDefRect::RectangleType>(record.value("rectangletype").toInt());
    x=record.value("x").toDouble();
    y=record.value("y").toDouble();
    w=record.value("w").toDouble();
    h=record.value("h").toDouble();

}

double CompDefRect::x2() const
{
    if((E==orientation)||(W==orientation)||(EW==orientation))
        return x+h;
    return x+w;
}

double CompDefRect::y2() const
{
    if((E==orientation)||(W==orientation)||(EW==orientation))
        return y+w;
    return y+h;
}

bool CompDefRect::update(double x, double y, double w, double h)
{
    qDebug()<<"update"<<x<<y<<w<<h;
    bool changed=false;
    if((this->x!=x)||(this->y!=y))
        changed=true;
    this->x=x;
    this->y=y;
    if((orientation==E)||(orientation==W)||(orientation==EW)){
        if((this->w!=h)||(this->h!=w))
            changed=true;
        this->w=h;
        this->h=w;
    } else {
        if((this->w!=w)||(this->h!=h))
            changed=true;
        this->w=w;
        this->h=h;
    }
    return changed;
}

void CompDefRect::applySettings(Orientation _orientation, OrientationMode orientationMode, qreal aspectRatio,
                                   RectangleType rectangleType)
{
    qDebug()<<"applySettings"<<orientation<<_orientation<<orientationMode<<x<<y<<w<<h;
    switch(orientationMode){
    case ANY:
        orientation=NONE;
        break;
    case H_V:
        if((_orientation==E)||(_orientation==W)||(_orientation==EW))
            orientation=EW;
        else
            orientation=NS;
        break;
    case N_E_S_V:
    default:
        if(_orientation==W){
            orientation=W;
        } else if((_orientation==E)||(_orientation==EW)){
            orientation=E;
        } else if(_orientation==S){
            orientation=S;
        } else
            orientation=N;
        break;
    }

    /*
    switch(_orientation){
    case N:
    case S:
    case NS:
        if((orientation==E)||(orientation==W)||(orientation==EW))
            std::swap(w,h);
        orientation=_orientation;
        if(aspectRatio>0)
            h=w/aspectRatio;
        break;
    case E:
    case W:
    case EW:
        if((orientation==N)||(orientation==S)||(orientation==NS))
            std::swap(w,h);
    case NONE:
        if(aspectRatio>0)
            h=w/aspectRatio;
        orientation=_orientation;
        break;
    }
*/
    if((rectangleType==CompDefRect::POSITIVE)||
    (rectangleType==CompDefRect::BOARDRECT)||
    (rectangleType==CompDefRect::BOARDCOMP)){
        if(aspectRatio>0){
            h=w/aspectRatio;
        }
    } else {
        //do nothing
    }
    assert(w>0);
    assert(h>0);
    this->rectangleType=rectangleType;
}

double CompDefRect::xCenter() const
{
    switch(orientation){
    case E:
    case W:
    case EW:
        return x+h/2;
    default:
        return x+w/2;
    }
}

double CompDefRect::yCenter() const
{
    switch(orientation){
    case E:
    case W:
    case EW:
        return y+w/2;
    default:
        return y+h/2;
    }

}

int CompDefRect::orientationDifference(CompDefRect::Orientation src, CompDefRect::Orientation dst)
{
    switch(src){
    default:
        return 0;
    case N:
        switch(dst){
        case N:
        case NS:
        default:
            return 0;
        case E:
        case EW:
            return 1;
        case S:
            return 2;
        case W:
            return 3;
        }
    case NS:
        switch(dst){
        case N:
        case NS:
        case S:
        default:
            return 0;
        case E:
        case EW:
        case W:
            return 1;
        }
    case E:
        switch(dst){
        case N:
            return 3;
        case E:
        case EW:
        default:
            return 0;
        case S:
        case NS:
            return 1;
        case W:
            return 2;
        }
    case EW:
        switch(dst){
        case N:
            return 1;
        case E:
        case EW:
        default:
            return 0;
        case S:
        case NS:
            return 1;
        case W:
            return 0;
        }
    case S:
        switch(dst){
        case N:
            return 2;
        case E:
            return 3;
        case S:
        case NS:
        default:
            return 0;
        case W:
        case EW:
            return 1;
        }
    case W:
        switch(dst){
        case N:
        case NS:
            return 1;
        case E:
            return 2;
        case S:
            return 3;
        case W:
        case EW:
        default:
            return 0;
        }
    }

}

void CompDefRect::addOrientationDifference(int diff)
{
    assert((diff>=0)&&(diff<=3));
    if(diff==0)
        return;
    switch(orientation){
    case N:
        if(diff==1)
            orientation=E;
        else if(diff==2)
            orientation=S;
        else
            orientation=W;
        break;
    case E:
        if(diff==1)
            orientation=S;
        else if(diff==2)
            orientation=W;
        else
            orientation=N;
        break;
    case S:
        if(diff==1)
            orientation=W;
        else if(diff==2)
            orientation=N;
        else
            orientation=E;
        break;
    case W:
        if(diff==1)
            orientation=N;
        else if(diff==2)
            orientation=E;
        else
            orientation=S;
        break;
    case NS:
        if((diff==1)||(diff==3))
            orientation=EW;
        break;
    case EW:
        if((diff==1)||(diff==3))
            orientation=NS;
        break;
    default:
        /* do nothing*/;
    }
    return;
}

double CompDefRect::checkOverlap(const CompDefRect &other, bool useUnion) const
{
    double intersection = std::max(0.0, -std::max(this->x, other.x) + std::min(this->x2(), other.x2())) *
            std::max(0.0, -std::max(this->y, other.y) + std::min(this->y2(), other.y2()));
    double baseArea=std::min(std::abs(this->w*this->h), std::abs((double)(other.w*other.h)));
    if(useUnion)
        baseArea=std::abs(this->w*this->h) + std::abs(other.w*other.h) - intersection;
    //qDebug()<<"checkOverlap"<<x<<y<<w<<h<<x2()<<y2()<<orientation;
    //qDebug()<<other.x<<other.y<<other.w<<other.h<<other.x2()<<other.y2()<<other.orientation;
    //qDebug()<<intersection<<baseArea;
    if(baseArea<=0)
        return 0;
    return intersection/baseArea;
}
