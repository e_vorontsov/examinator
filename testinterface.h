/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TESTINTERFACE_H
#define TESTINTERFACE_H

#include "widget.h"
#include "mscene.h"
#include "myftp.h"
#include "checksn.h"
#include "ipcamera.h"
#include "mycamera.h"
#ifdef OCVCAM
#include "opencvcam.h"
#endif

#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QWidget>
#include <QCommandLineParser>
#include "logger.h"
#include "iocontrol.h"

extern QCommandLineParser parser;

namespace Ui {
class TestInterface;
}
enum SaveImages {SAVENEVER,SAVEFAIL, SAVEALL};

class TestInterface : public QWidget
{
    Q_OBJECT

public:
    explicit TestInterface(QWidget *parent = 0);
    ~TestInterface();

private slots:
    void on_editorBtn_clicked();

    void on_loadBtn_clicked();

    void on_horizontalSlider_valueChanged(int position);

    void on_saveBtn_clicked();
    /// slot used to display the board select dialog and load the selected board if appropiate
    void on_boardBtn_clicked();
    /// camera trigger slot
    void on_cameraBtn_clicked();
    void getPixmap(QPixmap pixmap);
    //id not needed, present only because qCamera signal has it
    void getImage(int id, const QImage & img);
    void getImage(const QImage & img);
    void cameraIsReady(bool ready);
    void on_playBtn_clicked();
    void on_runButton_clicked();
    void runTestSlot();
    void on_cameraBtn_triggered(QAction *arg1);

    void on_snEdit_returnPressed();
    void addToClassifier(long long classifierId, const QString &imageName);
    void showBoard(long long boardId);
public slots:
    void showTestResult(QList<DetectedComponent> &boardRects, const bool testOk);
    void showImage(QString fileName);
    void showImageResult(QString fileName,QList<DetectedComponent> &boardRects, const bool testOk);
    void centerAt(double x, double y);

private:
    Ui::TestInterface *ui;
    Widget * editor=nullptr;
    MScene testScene;
    BoardDef * boardDef=nullptr;
    void createDatabase();
    QCamera * camera=nullptr;
    QCameraImageCapture * imageCapture=nullptr;
    MyCamera myCamera;
    IpCamera ipCamera;
#ifdef OCVCAM
    OpenCVCam ocvCamera;
#endif
    void configureCamera(const QByteArray &deviceName);
    void configureCamera();
    bool pictureRequested=false;
    QString parseArguments(void);
    void selectBoard(QString boardName="");
    MyFtp myFtp;
    QdmSNCheck snCheck;
    QString serialNumber;
    QString ioControlPort;
    bool makeProtocol(QString serialNumber, bool boardOk,
                      const QList<DetectedComponent> & foundComponents);
    /**
     * @brief check if the specified sqlite table has the specified field
     * @param tableName
     * @param fieldName
     * @return true if the table has the field
     */
    bool sqliteTableHasField(QString tableName, QString fieldName);
    QSettings * writableSettings;
    //settings
    bool displaySearchFrames=false;
    int autoSaveOption=1;
    QString autoSaveDir;
    bool showFiducials=false;
    //storage for inspection results
    QList<DetectedComponent> boardRects;
    enum RunTask {IDLE, CAPTURE, ANALYZE};
    RunTask runTask=IDLE;
    Logger logger;
    /// flag indicating that serial numbers are supposed to be read from the picture, instead of being provided by the keyboard
    bool barCodeMode=false;
    IOControl ioControl;
    /// save image to autosave location. This is usually done in a background thread, to save running time
    void saveImage(const QPixmap img, const QList<DetectedComponent> boardRects);
signals:
    void logMsg(QString message, LOGVERBOSITY verbosity);
    void imageLoaded(QImage img);
};

#endif // TESTINTERFACE_H
