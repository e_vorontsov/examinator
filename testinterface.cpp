/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "testinterface.h"
#include "ui_testinterface.h"
#include "boardselect.h"
#include <QFileDialog>
#include <QDebug>
#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMenu>
#include <QMessageBox>
#include "cameraconfig.h"
#include "logger.h"
#include <QtConcurrent>


//libgstreamer-plugins-bad1.0-dev
//libqt5gstreamer-1.0-0
//libqt5gstreamer-dev
//gstreamer1.0-plugins-bad

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#endif

QCommandLineParser parser;

TestInterface::TestInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestInterface)
{
    QString inspectionProgram=parseArguments();


    ui->setupUi(this);
    /*
    ui->editorBtn->setText("");
    ui->editorBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_FileIcon));
    ui->loadBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_DirIcon));
    ui->saveBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_DriveFDIcon));
    ui-> cameraBtn->setIcon(QIcon::fromTheme("camera-photo",ui->editorBtn->style()->standardIcon(QStyle::SP_ToolBarVerticalExtensionButton)));
    ui->playBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_MediaPlay));
    ui->boardBtn->setIcon(ui->editorBtn->style()->standardIcon(QStyle::SP_BrowserReload));
    */
    ui->testView->setScene(&testScene);
    ui->boardsButtonWidget->setVisible(false);
    QMenu *menu = new QMenu();
    QAction *testAction = new QAction(tr("configurează camera"), this);
    menu->addAction(testAction);
    ui->cameraBtn->setMenu(menu);
    createDatabase();
    connect(&myCamera,SIGNAL(imageCaptured(QPixmap)),this,SLOT(getPixmap(QPixmap)),Qt::QueuedConnection);
    connect(&ipCamera,SIGNAL(imageCaptured(QPixmap)),this,SLOT(getPixmap(QPixmap)),Qt::QueuedConnection);
    connect(this, SIGNAL(imageLoaded(QImage)), this, SLOT(getImage(QImage)));
#ifdef OCVCAM
    connect(&ocvCamera,SIGNAL(imageCaptured(QImage)),this,SLOT(getImage(QImage)),Qt::QueuedConnection);
#endif
    writableSettings=new QSettings(QSettings::IniFormat,QSettings::SystemScope, qApp->organizationName(),qApp->applicationName());
    if(!writableSettings->isWritable()){
        qDebug()<<"system settings not writable:"<<writableSettings->fileName();
        delete writableSettings;
        writableSettings=new  QSettings(QSettings::IniFormat,QSettings::UserScope, qApp->organizationName(),qApp->applicationName());
        if(!writableSettings->isWritable()){
            qDebug()<<"settings not writable";
        }
        qDebug()<<"settings used:"<<writableSettings->fileName();
    }
    ipCamera.setUrl(writableSettings->value("ipwebcam").toString());
    if(writableSettings->value("cameratype").toString()!="qcamera"){
        //disable qCamera if something else is used
        myCamera.setCamera("",false);
    }
    displaySearchFrames=writableSettings->value("displaySearchFrames", false).toBool();
    autoSaveOption=writableSettings->value("autoSaveOption", 1).toInt();
    autoSaveDir=writableSettings->value("autoSaveDir").toString();
    showFiducials=writableSettings->value("showFiducials",false).toBool();
    QString dataLocation;
#if QT_VERSION >= 0x050400
    dataLocation=QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
#elif QT_VERSION >= 0x050000
    dataLocation=QStandardPaths::writableLocation(QStandardPaths::DataLocation);
#else
    dataLocation=QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif
    if(autoSaveDir.isEmpty()){
        autoSaveDir=dataLocation+"/failedImages";
    }
    if(!inspectionProgram.isEmpty()){
        qDebug()<<inspectionProgram;
        selectBoard(inspectionProgram);
    }
    ui->resultLbl->setText("");
    //ui->horizontalSlider->blockSignals(true);
    ui->horizontalSlider->setValue(writableSettings->value("testinterfacescale",100).toInt());
    ui->horizontalSlider->setToolTip(QString::number(ui->horizontalSlider->value())+"%");
    //ui->horizontalSlider->blockSignals(false);
    myFtp.createConfigItem("LOGFILE",dataLocation+"/log/examinator.log",writableSettings);
    myFtp.createConfigItem("LOGVERBOSITY",INFO,writableSettings);
    logger.setLogFile(writableSettings->value("LOGFILE").toString());
    logger.setLogVerbosity(static_cast<LOGVERBOSITY>( writableSettings->value("LOGVERBOSITY",INFO).toInt()));
    connect(this, SIGNAL(logMsg(QString,LOGVERBOSITY)), &logger, SLOT(logMessage(QString,LOGVERBOSITY)));
    connect(&myFtp, SIGNAL(sendToLog(QString,LOGVERBOSITY)), &logger, SLOT(logMessage(QString,LOGVERBOSITY)));
    myFtp.createConfigItem("maxdnnarea",4e6, writableSettings);
    CompDef::maxDNNArea=writableSettings->value("maxdnnarea",CompDef::maxDNNArea).toUInt();
    myFtp.createConfigItem("iocontrolport","",writableSettings);
    ioControlPort=writableSettings->value("iocontrolport","").toString();
    ioControl.setPort(ioControlPort);
    connect(&ioControl, SIGNAL(start()), this, SLOT(on_runButton_clicked()));
    myFtp.createConfigItem("pathconverter","//hugo/genrad/boards=G:/boards",writableSettings);
    ui->splitter->setSizes(QList<int>({1,0}));
    connect(ui->testDebugWidget, SIGNAL(imageChanged(QString, QList<DetectedComponent> &, bool)), this, SLOT(showImageResult(QString, QList<DetectedComponent> &, bool)));
    ui->testDebugWidget->setBoardDefPtr(&boardDef);
    connect(ui->testDebugWidget, SIGNAL(imageForClassifier(long long, const QString &)), this, SLOT(addToClassifier(long long, const QString &)));
    connect(ui->testDebugWidget, SIGNAL(showBoard(long long)), this, SLOT(showBoard(long long)));
    connect(ui->testDebugWidget, SIGNAL(centerAt(double, double)), this, SLOT(centerAt(double, double)));
    qDebug()<<"constructor done";

}

TestInterface::~TestInterface()
{
    delete ui;
    delete writableSettings;
}

void TestInterface::on_editorBtn_clicked()
{
    if(editor==nullptr){
        editor=new Widget();
        if(editor==nullptr){
            qDebug()<<"can't open editor";
            return;
        }
        QString pathConverter=writableSettings->value("pathconverter").toString();
        editor->fileDeleter.setPathConverter(pathConverter);
    }
    if(editor->isVisible())
        editor->close();
    else
        editor->show();
}

void TestInterface::on_loadBtn_clicked()
{
    static QString fileName;
    qDebug()<<"fileName"<<fileName;
    QString name = QFileDialog::getOpenFileName(this,
                                            tr("Imagine"), fileName, tr("Fișiere Imagine (*.png *.jpg *.jpeg *.bmp);;Toate fișierele (*.*)"));
    if(!name.isEmpty())
        qDebug()<<name;
    if(name.isEmpty())
        return;
    fileName=name;
    QImage img;
    showImage(fileName);
}

void TestInterface::on_horizontalSlider_valueChanged(int position)
{
    ui->testView->setTransform(QTransform::fromScale(position/100.0,position/100.0));
    ui->horizontalSlider->setToolTip(QString::number(position)+"%");
    writableSettings->setValue("testinterfacescale",position);
}

void TestInterface::on_saveBtn_clicked()
{
    qDebug()<<"save"<<testScene.bg.size()<<testScene.bg.isNull();
    if(testScene.bg.isNull())
        return;
    qDebug()<<"getSaveFileName";
    static QString fileName;
    QString filters("Imagini (*.png);;Imagini (*.jpg)");
    static QString defaultFilter("Imagini (*.png)");

    fileName = QFileDialog::getSaveFileName(this, "Salveaza fisierul ca", fileName, filters, &defaultFilter);
    if(fileName.isEmpty())
        return;
    testScene.bg.save(fileName);
}

void TestInterface::on_boardBtn_clicked()
{
    selectBoard();
    if(boardDef)
        qDebug()<<boardDef->testStartType;
    ui->controlWidget->setPalette(this->palette());
    ui->resultLbl->setText("");
}

void TestInterface::createDatabase()
{
    QString path, defaultPath;
    bool defaultDb=true;
#if QT_VERSION >= 0x050400
    defaultPath=QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
#elif QT_VERSION >= 0x050000
    defaultPath=QStandardPaths::writableLocation(QStandardPaths::DataLocation);
#else
    defaultPath=QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif

    if(parser.isSet("db")){
        path=parser.value("db");
        if(QFileInfo::exists(path)){
            defaultDb=false;
        } else {
            QMessageBox::warning(this,
                                 "fisier inexistent",
                                 "fisierul "+path+
                                 " nu exista. Se va folosi locatia implicita pentru baza de date:"+defaultPath);
        }
    }
    if(defaultDb){
        path=defaultPath;
        QDir dir(path);
        dir.mkpath(path);
        path.append("/aoidata.sqlite");
    }
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(path);
    if(db.open())
        qDebug()<<"database"<<path<<"opened";
    else {
        qDebug()<<"unable to open database"<<path;
    }

    QSqlQuery query;
    bool result=query.exec("CREATE TABLE IF NOT EXISTS compdefs "
                           "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                           "name VARCHAR(30), "
                           "minarea INTEGER, "
                           "aspectratio DOUBLE,"
                           "orientationmode INTEGER,"
                           "upsample INTEGER,"
                           "horizontalflip BOOLEAN NOT NULL CHECK (horizontalflip IN (0,1)),"
                           "verticalflip  BOOLEAN NOT NULL CHECK (verticalflip IN (0,1)),"
                           "c DOUBLE,"
                           "eps DOUBLE,"
                           "numthreads INTEGER,"
                           "trainedfhog BOOLEAN NOT NULL DEFAULT 0,"
                           "oldfhog BOOLEAN NOT NULL DEFAULT 1,"
                           "traineddnn BOOLEAN NOT NULL DEFAULT 0,"
                           "olddnn BOOLEAN NOT NULL DEFAULT 1,"
                           "fhogclassifier BLOB,"
                           "dnnclassifier BLOB,"
                           "colorvalidator BLOB,"
                           "shapepredictor BLOB,"
                           "spparams BLOB,"
                           "dnnparams BLOB"
                           ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS compdefimages "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "imagepath VARCHAR(256), "
                      "implicitnegative  BOOLEAN NOT NULL CHECK (implicitnegative IN (0,1)),"
                      "compdef INTEGER, "
                      "FOREIGN KEY(compdef) REFERENCES compdefs(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS compdefimages_compdef ON compdefimages ( compdef )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS compdefrects "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "x DOUBLE, "
                      "y DOUBLE, "
                      "w DOUBLE, "
                      "h DOUBLE, "
                      "orientation INTEGER, "
                      "rectangletype INTEGER, "
                      "compdefimage INTEGER, "
                      "FOREIGN KEY(compdefimage) REFERENCES compdefimages(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS compdefrects_compdefimage ON compdefrects ( compdefimage )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boards "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "refimagepath VARCHAR(256), "
                      "name VARCHAR(128), "
                      // reference object image location
                      "x DOUBLE, "
                      "y DOUBLE, "
                      "w DOUBLE, "
                      "h DOUBLE, "
                      "orientation INTEGER, "
                      "minarea INTEGER, "
                      "maxarea INTEGER, "
                      "alignmethod INTEGER, "
                      "refclassifier INTEGER, "
                      "snvalidator VARCHAR(32),"
                      "testcodeoverride VARCHAR(10)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boardcomptypes "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64), "
                      "compType INTEGER, "
                      "board INTEGER, "
                      "FOREIGN KEY(board) REFERENCES boards(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boardcompclassifiers "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "classifier INTEGER, "
                      "comptype INTEGER, "
                      "FOREIGN KEY(classifier) REFERENCES compdefs(id), "
                      "FOREIGN KEY(comptype) REFERENCES boardcomptypes(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS boardcompclassifiers_comptype ON boardcompclassifiers ( comptype  )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }

    result=query.exec("CREATE TABLE IF NOT EXISTS boardcomponents "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64), "
                      "x DOUBLE, "
                      "y DOUBLE, "
                      "w DOUBLE, "
                      "h DOUBLE, "
                      "orientation INTEGER, "
                      "xtol DOUBLE, "
                      "ytol DOUBLE, "
                      "sizetol DOUBLE, "
                      "rectangletype INTEGER, "
                      "comptype INTEGER, "
                      "symbology INTEGER DEFAULT 0, "
                      "FOREIGN KEY(comptype) REFERENCES boardcomptypes(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE INDEX IF NOT EXISTS boardcomponents_comptype ON boardcompclassifiers ( comptype )");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    result=query.exec("CREATE TABLE IF NOT EXISTS boarddetails "
                      "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "name VARCHAR(64) DEFAULT '', "
                      "type INTEGER DEFAULT -1, "
                      "status INTEGER DEFAULT 0, "
                      "data BLOB,"
                      "board INTEGER, "
                      "FOREIGN KEY(board) REFERENCES boards(id)"
                      ")");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }

    result=query.exec("PRAGMA foreign_keys = ON");
    if(!result){
        qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
    }
    if(!sqliteTableHasField("compdefs","fhogthreshold")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN fhogthreshold double default -0.1");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boardcomponents","symbology")){
        result=query.exec("ALTER TABLE boardcomponents ADD COLUMN symbology INTEGER DEFAULT 0");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","spparams")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN spparams BLOB");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","shapepredictor")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN shapepredictor BLOB");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","dnnparams")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN dnnparams BLOB");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("compdefs","fhognorient")){
        result=query.exec("ALTER TABLE compdefs ADD COLUMN fhognorient INT");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }
    if(!sqliteTableHasField("boards","testcodeoverride")){
        result=query.exec("ALTER TABLE boards ADD COLUMN testcodeoverride VARCHAR(10) default ''");
        if(!result){
            qDebug()<<query.lastQuery()<<":"<<query.lastError().text();
        }
    }

}

void TestInterface::configureCamera(const QByteArray &deviceName)
{
    if(camera!=nullptr)
        delete camera;
    camera=new QCamera(deviceName);
    qDebug()<<camera->status()<<camera->state();
    if(deviceName.isNull()){
        if(camera){
            if(camera->isAvailable()){
                camera->stop();
                camera->unload();
            }
        }
        qDebug()<<"no camera";
        qDebug()<<QCameraInfo::availableCameras();
        return;
    }
    camera->load();
    qDebug()<<"still"<<camera->isCaptureModeSupported(QCamera::CaptureStillImage);

    camera->setCaptureMode(QCamera::CaptureStillImage);
    if(imageCapture)
        delete imageCapture;
    imageCapture = new QCameraImageCapture(camera);
    qDebug()<<"CaptureToBuffer"<<imageCapture->isCaptureDestinationSupported(QCameraImageCapture::CaptureToBuffer);
    imageCapture->setCaptureDestination(QCameraImageCapture::CaptureToBuffer);
    QList<QSize> resolutions = imageCapture->supportedResolutions();
    qDebug()<<resolutions;
    qDebug()<<"format"<<imageCapture->supportedBufferFormats();
    qDebug()<<"codecs"<<imageCapture->supportedImageCodecs();
    qDebug()<<"destination"<<imageCapture->captureDestination();
    connect(imageCapture,SIGNAL(imageCaptured(int,QImage)),this,SLOT(getImage(int,QImage)));
    connect(imageCapture,SIGNAL(readyForCaptureChanged(bool)), this, SLOT(cameraIsReady(bool)));
    camera->start();
    QDir dir;
    dir.mkpath("/tmp/images");
}

void TestInterface::configureCamera()
{
    configureCamera(QCameraInfo::defaultCamera().deviceName().toLatin1());
}

QString TestInterface::parseArguments()
{
    const QStringList args = parser.positionalArguments();
    const QString inspectionProgramm = args.isEmpty() ? QString() : args.first();

    return inspectionProgramm;
}

void TestInterface::selectBoard(QString boardName)
{
    long long id=-1;
    if(boardDef!=nullptr)
        id=boardDef->id;
    BoardSelect dlg(displaySearchFrames, showFiducials, autoSaveOption, autoSaveDir, writableSettings);
    //dlg.setDisplaySearchFrames(displaySearchFrames);
    Widget::loadBoardList(dlg.getBoardCombo(),id);
    if(boardName.isEmpty()){
        int result=dlg.exec();
        if(result!=QDialog::Accepted)
            return;
        id=dlg.getSelectedBoard();
    } else {
        id=dlg.getId(boardName);
        if(id<0)
            return;
    }
    if(id<0)
        return;
    if(boardDef!=nullptr)
        delete boardDef;
    boardDef = new BoardDef();
    boardDef->loadFromDB(id, true);
    barCodeMode=boardDef->hasBarcodeSN();
    emit logMsg( "board "+ boardDef->name + " loaded, having serial number validator set to '" +
                 boardDef->snValidator+"', barcodeMode "+QString::number(barCodeMode), IMPORTANT);
    this->setWindowTitle(boardDef->name);
    displaySearchFrames=dlg.getDisplaySearchFrames();
    writableSettings->setValue("displaySearchFrames",displaySearchFrames);
    showFiducials=dlg.getShowFiducials();
    writableSettings->setValue("showFiducials",showFiducials);
    autoSaveOption=dlg.getSaveOption();
    writableSettings->setValue("autoSaveOption", autoSaveOption);
    autoSaveDir=dlg.getSaveDir();
    writableSettings->setValue("autoSaveDir", autoSaveDir);
    QDir dir(autoSaveDir);
    if(!dir.exists())
        dir.mkpath(autoSaveDir);
    autoSaveDir=dir.absolutePath();

}

bool TestInterface::makeProtocol(QString serialNumber, bool boardOk,
                                 const QList<DetectedComponent> &foundComponents)
{
    writableSettings->sync();
    QString t=QDateTime::currentDateTime().toString("ddMMyyHHmmss");
    QString testPlaceType=writableSettings->value("testPlaceType","AOI").toString();
    QString testPlaceNumber=writableSettings->value("testPlaceNumber").toString();
    QString fileName=t+"_"+boardDef->name+"_"+serialNumber+"."+
            testPlaceType+
            testPlaceNumber;
    QString destination;
    if(!boardDef->snValidator.isEmpty()){
        destination=writableSettings->value("localPath").toString();
    }else{
        destination=writableSettings->value("localStoragePath").toString();
    }
    QDir dir;
    dir.mkpath(destination);
    QFile f(destination+fileName);
    f.open(QIODevice::WriteOnly);
    if(!f.isOpen()){
        qDebug()<<"Unable to open file "+f.fileName();
        emit logMsg("Unable to open file "+f.fileName(),CRITICAL);
        return false;
    }
    QString mat=boardDef->name.section("_",0,0);
    QString testCode=writableSettings->value("testCode").toString();
    if(!boardDef->testCodeOverride.isEmpty())
        testCode=boardDef->testCodeOverride;
    QTextStream ts( &f );
    ts <<  "sender            :  " << testPlaceType;
    ts <<  "\ndatum             :  " << t;
    ts <<  "\nlaenge            :";
    ts <<  "\nprotokoll         :  LEIKA\n\n";

    ts <<  "-----------------------------------------------------------\n\n";
    ts <<   "#KOPF {\n" <<
            "    #BARCODE = \"" << serialNumber << "\";\n" <<
            "    #ZEICHNUNGSNR = \"" << mat << "\";\n"<<
            "    #PRUEFCODE = \"" << testCode << "\";\n" <<
            "    #PRUEFMETHODE = \"" << writableSettings->value("testMethod").toString() << "\";\n"<<
            "    #PLATZNUMMER = \"" << testPlaceNumber << "\";\n" <<
            "    #PLATZART = \""<< testPlaceType << "\";\n" <<
            "    #ZEITPUNKT = \"" << t <<"\";\n" <<
            "    #PRUEFKENN = \"" << (boardOk?"P":"F") << "\";\n";
    if(ts.status()!=QTextStream::Ok){
        //qDebug()<<"error writing to file"+f.fileName();
        emit logMsg("error writing to file "+f.fileName(),CRITICAL);
        return false;
    }
    ts <<   "}\n";
    if(!boardOk){
        ts << "#PRUEFUNG {\n";
        if(foundComponents.count()>0){
            QStringList lst;
            for(int i=0; i<foundComponents.count(); i++){
                const DetectedComponent & comp=foundComponents[i];
                qDebug()<<comp.x<<comp.y<<comp.w<<comp.h<<comp.orientation<<
                          comp.boardComp.name<<comp.status;
                //only save not detected componente and barcodes to protocol
                if((comp.status!=DetectedComponent::Expected)&&(comp.status!=DetectedComponent::ExpectedBarcode))
                    continue;
                QString compName=comp.boardComp.name;
                if(compName.isEmpty())
                    compName="unnamed component";
                if(lst.contains(compName)){
                    for(int i=2; i<1000; i++){
                        QString tmpName=compName+"_"+QString::number(i);
                        if(lst.contains(tmpName))
                            continue;
                        compName=tmpName;
                        break;
                    }
                }
                QString comment;
                if(comp.status==DetectedComponent::ExpectedBarcode){
                    comment="        #KOMMENTAR = \"Barcode not found\";\n";
                }
                ts << "    #SATZ {\n"
                      "        #EINBAUBEZ = \"" << compName <<"\";\n"<<
                      "        #TESTART = \"PRESENCE\";\n"<<
                      //"        #KOMMENTAR = \"Component not found\";\n"<<
                      comment <<
                      "        #STATUS = \"F\";\n"<<
                      //"        #ZERTIFIKATE = \"N\";\n"<<
                      "    }\n";
            }

        } else {
            ts << "    #SATZ {\n"
                  "        #EINBAUBEZ = \"" << mat <<"\";\n"<<
                  "        #TESTART = \"Board presence\";\n"<<
                  //"        #KOMMENTAR = \"Board not found\";\n"<<
                  "        #STATUS = \"F\";\n"<<
                  //"        #ZERTIFIKATE = \"N\";\n"<<
                  "    }\n";

        }
        ts << "}\n";
    }
    ts.flush();
    if(ts.status()!=QTextStream::Ok){
        qDebug()<<"error writing to file"+f.fileName();
        emit logMsg("Error writing to file "+f.fileName(),CRITICAL);
        return false;
    }
    f.close();
    myFtp.ftpUpload();
    return true;
}

bool TestInterface::sqliteTableHasField(QString tableName, QString fieldName)
{
    QSqlQuery query;
    QString sql="PRAGMA table_info("+ tableName+")";
    if(!query.exec(sql)){
        qDebug()<<"error while checking for field"<<query.lastError()<<query.lastQuery();
    }
    while (query.next()) {
        if(query.value("name").toString()==fieldName)
            return true;
    }
    return false;
}

void TestInterface::saveImage(const QPixmap img, const QList<DetectedComponent> boardRects)
{
    //save image
    QString dirName=autoSaveDir+"/"+boardDef->name;
    QDir dir;
    dir.mkpath(dirName);
    QString fileName=dirName+"/"+QDateTime::currentDateTime().toString("ddMMyyHHmmss")+"_"+serialNumber;
    img.save(fileName+".png");
    std::ofstream out((fileName+".result").toStdString(), std::ofstream::out|std::ofstream::binary);
    if(out.is_open()){
        serialize(boardRects, out);
        out.close();
    }

}

void TestInterface::getImage(int id, const QImage &img)
{
    Q_UNUSED(id);
    emit logMsg("capture done with qcamera", VERBOSE);
    qDebug()<<"captured"<<img.width()<<img.height()<<img.depth()<<img.byteCount()<<img.bytesPerLine()<<
              QDateTime::currentDateTime();
    //QImage i2=img.convertToFormat(QImage::Format_RGB888);
    //qDebug()<<i2.width()<<i2.height()<<i2.depth()<<i2.byteCount()<<i2.bytesPerLine();
    //i2.save("i2.png");
    testScene.setBackgroundImage(img);
    testScene.update();
    qDebug()<<QDateTime::currentDateTime();
    pictureRequested=false;
    //if this slot has been called while running a test, continue with the next step
    if(runTask==ANALYZE){
        runTestSlot();
        runTask=IDLE;
    }
}

void TestInterface::getImage(const QImage &img)
{
    emit logMsg("capture done with ocv camera or manually", VERBOSE);
    qDebug()<<"captured"<<img.width()<<img.height()<<img.depth()<<img.byteCount()<<img.bytesPerLine()<<
              QDateTime::currentDateTime();
    //QImage i2=img.convertToFormat(QImage::Format_RGB888);
    //qDebug()<<i2.width()<<i2.height()<<i2.depth()<<i2.byteCount()<<i2.bytesPerLine();
    //i2.save("i2.png");
    testScene.setBackgroundImage(img);
    testScene.update();
    qDebug()<<QDateTime::currentDateTime();
    //if this slot has been called while running a test, continue with the next step
    if(runTask==ANALYZE){
        runTestSlot();
        runTask=IDLE;
    }

}

void TestInterface::cameraIsReady(bool ready)
{
    qDebug()<<"statusChanged"<<ready<<pictureRequested;
    if(!pictureRequested)
        return;
    if(ready){
        on_cameraBtn_clicked();
    }
}

void TestInterface::on_cameraBtn_clicked()
{
    emit logMsg("starting capture operation",VERBOSE);
    ui->controlWidget->setPalette(this->palette());
    testScene.clear();
    boardRects.clear();
    ui->resultLbl->setText("");
    if(writableSettings->value("cameratype").toString()=="qcamera") {
        myCamera.capture();
    } else if(writableSettings->value("cameratype").toString()=="ipcamera"){
        ipCamera.capture();
    } else if(writableSettings->value("cameratype").toString()=="simcamera"){
        on_loadBtn_clicked();
    } else {
#ifdef OCVCAM
        ocvCamera.capture();
#endif
    }

    return;
#if 0
    qDebug()<<"cameraBtn"<<QDateTime::currentDateTime();
    if(!imageCapture)
        configureCamera();
    if(imageCapture){
        qDebug()<<"isreadyforcapture"<<imageCapture->isReadyForCapture();
        if(camera->status()!=QCamera::ActiveStatus){
            pictureRequested=true;
            qDebug()<<"retry capture";
        }else {
            qDebug()<<"capture";
            //capture writes to file. There is no known to prevent it
            QDir dir("/tmp/images/");
            dir.setNameFilters(QStringList() << "*");
            dir.setFilter(QDir::Files);
            foreach (QString entry, dir.entryList()) {
                dir.remove(entry);
            }
            imageCapture->capture("/tmp/images/");
        }
    }
#endif
}

void TestInterface::getPixmap(QPixmap pixmap)
{
    emit logMsg("capture done", VERBOSE);
    testScene.setBackgroundImage(pixmap);
    testScene.update();
    //if this slot has been called while running a test, continue with the next step
    if(runTask==ANALYZE){
        runTestSlot();
        runTask=IDLE;
    }
}


void TestInterface::on_playBtn_clicked()
{
    //this slot is either activated by pressing the button, or during a test, after acquiring the image
    bool testOk=false;
    try {

        if(nullptr==boardDef){
            return;
        }
        if(testScene.bg.isNull()){
            return;
        }
        testScene.clearRects();
        qDebug()<<"on_playBtn_clicked";
        qDebug()<<boardDef->refClassifier.name<<boardDef->refClassifier.minArea<<boardDef->minArea;
        //comp.classifiers.insert(-1,QSharedPointer<MemCompDef>(&(boardDef->refClassifier)));
        //scale=0.55;
        boardRects=boardDef->runOnImage(testScene.bg.toImage().convertToFormat(QImage::Format_RGB888), displaySearchFrames, testOk);
    }  catch (std::exception& e) {
        emit logMsg( QString("Standard exception in on_playBtn_clicked: ") + e.what(), CRITICAL);
        testOk=false;
    } catch (...) {
        emit logMsg("default exception caught while running test in on_playBtn_clicked", CRITICAL);
        testOk=false;
    }
    showTestResult(boardRects, testOk);
}

void TestInterface::on_runButton_clicked()
{
    try {
        //clear background
        testScene.setBackgroundImage(QPixmap());
        testScene.update();
        emit logMsg("starting test with SN="+ui->snEdit->text()+" runTask="+QString::number(runTask), INFO);

        /// @todo add option to delay test after scanning serial number
        if(nullptr==boardDef){
            QMessageBox::information(this, tr("nu e incărcat nici un program"),
                                     tr("dacă doriți să verificați plăci, incărcați întâi un program de inspecție"));
            emit logMsg("nu e incarcat nici un program. Abandon test SN="+ui->snEdit->text(), IMPORTANT);
            ui->snEdit->setFocus();
            ui->snEdit->selectAll();
            return;
        }
        qDebug()<<"runButton"<<"task"<<runTask;
        if(runTask==IDLE){
            serialNumber=ui->snEdit->text().toUpper();
            QStringList dummySerials;
            dummySerials<<"TESTA"<<"TESTAA"<<"111111111";
            if(!boardDef->snValidator.isEmpty()){
                if(!barCodeMode){
                    if(dummySerials.contains(serialNumber)){
                        //just accept the serial number
                    } else {
                        QRegExp regExp(boardDef->snValidator);
                        if(!regExp.exactMatch(serialNumber)){
                            QString msg="Numărul de serie "+serialNumber+" nu e valid";
                            QMessageBox::information(this, "număr serie invalid", msg);
                            emit logMsg(msg,INFO);
                            ui->snEdit->setFocus();
                            ui->snEdit->selectAll();
                            return;
                        }
                        QString testPlaceType, testPlaceNumber, testCode;
                        testPlaceType=writableSettings->value("testPlaceType").toString();
                        testPlaceNumber=writableSettings->value("testPlaceNumber").toString();
                        testCode=writableSettings->value("testCode").toString();
                        if(!snCheck.checkSNSocket(serialNumber, boardDef->name, testCode, testPlaceType, testPlaceNumber)){
                            QString msg="număr serie invalid, eroare "+ snCheck.getLastError();
                            QMessageBox::information(this, "număr serie invalid", snCheck.getLastError());
                            emit logMsg(msg,INFO);
                            ui->snEdit->setFocus();
                            ui->snEdit->selectAll();
                            return;
                        }
                    }
                }
            }
            switch(boardDef->testStartType){
            case 0://start immediate
            default:
                //prepare for the next task and fall through to it
                runTask=CAPTURE;
                break;
            case 1://start delayed
                //prepare for the next task and start the timer to initiate the task.
                //this will also allow to start the task by pressing RUN a second time, but there is the risk
                //to receive the timer signal after the analyze is finished, starting a new test
                runTask=CAPTURE;
                QTimer::singleShot(boardDef->testStartDelay,this, SLOT(on_runButton_clicked()));
                //exit from the function
                return;
            case 2://start when RUN is pressed a second time
                //prepare for the next task and exit from the function
                runTask=CAPTURE;
                return;
            }
        }
        if(runTask==CAPTURE){
            emit logMsg("capture image",VERBOSE);
            runTask=ANALYZE;
            on_cameraBtn_clicked();
        }
    }  catch (std::exception& e) {
        emit logMsg( QString("Standard exception in on_runButton_clicked: ") + e.what(), CRITICAL);
    } catch (...) {
        emit logMsg("default exception caught while running test in on_runButton_clicked", CRITICAL);
    }
}

void TestInterface::runTestSlot()
{
    //this slot is activated during a test as the last phase, after acquiring the image
    //do the test
    on_playBtn_clicked();

    //check if image needs to be saved
    bool imageOk=(boardRects.count()>0);
    foreach (const DetectedComponent & board, boardRects) {
        if(board.status!=DetectedComponent::Detected)
            imageOk=false;
    }

    // save results
    if(boardDef->snValidator.isEmpty()){
        //no serial number requested, so nothing needs to be saved
        emit logMsg("test run without result saving, SN="+ui->snEdit->text()+(imageOk?" PASS":" FAIL"), INFO);
    } else {
        if(boardRects.count()==0){
            //board not found
            QList<DetectedComponent> list;
            emit logMsg("board not found, SN="+ui->snEdit->text(), IMPORTANT);
            if(!barCodeMode){
                //if no board has been detected, for sure we do not have scanned the serial number from the picture
                makeProtocol(serialNumber,false,list);
            }
        } else {
            emit logMsg("found "+QString::number(boardRects.count()) +" boards, making protocol, SN="+ui->snEdit->text()+" result "+(imageOk?"PASS":"FAIL"),INFO);
            if(boardRects.count()>1){
                qDebug()<<"s-au găsit"<<boardRects.count()<<"plăci în loc de una singură";
            }
            for(int i=0; i<boardRects.count(); i++){
                bool snOk=true;
                 if(barCodeMode){
                    /// serial number check
                    /// find serial number
                    serialNumber.clear();
                    foreach(DetectedComponent comp, boardRects[i].parts){
                        if(comp.status==DetectedComponent::DetectedBarcode){
                            if(comp.parts.count()!=1){
                                qDebug()<<"barcode detection went wrong, parts="<<comp.parts.count();
                            } else {
                                qDebug()<<"barcode"<<comp.boardComp.name<<comp.parts[0].boardComp.name;
                                if(comp.boardComp.name.compare("barcode",Qt::CaseInsensitive)==0){
                                    serialNumber=comp.parts[0].boardComp.name;
                                    break;
                                }
                            }
                        }
                    }
                    QString testPlaceType, testPlaceNumber, testCode;
                    testPlaceType=writableSettings->value("testPlaceType").toString();
                    testPlaceNumber=writableSettings->value("testPlaceNumber").toString();
                    testCode=writableSettings->value("testCode").toString();
                    if(!snCheck.checkSNSocket(serialNumber, boardDef->name, testCode, testPlaceType, testPlaceNumber)){
                        QString msg=tr("număr serie invalid, eroare ")+ snCheck.getLastError();
                        QMessageBox::information(this, tr("număr serie invalid, rezultatul pentru placa nu se va salva"),
                                                 snCheck.getLastError());
                        emit logMsg(msg,INFO);
                        snOk=false;
                        boardRects[i].status=DetectedComponent::Expected;
                        /// @todo corectie numar serie
                    }

                }
                if(snOk&&(!serialNumber.isEmpty())){
                    makeProtocol(serialNumber,boardRects[i].status==DetectedComponent::Detected,boardRects[i].parts);
                }
            }
        }
    }
    //redo check if image needs to be saved, maybe something was changed
    imageOk=(boardRects.count()>0);
    foreach (const DetectedComponent & board, boardRects) {
        if(board.status!=DetectedComponent::Detected)
            imageOk=false;
    }
    //save the image if needed
    if((autoSaveOption==SAVEALL)||
            ((autoSaveOption==SAVEFAIL)&&(!imageOk))){
        QtConcurrent::run(this, &TestInterface::saveImage, testScene.bg, boardRects);
    }
    ui->snEdit->setFocus();
    ui->snEdit->selectAll();
}

void TestInterface::on_cameraBtn_triggered(QAction *arg1)
{
    qDebug()<<"triggered"<<arg1->objectName()<<arg1->text();
    CameraConfig dlg((writableSettings), &myCamera, &ipCamera,
#ifdef OCVCAM
                     &ocvCamera,
#endif
                     this);
    connect(&dlg, SIGNAL(loadImageRequest()), this, SLOT(on_loadBtn_clicked()));
    dlg.exec();
}


void TestInterface::on_snEdit_returnPressed()
{
    on_runButton_clicked();
}

void TestInterface::addToClassifier(long long classifierId, const QString &imageName)
{
    if(editor==nullptr)
        on_editorBtn_clicked();
    if(editor==nullptr)
        return;
    editor->addToClassifier(classifierId, imageName);
}

void TestInterface::showBoard(long long boardId)
{
    qDebug()<<"showBoard"<<boardId;
    if(editor==nullptr)
        on_editorBtn_clicked();
    if(editor==nullptr)
        return;
    editor->showBoard(boardId);
}

void TestInterface::showTestResult(QList<DetectedComponent> &boardRects, const bool testOk)
{
    //show result
    for(int i=0; i<boardRects.count(); i++){
        QList<DetectedComponent> & foundComponents = boardRects[i].parts;
        for(int j=0; j<foundComponents.count(); j++){
            DetectedComponent & comp=foundComponents[j];
            if(!showFiducials){
                if((comp.status==DetectedComponent::DetectedFiducial)||
                        (comp.status==DetectedComponent::ExpectedFiducial))
                    continue;
            }
            ComponentRectItem * item=new ComponentRectItem(comp);
            testScene.addItem(item);
        }
        ComponentRectItem *item=new ComponentRectItem(boardRects[i]);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        testScene.addItem(item);
    }
    //display the global status of the test
    QPalette pal=ui->controlWidget->palette();
    ui->controlWidget->setAutoFillBackground(true);
    if(testOk){
        //ui->controlWidget->setStyleSheet("background-color:green;");
        pal.setColor(QPalette::Background,Qt::green);
        ui->controlWidget->setPalette(pal);
        if(writableSettings->value("showResultLabel").toBool())
            ui->resultLbl->setText("PASS");
    } else {
        //ui->controlWidget->setStyleSheet("background-color:red;");
        pal.setColor(QPalette::Background,Qt::red);
        ui->controlWidget->setPalette(pal);
        if(writableSettings->value("showResultLabel").toBool())
            ui->resultLbl->setText("FAIL");
    }
}

void TestInterface::showImage(QString fileName)
{
    QImage img;
    img.load(fileName);
    testScene.clear();
    boardRects.clear();
    //testScene.setBackgroundImage(img);
    ui->controlWidget->setPalette(this->palette());
    ui->resultLbl->setText("");
    //emit imageLoaded(img);
    //testScene.setBackgroundImage(fileName);
    testScene.setBackgroundImage(img);
    testScene.update();
}

void TestInterface::showImageResult(QString fileName, QList<DetectedComponent> &boardRects, const bool testOk)
{
    showImage(fileName);
    showTestResult(boardRects, testOk);
}

void TestInterface::centerAt(double x, double y)
{
    qDebug()<<"centerAt"<<x<<y;
    ui->testView->centerOn(x, y);
}
