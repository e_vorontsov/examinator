/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "boardselect.h"
#include "ui_boardselect.h"
#include <QDebug>

BoardSelect::BoardSelect(bool displaySearchFrames, bool showFiducials, int saveOption, QString saveDir,
                         QSettings * settings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BoardSelect)
{
    ui->setupUi(this);
    ui->displaySearchBox->setChecked(displaySearchFrames);
    ui->showFiducialBox->setChecked(showFiducials);
    ui->saveOptionCombo->setCurrentIndex(saveOption);
    ui->saveDirEdit->setText(saveDir);
    this->settings=settings;
    if(settings){
        //load settings
        QStringList keys=settings->allKeys();
        ui->settingsCombo->addItems(keys);
    }
    ui->splitter->setSizes(QList<int>({1, 0}));
}

BoardSelect::~BoardSelect()
{
    delete ui;
}

QComboBox *BoardSelect::getBoardCombo()
{
    return ui->boardCombo;
}

long long BoardSelect::getSelectedBoard()
{
    if(ui->boardCombo->count()<1)
        return -1;
    return ui->boardCombo->currentData().toLongLong();
}

long long BoardSelect::getId(QString boardName)
{
    int idx=ui->boardCombo->findText(boardName);
    if(idx<0)
        return -1;
    //qDebug()<<"BoardSelect::getId"<<boardName<<idx<<ui->boardCombo->itemText(idx)<<ui->boardCombo->itemData(idx).toLongLong();
    return ui->boardCombo->itemData(idx).toLongLong();
}

bool BoardSelect::getDisplaySearchFrames()
{
    return ui->displaySearchBox->isChecked();
}

bool BoardSelect::getShowFiducials(void){
    return ui->showFiducialBox->isChecked();
}

int BoardSelect::getSaveOption(void){
    return ui->saveOptionCombo->currentIndex();
}

QString BoardSelect::getSaveDir(void){
    return ui->saveDirEdit->text();
}

void BoardSelect::on_settingsCombo_currentIndexChanged(int index)
{
    if(settings!=nullptr){
        QString txt=ui->settingsCombo->itemText(index);
        ui->settingsEdit->setText(settings->value(txt).toString());
    }

}

void BoardSelect::on_settingsEdit_editingFinished()
{
    settings->setValue(ui->settingsCombo->currentText(),ui->settingsEdit->text());
}

void BoardSelect::on_boardSearchEdit_textChanged(const QString &arg1)
{
    if(ui->boardCombo->count()<=0)
        return;
    if(ui->boardCombo->currentText().startsWith(arg1))
        return;
    int letters=arg1.length();
    if(letters<=0)
        return;
    int newIndex=0;
    for(int i=0; i<ui->boardCombo->count(); i++){
        //qDebug()<<i<<ui->boardCombo->itemText(i)<<ui->boardCombo->itemText(i).compare(arg1,Qt::CaseInsensitive);
        int result=ui->boardCombo->itemText(i).left(letters).compare(arg1,Qt::CaseInsensitive);
        if(result>0)
            break;
        newIndex=i;
        if(result==0)
            break;
    }
    ui->boardCombo->setCurrentIndex(newIndex);
}
