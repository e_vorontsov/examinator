#ifndef OPENCVCAM_H
#define OPENCVCAM_H

#include <QObject>
#include <QImage>
#include <opencv2/videoio.hpp>

class OpenCVCam : public QObject
{
    Q_OBJECT
public:
    explicit OpenCVCam(QObject *parent = nullptr);
    QList<int> getAvailableCameras(int cameraBackend=0);
    QList<QSize> getResolutions();

    QSize getResolution() const;
    void setResolution(const QSize &value);

    int getNImages() const;
    void setNImages(int value);

    int getNIgnore() const;
    void setNIgnore(int value);

    int getDeviceId() const;

signals:
    void imageCaptured(QImage image);
    void captureError(QString errorMessage);

public slots:
    void capture(void);
    void setCamera(QString deviceName, bool enabled=true);
    void setCamera(int deviceId, bool enabled=true);
private:
    cv::VideoCapture cap;
    int deviceId=0;
    QString deviceName;
    QSize resolution=QSize(10000,10000);
    ///number of images to average in order to improve the image quality
    int nImages=2;
    ///number of images to ignore in order tu flush the buffers
    int nIgnore=1;
    bool reopen();
};

#endif // OPENCVCAM_H
