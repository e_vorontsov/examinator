#ifndef FILEDELETER_H
#define FILEDELETER_H

#include <QWidget>
#include <QVector>
#include <QString>
#include <QDir>
#include <QMultiMap>
#include <QListWidgetItem>

namespace Ui {
class FileDeleter;
}

class FileDeleter : public QWidget
{
    Q_OBJECT

public:
    explicit FileDeleter(QWidget *parent = nullptr);
    ~FileDeleter();
    void setPathConverter(QString combinedString);
    QString convertPath(QString input);

private slots:
    void on_dirSelButton_clicked();

    void on_filesList_itemSelectionChanged();

    void on_deleteBtn_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_classifierList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_classifierList_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::FileDeleter *ui;
    QVector<QString> pathConverterFrom, pathConverterTo;
    void addPathConverter(const QString from, const QString to);
    QDir dir;
    void updateStats(void);
    //update the files list, using the folder already stored in dir.
    void updateFilesList(void);
    void updateClassifierList(void);
    QMultiMap<long long, QString> classifierBoards;
signals:
    void requestClassifier(long long classifier, QString fileName=QString());
};

#endif // FILEDELETER_H
