/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mscene.h"
#include <QDebug>
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include <assert.h>
#include <limits>
#include <math.h>
#include <exception>

//#include <QGraphicsRectItem>

MScene::MScene(QObject *parent) :
    QGraphicsScene(parent)
{
    /*
    QString imgpath;
    imgpath="/home/guenter/source/scene/raspi2.jpg";
    //imgpath="raspi2.jpg";
    QPixmap img(imgpath);
    bg.setPixmap(img);
    bg.setZValue(-1);
    //s.setBackgroundBrush(img);
    setSceneRect(img.rect());
    addItem(&bg);
    */
    //MemCompDefImage compDefImageTmp;
    compDef=new CompDef;
    compDefImage=NULL;
    compDefRect=NULL;
    connect(this,SIGNAL(rectChanged(ComponentRectItem*,bool)),this,SLOT(compRectChanged(ComponentRectItem*,bool)));
}

void MScene::emitRectChanged(ComponentRectItem *item, bool selected)
{
    emit rectChanged(item, selected);
}

QPixmap MScene::getCompChip(CompDefRect *rect)
{
    double w,h;
    if((rect->orientation==CompDefRect::E)||
            (rect->orientation==CompDefRect::W)||
            (rect->orientation==CompDefRect::EW)){
        w=rect->h;
        h=rect->w;
    } else {
        w=rect->w;
        h=rect->h;
    }
    //qDebug()<<"copy from"<<compRect->compDefRect->x<<compRect->compDefRect->y<<w<<h;
    QPixmap compPixmap=bg.copy(rect->x,rect->y, w, h);
    int rotation=0;
    switch(rect->orientation){
    case CompDefRect::E:
    case CompDefRect::EW:
        rotation=270;
        break;
    case CompDefRect::S:
        rotation=180;
        break;
    case CompDefRect::W:
        rotation=90;
        break;
    default:
        break;
    }
    if(rotation){
        QTransform t2;
        QTransform t=t2.rotate(rotation);

        //qDebug()<<"orientation"<<compRect->compDefRect->orientation<<"rotation"<<rotation<<t;
        compPixmap=compPixmap.transformed(t);
    } else {
        //qDebug()<<"orientation"<<compRect->compDefRect->orientation<<"rotation"<<rotation;
    }
    return compPixmap;
}
void MScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){
    qDebug()<<mouseEvent->scenePos();
    if(mouseEvent->button()==Qt::MiddleButton){
        //unselect all
        QList<QGraphicsItem *> items=selectedItems();
        for(int i=0; i<items.count(); i++){
            items[i]->setSelected(false);
        }
        //add component type
        emit middleMousePressed(mouseEvent->scenePos().x(),mouseEvent->scenePos().y());
        return;
    }
    if(mouseEvent->button()==Qt::RightButton){
        //right button does the same thing as left button
        mouseEvent->setButton(Qt::LeftButton);
        QGraphicsScene::mousePressEvent(mouseEvent);
        return;
    }
    QGraphicsScene::mousePressEvent(mouseEvent);
    /*
    qDebug()<<this->selectedItems().count();
    foreach (MemCompDefImage img, compDef->images) {
        foreach (MemCompDefRect r, img.rect) {
            qDebug()<<"rectangle"<<r.x<<r.y<<r.w<<r.h;
        }
    }
    */
}

void MScene::keyReleaseEvent(QKeyEvent *event)
{
    int key=event->key();
    if((key==Qt::Key_Backspace)||
            (key==Qt::Key_Delete)||
            (key==Qt::Key_R)||
            (key==Qt::Key_P)||
            (key==Qt::Key_N)||
            (key==Qt::Key_I)){
        qDebug()<<key;
        emit sceneKeyEvent(event);
        return;
    }
    qDebug()<<key<<"ignored";
    QGraphicsScene::keyReleaseEvent(event);
}

bool MScene::setBackgroundImage(QString imgPath)
{
    this->clear();
    //setBackgroundBrush(QBrush(Qt::red, Qt::SolidPattern));
    setBackgroundBrush(QBrush(QColor(192,210,255), Qt::SolidPattern));
    bg.load(imgPath);
    if(bg.isNull()){
        return false;
    }
    qDebug()<<"setbg"<<imgPath;
    QGraphicsPixmapItem * item=new QGraphicsPixmapItem();
    item->setPixmap(bg);
    item->setZValue(-1000);
    //s.setBackgroundBrush(img);
    setSceneRect(bg.rect());
    addItem(item);
    return true;
}

bool MScene::setBackgroundImage(const QImage &img)
{
    this->clear();
    this->compDefRect=nullptr;
    //setBackgroundBrush(QBrush(Qt::red, Qt::SolidPattern));
    setBackgroundBrush(QBrush(QColor(192,210,255), Qt::SolidPattern));
    bg=QPixmap::fromImage(img);
    if(bg.isNull()){
        qDebug()<<"bg.isnull"<<img.isNull()<<img.width();
        return false;
    }
    QGraphicsPixmapItem * item=new QGraphicsPixmapItem();
    item->setPixmap(bg);
    item->setZValue(-1000);
    //s.setBackgroundBrush(img);
    setSceneRect(bg.rect());
    addItem(item);
    return true;
}

bool MScene::setBackgroundImage(const QPixmap &pixmap)
{
    bg=pixmap;
    this->clear();
    this->compDefRect=nullptr;
    //setBackgroundBrush(QBrush(Qt::red, Qt::SolidPattern));
    setBackgroundBrush(QBrush(QColor(192,210,255), Qt::SolidPattern));
    if(pixmap.isNull()){
        return false;
    }
    QGraphicsPixmapItem * item=new QGraphicsPixmapItem();
    item->setPixmap(pixmap);
    item->setZValue(-1000);
    //s.setBackgroundBrush(img);
    setSceneRect(pixmap.rect());
    addItem(item);
    return true;
}

void MScene::compRectChanged(ComponentRectItem *item, bool selected)
{
    //qDebug()<<"compRectChanged"<<selected;
    if(selected){
        this->compDefRect=item;
        qDebug()<<item->compDefRect->x<<item->compDefRect->y<<item->compDefRect->w<<
                  item->compDefRect->h<<item->compDefRect->orientation;
    } else
        this->compDefRect=NULL;
}

void MScene::showDetections(QList<DetectedComponent> &detections, double threshold)
{
    for(int i=0; i<detections.count(); i++){
        DetectedComponent & comp = detections[i];
        if(comp.score[0]<threshold)
            continue;
        qDebug()<<"detected"<<comp.x<<comp.y<<comp.w<<comp.h<<"score"<<comp.score[0]<<"type"<<comp.rectangleType;
        ComponentRectItem *item=new ComponentRectItem(comp);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        addItem(item);
    }

}

void MScene::changeImage(QString fileName)
{
    setBackgroundImage(fileName);
    //find the image object
    for(int i=0; i<compDef->images.count();i++) {
        CompDefImage & img = compDef->images[i];
        if(img.imagePath == fileName){
            compDefImage=&img;
        }
    }
    for(int i=0; i<compDefImage->rect.count();i++) {
        CompDefRect &r = compDefImage->rect[i];
        //qDebug()<<"restore"<<r.x<<r.y<<r.w<<r.h<<r.orientation;
        ComponentRectItem *item=new ComponentRectItem(r);
        //ComponentRectItem *item=new ComponentRectItem(mouseEvent->scenePos().x(),mouseEvent->scenePos().y(),30,30);
        this->addItem(item);
        item->setAcceptedMouseButtons(Qt::LeftButton);
        item->setFlag(QGraphicsItem::ItemIsMovable);
        item->setFlag(QGraphicsItem::ItemIsSelectable);
    }
    this->compDefRect=nullptr;
}

void MScene::changeCompDef(long long id)
{
    /// @todo
}

void MScene::clearRects()
{
    //QList<QGraphicsItem *>items=items();
    QGraphicsItem * item;
    ComponentRectItem * r;
    foreach(item, items()){
        r=dynamic_cast<ComponentRectItem*>(item);
        if(r==nullptr)
            continue;
        this->removeItem(item);
    }
}

void MScene::clearDetectedRects()
{
    QGraphicsItem * item;
    ComponentRectItem * r;
    foreach(item, items()){
        r=dynamic_cast<ComponentRectItem*>(item);
        if(r==nullptr)
            continue;
        try {
            if(r->compDefRect->rectangleType!=CompDefRect::FOUNDCOMP)
                continue;
        } catch (...) {
            //do nothing. If there is an error accessing the compDefRect, the rectangle will be deleted
        }
        this->removeItem(item);
    }
}


void ComponentRectItem::paint(QPainter *p, const QStyleOptionGraphicsItem *opt, QWidget *wdg)
//Q_DECL_OVERRIDE
{
    QGraphicsRectItem::paint(p, opt, wdg);
    try {

        // optional
        if(compDefRect){
            qreal x,y,w,h;
            this->rect().getCoords(&x, &y, &w, &h);
            w-=x;
            h-=y;
            if(compDefRect->orientation>=0){
                switch(compDefRect->orientation){
                default:
                    //don't draw the polarity mark
                    break;
                case CompDefRect::N:
                    p->drawEllipse(x+w/2-polarityMarkSize/2, y, polarityMarkSize, polarityMarkSize);
                    break;
                case CompDefRect::E:
                    p->drawEllipse(x+w-polarityMarkSize, y+h/2-polarityMarkSize/2, polarityMarkSize, polarityMarkSize);
                    break;
                case CompDefRect::S:
                    p->drawEllipse(x+w/2-polarityMarkSize/2, y+h-polarityMarkSize, polarityMarkSize, polarityMarkSize);
                    break;
                case CompDefRect::W:
                    p->drawEllipse(x, y+h/2-polarityMarkSize/2, polarityMarkSize, polarityMarkSize);
                    break;
                case CompDefRect::NS:
                    p->drawLine(x+w/2, y, x+w/2, y+h);
                    break;
                case CompDefRect::EW:
                    p->drawLine(x, y+h/2, x+w, y+h/2);
                    break;
                }
            }
            if(compDefRect->rectangleType==CompDefRect::FOUNDCOMP){
                DetectedComponent *dc=static_cast<DetectedComponent*>(compDefRect);
                //qDebug()<<"foundComp"<<dc->status<<dc->boardComp.name<<dc->boardComp.orientation;
                if((dc->status==DetectedComponent::DetectedFiducial)||(dc->status==DetectedComponent::ExpectedFiducial))
                    p->drawEllipse(x, y, w, h);
                p->drawText(x+3,y+h-3,dc->boardComp.name+": "+QString::number(dc->score[0]));
                p->drawText(x+3,y+h-14,
                            QString::number(100*dc->xerr,'f',1)+":"+
                            QString::number(100*dc->yerr,'f',1)+":"+
                            QString::number(100*dc->scaleerr,'f',1));
            } else {
                //qDebug()<<"otherComp"<<compDefRect->rectangleType;
            }

        }
    } catch (std::exception& e) {
        qDebug()<<"error in ComponentRectItem::paint()"<<e.what();
    }
}

void ComponentRectItem::updateCompDefRect()
{
    qreal x1, y1, x2, y2;
    this->rect().getCoords(&x1,&y1,&x2,&y2);
    //qDebug()<<"release"<<x1<<y1<<x2<<y2<<this->rect()<<this->pos();
    if(compDefRect->update(x1+pos().x(),y1+pos().y(),x2-x1,y2-y1)){
        MScene * p = dynamic_cast<MScene *>(scene());
        p->compDef->dirty=true;
    }

}

void ComponentRectItem::updateFromCompDef()
{
    //qDebug()<<"update from comp def";
    this->setPos(0,0);
    if((compDefRect->orientation==CompDefRect::E)||
            (compDefRect->orientation==CompDefRect::W)||
            (compDefRect->orientation==CompDefRect::EW)){
        this->setRect(compDefRect->x,compDefRect->y,compDefRect->h,compDefRect->w);
        aspectRatio=compDefRect->h/compDefRect->w;
    } else {
        this->setRect(compDefRect->x,compDefRect->y,compDefRect->w,compDefRect->h);
        aspectRatio=compDefRect->w/compDefRect->h;
    }
    if(compDefRect->rectangleType==CompDefRect::POSITIVE){
        this->setPen(QPen(Qt::yellow));
    } else if(compDefRect->rectangleType==CompDefRect::BOARDCOMP){
        this->setPen(QPen(Qt::blue));
    } else if(compDefRect->rectangleType==CompDefRect::BOARDRECT){
        this->setPen(QPen(Qt::white));
    } else if(compDefRect->rectangleType==CompDefRect::IGNORERECT){
        this->setPen(QPen(Qt::magenta));
        aspectRatio=-1;
    } else {
        this->setPen(QPen(Qt::cyan));
        aspectRatio=-1;
        qDebug()<<"rectangletype"<<compDefRect->rectangleType<<CompDefRect::NEGATIVE;
    }
    QPen pen=this->pen();
    pen.setWidth(2);
    if(compDefRect->rectangleType==CompDefRect::FOUNDCOMP){
        DetectedComponent *dc=static_cast<DetectedComponent*>(compDefRect);
        if((dc->status!=DetectedComponent::Detected)&&
                (dc->status!=DetectedComponent::DetectedFiducial))
            pen.setWidth(4);
    }
    this->setPen(pen);

    if(this->isSelected())
        this->setCornerPositions();
    this->update();
}

QVariant ComponentRectItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value){
    if (change == ItemSelectedHasChanged && scene()) {
        // value is the new selection status.
        bool newStatus = value.toBool();
        MScene *s=dynamic_cast<MScene*>(scene());
        qreal scale=1;
        if(s)
            scale=s->scale;
        // Keep the item inside the scene rect.
        //qDebug()<<"selected"<<newStatus;
        if(newStatus){
            _corners[0] = new CornerGrabber(this,0, HANDLESIZE/scale);
            _corners[1] = new CornerGrabber(this,1, HANDLESIZE/scale);
            _corners[2] = new CornerGrabber(this,2, HANDLESIZE/scale);
            _corners[3] = new CornerGrabber(this,3, HANDLESIZE/scale);


            _corners[0]->installSceneEventFilter(this);
            _corners[1]->installSceneEventFilter(this);
            _corners[2]->installSceneEventFilter(this);
            _corners[3]->installSceneEventFilter(this);

            setCornerPositions();
            MScene * p = dynamic_cast<MScene *>(scene());
            if(p!=nullptr)
                p->emitRectChanged(this,newStatus);
        } else {
            _corners[0]->setParentItem(NULL);
            _corners[1]->setParentItem(NULL);
            _corners[2]->setParentItem(NULL);
            _corners[3]->setParentItem(NULL);
            delete _corners[0];
            delete _corners[1];
            delete _corners[2];
            delete _corners[3];
            if(compDefRect){
                qreal x1, y1, x2, y2;
                this->rect().getCoords(&x1,&y1,&x2,&y2);
                //qDebug()<<"unselect"<<x1<<y1<<x2<<y2<<this->rect()<<this->pos();
                bool changed=compDefRect->update(x1+pos().x(),y1+pos().y(),x2-x1,y2-y1);
                //inform scene about the altered rectangle
                MScene * p = dynamic_cast<MScene *>(scene());
                if((p!=nullptr))
                    p->emitRectChanged(this,newStatus);
            }
            this->setZValue(this->zValue()-1);

        }
        return newStatus;
    }
    /* nu merge
    if(change == ItemPositionHasChanged){
        qDebug()<<"ItemPositionHasChanged";
        MScene * p = dynamic_cast<MScene *>(scene());
        p->emitRectChanged(this,this->isSelected());
    } else {
        qDebug()<<"unknown"<<change;
    }
    */
    return QGraphicsItem::itemChange(change, value);
}

void ComponentRectItem::setCornerPositions()
{
    qreal x1, y1, x2, y2;
    MScene *s=dynamic_cast<MScene*>(scene());
    qreal scale=1;
    if(s)
        scale=s->scale;
    qreal handleSize=HANDLESIZE/scale;
    this->rect().getCoords(&x1,&y1,&x2,&y2);
    _corners[0]->setPos(x1, y1);
    _corners[1]->setPos(x2-handleSize, y1);
    _corners[2]->setPos(x2-handleSize, y2-handleSize);
    _corners[3]->setPos(x1, y2-handleSize);

}

//QRectF ComponentRectItem::boundingRect() const
//{
//    qreal x1, y1, x2, y2;
//    this->rect().getCoords(&x1,&y1,&x2,&y2);
//    return QRectF(x1-3,y1-3,x2-x1+6,y2-y1+6);
//}

bool ComponentRectItem::sceneEventFilter ( QGraphicsItem * watched, QEvent * event )
{
    //qDebug() << " QEvent == " + QString::number(event->type());

    CornerGrabber * corner = dynamic_cast<CornerGrabber *>(watched);
    if ( corner == NULL) return false; // not expected to get here
    QGraphicsSceneMouseEvent * mevent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
    if ( mevent == NULL)
    {
        // this is not one of the mouse events we are interrested in
        return false;
    }


    switch (event->type() ){
    // if the mouse went down, record the x,y movement of the press, record it inside the corner object
    case QEvent::GraphicsSceneMousePress:
        //corner->setMouseState(CornerGrabber::kMouseDown);
        corner->mouseMoveX = 0;//mevent->pos().x();
        corner->mouseMoveY = 0;//mevent->pos().y();
        return true;
        break;

    case QEvent::GraphicsSceneMouseRelease:
        //item is already resized, update the corresponding component rectangle definition
        this->setCornerPositions();
        this->update();
        if(compDefRect){
            updateCompDefRect();
            //inform scene about the altered rectangle
            MScene * p = dynamic_cast<MScene *>(scene());
            if(p!=nullptr)
                p->emitRectChanged(this,true);

        }
        return true;
        break;
    case QEvent::GraphicsSceneMove:
        if(compDefRect){
            qreal x1, y1, x2, y2;
            this->rect().getCoords(&x1,&y1,&x2,&y2);
            qWarning()<<"unexpected event";
            //qDebug()<<"move"<<x1<<y1<<x2<<y2<<this->rect()<<this->pos();
            bool changed=compDefRect->update(x1+pos().x(),y1+pos().y(),x2-x1,y2-y1);
            //inform scene about the altered rectangle
            MScene * p = dynamic_cast<MScene *>(scene());
            if((p!=nullptr))
                p->emitRectChanged(this,true);

        }
        return true;
    case QEvent::GraphicsSceneMouseMove:
        //handled below
        //return true;
        qDebug()<<"mouse move";
        break;
    default:
        return false;


    }
    qDebug()<<event->type();
    assert(event->type()==QEvent::GraphicsSceneMouseMove);
    //mouse move - adjust rectangle on the fly
    qreal x=mevent->pos().x();
    qreal y=mevent->pos().y();
    qreal dx=x-corner->mouseMoveX;
    qreal dy=y-corner->mouseMoveY;
    if(aspectRatio>0){
        /* if fixed aspect ratio has been requested, enforce the requested aspect ratio upon the movement of the corners */
        qreal dynew;
        switch(corner->getCorner()){
        case 0:
        case 2:
            //dynew=dxnew*aspectRatio
            //dxnew+dynew=dx+dy
            //dxnew(1+aspectRatio)=dx+dy
            dynew=(dx+dy)/(1+aspectRatio);
            dx=dynew*aspectRatio;//dx+dy-dxnew;
            dy=dynew;
            break;
        default:
            dynew=(dx-dy)/(1+aspectRatio);
            dx=dynew*aspectRatio;//-(dx-dy-dxnew);
            dy=-dynew;
            break;
        }
    }
    // apply the new coordinates upon the rectangle
    qreal x1, y1, x2, y2;
    this->rect().getCoords(&x1,&y1, &x2, &y2);
    qreal maxWidthShrink=std::numeric_limits<double>::max();
    {
        MScene * p = dynamic_cast<MScene *>(scene());
        if(compDefRect->rectangleType==CompDefRect::POSITIVE){
            if(p){
                qreal minwidth=sqrt(p->compDef->minArea*p->compDef->aspectRatio);
                if((compDefRect->orientation==CompDefRect::E)||
                        (compDefRect->orientation==CompDefRect::EW)||
                        (compDefRect->orientation==CompDefRect::W)){
                    minwidth/=p->compDef->aspectRatio;
                }
                for(int i=0; i<p->compDef->upsample; i++)
                    minwidth /= 2;
                qreal width=x2-x1;
                //qDebug()<<"minwidth"<<minwidth<<width<<p->compDef->aspectRatio;
                maxWidthShrink=width-minwidth;
            }
        } else {
            //minimumwidth==1
            maxWidthShrink=x2-x1-1;
        }
    }
    switch(corner->getCorner()){
    case 0:
        if(dx > maxWidthShrink){
            dx=maxWidthShrink;
            dy=dx/aspectRatio;
        }
        x1+=dx;
        y1+=dy;
        break;
    case 1:
        if(-dx > maxWidthShrink){
            dx=-maxWidthShrink;
            dy=-dx/aspectRatio;
        }
        x2+=dx;
        y1+=dy;
        break;
    case 2:
        if(-dx > maxWidthShrink){
            dx=-maxWidthShrink;
            dy=dx/aspectRatio;
        }
        x2+=dx;
        y2+=dy;
        break;
    case 3:
        if(dx > maxWidthShrink){
            dx=maxWidthShrink;
            dy=-dx/aspectRatio;
        }
        x1+=dx;
        y2+=dy;
        break;
    }
    //qDebug()<<"x1"<<x1<<y1<<x2<<y2<<dx<<dy<<x<<y<<aspectRatio;
    this->setRect(x1,y1,x2-x1,y2-y1);
    //this->setCornerPositions();
    this->update();
    corner->mouseMoveX=x;
    corner->mouseMoveY=y;
    return true;
}


