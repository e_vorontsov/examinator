/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "widget.h"
#include "testinterface.h"
#include <QApplication>
#include <QLocale>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("cicor");
    a.setApplicationName("examinator");
    a.setApplicationVersion(APP_VERSION);

    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
    parser.setApplicationDescription("Automatic optical inspection programm using a webcam");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("inspectionProgramm", QObject::tr("Numele programului de inspecție optică de încărcat"));
    QCommandLineOption dbOption("db", QObject::tr("Fișier bază de date sqlite de deschis"),"dbname");
    parser.addOption(dbOption);
    QCommandLineOption dbOption2("LANG", QObject::tr("Limbă folosită pentru interfața programului"),"language");
    parser.addOption(dbOption2);
    //parser.showHelp();

    //parser.process(*qApp);
    parser.parse(qApp->arguments());
    QString language="en";
    if(parser.isSet("LANG")){
        if((parser.value("LANG").compare("ro",Qt::CaseInsensitive)==0)||
                (parser.value("LANG").startsWith("ro_",Qt::CaseInsensitive))){
            language="ro";
        }
    } else {
        QStringList languages=QLocale().uiLanguages();
        if(languages.at(0).startsWith("ro",Qt::CaseInsensitive))
            language="ro";

    }
    QTranslator myappTranslator;
    if(language!="ro"){
        qDebug()<<"English Selected!";
        myappTranslator.load(":/i18n/examinator.qm");
       qApp->installTranslator(&myappTranslator);
    }

    qDebug()<<qApp->arguments();
    qDebug()<<"names"<<parser.optionNames();
    qDebug()<<parser.values("db")<<parser.value(dbOption)<<parser.errorText();


 // QDir dir;
 //   qDebug()<<dir.absolutePath();
 //   qDebug()<<QResource::registerResource(dir.absolutePath()+"/translation.rcc");
    //Widget w;
   TestInterface w;
   w.show();

    return a.exec();
}
