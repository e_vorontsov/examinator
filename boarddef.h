/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDDEF_H
#define BOARDDEF_H
#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif
#include <QList>
#include <QImage>
#include "compdef.h"
#include "boardcomptype.h"
#include "detectedcomponent.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <QDebug>

#ifndef CV_BGR2RGB
#define CV_BGR2RGB cv::COLOR_BGR2RGB
#endif

class Features{
public:
    cv::Mat descriptors;
    std::vector<cv::KeyPoint> keypoints;
    bool isEmpty(void);
};

class BoardDef
{
public:
    enum AlignMethod {FIDUCIAL=0, AKAZE=1, ORB=2};
    enum TestStartType {STARTIMMEDIATE=0, STARTDELAYED=1, STARTONCLICK=2};
    enum DetailType {ALIGNFIDUCIAL=0,/* 1,2...9=AKAZE, ORB...*/ STARTMETHOD=10};
    BoardDef();
    QString name;
    QString refImagePath;
    CompDef refClassifier;
    void saveToDB(void);
    long long saveToDBAs(QString newName);
    void loadFromDB(const long long id, bool loadForTest=false);
    void removeFromDB(void);
    bool dirty;
    CompDefRect refRect;
    //used to initialize
    BoardComp recentComp;
    /// active component type
    BoardCompType * compType;
    QList<BoardCompType> compTypes;
    long long id;
    int minArea;
    int maxArea;
    AlignMethod alignMethod=FIDUCIAL;
    QString snValidator;
    QString testCodeOverride;
    QList<DetectedComponent> detectComponents(QVector<QImage> img, CompDef * comp,
                                              double scale, bool displaySearchFrames, double adjustThreshold);

    QList<DetectedComponent> detectComponentsImg(const QImage img, CompDef * comp,
                                              bool displaySearchFrames, double adjustThreshold);
    QList<DetectedComponent> testBoard(const QImage &img, DetectedComponent & boardRect, bool displaySearchFrames);
    void adjustDetectedComponents(QList<DetectedComponent> & dets, double chipwidth, double chipheight,
                                  int angle, double xoffset=0, double yoffset=0, double scale=1);
    /**
     * @brief check if detections overlap and remove the components with the lower detection score
     * @param components - list of components to be checked
     * @return the number of removed components
     */
    int removeOverlaps(QList<DetectedComponent> & components);
    /**
     storage for keypoints and the corresponding keypoint descriptors for the reference image
     these keypoints will be stored to the database, in order to be used for image alignment
     */
    Features refFeatures;
    void binaryDescriptors(Features & features, cv::Mat image, const CompDefRect &roi);
    void saveBinaryFeatures(void);
    void loadBinaryFeatures(void);
    static cv::Mat qImage2Mat(QImage const& src, bool switchRedBlue=true);
    int testStartType=0;
    int testStartDelay=2000;
    ///check if a barcode location for the serial number has been defined
    bool hasBarcodeSN(void);
    /// run the board definition on the given image
    /// return status in testOk, and the actual results as list of DetectedComponents, one for each
    /// instance of the board found
    /// if resultsDile is given, the results are also saved into the file with this name. If oldResults
    /// is also provided, the decisins from the oldResult ar merged with the current result
    QList<DetectedComponent> runOnImage(QImage image, bool displaySearchFrames,
                                        bool &testOk, QString resultsFile=QString(),
                                        const QList<DetectedComponent> &oldResults=QList<DetectedComponent>(),
                                        int *finalDecision=nullptr);
    /// get classifiers from boardComp id
    const QList<QSharedPointer<CompDef> > classifierFromComponent(long long component);
    static int calculateDecision(const QList<DetectedComponent> &boards);

private:
    //double x, y, w, h;
    //MemCompDefRect::Orientation orientation;
    /**
     * @brief findBoardComponents find all fiducials or all other componente corresponding to a board in the image
     * @param img image to be analyzed
     * @param transform trannsform to be applied to get the expected components coordinates from the defined component coordinates
     * @param boardRect detected board rectangle
     * @param fiducial boolean specifying if to find fiducials or components
     * @param displaySearchFrames boolean specifying if to show the ROIs for each component.
     * @return returns the list of found components. If a component is not found, a failing rectangle with the expected position is returned.
     */
    QList<DetectedComponent> findBoardComponents(const QImage &img, const cv::Mat & transform,
                                                 const DetectedComponent & boardRect, bool fiducial,
                                                 bool displaySearchFrames);
    /**
     */
    DetectedComponent findOneBoardComponent(DetectedComponent comp, const BoardComp & c, double xoffset, double yoffset, int angle, int rot90,
                                            double wChip, double hChip, double chipScale, const BoardCompType &cT,
                                            bool fiducial, const QImage qChip2, bool displaySearchFrames);

    // from https://stackoverflow.com/questions/17127762/cvmat-to-qimage-and-back
    inline QImage mat2QImage(cv::Mat const& src)
    {
         cv::Mat temp; // make the same cv::Mat
         cv::cvtColor(src, temp,CV_BGR2RGB); // cvtColor Makes a copy, that's what we need
         QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
         dest.bits(); // enforce deep copy, see documentation
         // of QImage::QImage ( const uchar * data, int width, int height, Format format )
         return dest;
    }

    void qImage2array2d(QImage const& src, dlib::array2d<dlib::rgb_pixel> &dst);
    /**
     * @brief helper function to convert the vertices of a component rect to a vector of cv::Point2f
     * @param rect - input rectangle
     * @return returns a vector of four points, in clockwise order starting top-left
     */
    std::vector<cv::Point2f> rectPointList(const CompDefRect & rect);
    /**
     * @brief padding - constant used to define extra padding, besides the specified tollerances, for the image chips on which co
     */
    const double padding=0.5;
    /// attempt alignment of the scene using ORB/AKAZE features. The reference features are loaded from the database.
    /// this function extracts features from the scene and attempts to match them to the reference features.
    /// If a match is found, the transformation matrix gets updated, otherwise it remains untouched
    bool alignBinaryFeatures(const QImage img, const DetectedComponent & boardRect, cv::Mat &transform);
    //QList<DetectedComponent> testBoard(const QImage &img, DetectedComponent & boardRect);
    /// calculate the total error in the destination space by adding up the distances between the destination points and
    /// the points obtained by applying the provided transformation onto  the source points
    double transformError(std::vector<cv::Point2f>srcPts, std::vector<cv::Point2f>dstPts,cv::Mat transform);
    /// merge the decision from the oldResults with the results
    /// returns the global resulting decision and updates the decisions for the found components
    int mergeResults(QList<DetectedComponent> &results, const QList<DetectedComponent> &oldResults);
};


#endif // BOARDDEF_H
