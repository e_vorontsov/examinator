/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ipcamera.h"

IpCamera::IpCamera(QObject *parent) : QObject(parent)
{
    connect(&manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
}

void IpCamera::setUrl(QString url)
{
    qDebug()<<"set url"<<url;
    this->url=QUrl(url);
}

void IpCamera::capture()
{
    qDebug()<<"capture";

    manager.get(QNetworkRequest(url));
}

void IpCamera::replyFinished(QNetworkReply *reply)
{
    qDebug()<<"reply";
    if(reply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
        emit captureError(reply->errorString());
        return;
    }
    QByteArray buf=reply->readAll();
    QPixmap pixmap;
    bool ok=pixmap.loadFromData(buf);
    if(ok){
        qDebug()<<"imageCaptured"<<ok<<buf.length()<<reply;
        emit imageCaptured(pixmap);
    }else{
        qDebug()<<"unable to convert received data to image"<<ok<<buf.length()<<reply;
        emit captureError("unable to convert received data to image");
    }
    reply->deleteLater();
}
