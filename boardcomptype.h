/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARDCOMPTYPE_H
#define BOARDCOMPTYPE_H
#include <QList>
#include <QMap>
#include <QSqlRecord>
#include <QSharedPointer>

#include "compdef.h"
#include "boardcomp.h"

class BoardCompType
{
public:
    enum ComponentType {COMPONENT=0, FIDUCIAL=1, BARCODE=2, MAXCOMPONENTTYPE} ;
    BoardCompType();
    ~BoardCompType();
    BoardCompType(QSqlRecord record, bool loadForTest=false);
    QString name;
    ComponentType componentType;
    long long id;
    void saveToDB(long long boardDef);
    QMap<long long,QSharedPointer<CompDef> > classifiers;
    QList<BoardComp> components;
    BoardComp * addNewComp(const double x, const double y, const BoardComp &recentComp);
    double aspectRatio(void);
    //returns the minimum area of a sample, considering that it will be upsampled as specified
    double minArea(void) const;
    unsigned upsample(void) const;
private:
};

#endif // BOARDCOMPTYPE_H
