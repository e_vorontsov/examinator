/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef IPCAMERA_H
#define IPCAMERA_H

#include <QObject>
#include <QPixmap>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>


class IpCamera : public QObject
{
    Q_OBJECT
public:
    explicit IpCamera(QObject *parent = nullptr);

signals:
    void imageCaptured(QPixmap image);
    void captureError(QString errorMessage);

public slots:
    void setUrl(QString url);
    void capture(void);
private slots:
    void replyFinished (QNetworkReply *reply);
private:
    QUrl url;
    QNetworkAccessManager manager;
};

#endif // IPCAMERA_H
