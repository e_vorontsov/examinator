/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DETECTEDCOMPONENT_H
#define DETECTEDCOMPONENT_H
#include "compdefrect.h"
#include "boardcomp.h"
#include <QList>
#include <dlib/image_processing.h>
#include <QDebug>

class DetectedComponent : public CompDefRect
{
public:
    enum Status {Expected=0, Detected=1, ExpectedFiducial=2, DetectedFiducial=3, ExpectedBarcode=4, DetectedBarcode=5};
    DetectedComponent();
    double score[4]={-10, -10, -10, -10}, xerr=-1, yerr=-1, scaleerr=-1, red=-1, green=-1, blue=-1, hue=-1, sat=-1, val=-1;
    BoardComp boardComp;
    QList<DetectedComponent> parts;
    Status status=Expected;
    int decision=-1;// -1=UNDEFINED, 0=WRONG, 1=RIGHT
    CompDefRect expected;
    friend void serialize (const DetectedComponent& item, std::ostream& out);
    friend void deserialize (DetectedComponent& item, std::istream& in);
};

template<typename itemType>
inline void serialize(const QList<itemType> &item, std::ostream &out){
    dlib::serialize(item.count(), out);
    for(int i=0; i<item.count(); i++){
        serialize(item[i], out);
    }
}
template<typename itemType>
inline void deserialize(QList<itemType> &item, std::istream &in){
    int cnt;
    dlib::deserialize(cnt, in);
    for(int i=0; i<cnt; i++){
        itemType element;
        deserialize(element, in);
        item.append(element);
    }
}

inline void serialize(const DetectedComponent &item, std::ostream &out)
{
    int version = 2;
    dlib::serialize(version, out);
    const CompDefRect &itemBase=static_cast<const CompDefRect&>(item);
    serialize(itemBase, out);
    dlib::serialize(item.score, out);
    dlib::serialize(item.xerr, out);
    dlib::serialize(item.yerr, out);
    dlib::serialize(item.scaleerr, out);
    dlib::serialize(item.red, out);
    dlib::serialize(item.green, out);
    dlib::serialize(item.blue, out);
    dlib::serialize(item.hue, out);
    dlib::serialize(item.sat, out);
    dlib::serialize(item.val, out);
    serialize(item.boardComp, out);

    /*
    dlib::serialize(item.parts.count(), out);
    for(int i=0; i<item.parts.count(); i++){
        serialize(item.parts[i], out);
    }
    */
    serialize(item.parts, out);
    dlib::serialize(item.status, out);
    dlib::serialize(item.decision, out);
    serialize(item.expected, out);
}

inline void deserialize (DetectedComponent& item, std::istream& in)
{
    //qDebug()<<"deserialize DetectedComponent";
    int version = 0;
    dlib::deserialize(version, in);
    if ((version != 1) && (version != 2))
        throw dlib::serialization_error("Unexpected version found while deserializing DetectedComponent.");
    CompDefRect &itemBase=static_cast<CompDefRect&>(item);
    deserialize(itemBase, in);
    int tmp;
    //qDebug()<<"deserialize DetectedComponent score";
    dlib::deserialize(item.score, in);
    //qDebug()<<"deserialize DetectedComponent xerr";
    dlib::deserialize(item.xerr, in);
    dlib::deserialize(item.yerr, in);
    dlib::deserialize(item.scaleerr, in);
    dlib::deserialize(item.red, in);
    dlib::deserialize(item.green, in);
    dlib::deserialize(item.blue, in);
    dlib::deserialize(item.hue, in);
    dlib::deserialize(item.sat, in);
    dlib::deserialize(item.val, in);
    //qDebug()<<"deserialize DetectedComponent boardComp";
    deserialize(item.boardComp, in);
    /*
    dlib::deserialize(tmp, in);
    for(int i=0; i<tmp; i++){
        DetectedComponent d;
        deserialize(d, in);
        item.parts.append(d);
    }*/
    deserialize(item.parts, in);
    dlib::deserialize(tmp, in);
    item.status=static_cast<DetectedComponent::Status>(tmp);
    if(version>1)
        dlib::deserialize(item.decision, in);
    deserialize(item.expected, in);

}


#endif // DETECTEDCOMPONENT_H
