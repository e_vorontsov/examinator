/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "compdefimage.h"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>

CompDefImage::CompDefImage()
{
    id=-1;
    implicitNegative=true;
}

CompDefImage::CompDefImage(QString filename)
{
    id=-1;
    imagePath=filename;
    implicitNegative=true;
}

CompDefImage::CompDefImage(QSqlRecord record, QString newPath)
{
    id=record.value("id").toLongLong();
    imagePath=record.value("imagepath").toString();
    if(!newPath.isEmpty())
        imagePath=newPath;
    implicitNegative=record.value("implicitnegative").toBool();
    QSqlQuery query;
    QString sql;
    sql="SELECT * FROM compdefrects WHERE compdefimage="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    while(query.next()){
        rect.append(CompDefRect(query.record()));
    }
}

CompDefRect * CompDefImage::addNewRect(double x, double y, double w, double h, CompDefRect::Orientation orientation)
{
    CompDefRect rectangle(x, y, w, h, orientation, CompDefRect::POSITIVE);
    qDebug()<<rectangle.x<<rectangle.y;
    rect.append(rectangle);
    return & rect.last();

}

void CompDefImage::saveToDB(long long compdef)
{
    QSqlQuery query;
    QString sql;
    //save image
    if(id>=0){
        //update already existing component definition
        sql="UPDATE compdefimages SET imagepath=:imagepath"
                ", implicitnegative="+QString::number(implicitNegative)+
                " WHERE id="+ QString::number(id);
    } else {
        sql="INSERT INTO compdefimages (imagepath, compdef, implicitnegative)"
                "VALUES ( :imagepath, "+QString::number(compdef)+
                ", "+QString::number(implicitNegative)+")";
    }
    query.prepare(sql);
    query.bindValue(":imagepath",imagePath.toLatin1());
    qDebug()<<"saving image"<<sql;
    if(!query.exec()){
        qDebug()<<query.lastError();
        return;
    }
    //keep id if it's new
    bool isNewImage=false;
    if(id<0){
        isNewImage=true;
        id=query.lastInsertId().toLongLong();
    } else {
        //delete deleted rects from database
        QString idList;
        for(int i=0; i<rect.count(); i++) {
            if(rect[i].id<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(rect[i].id));
        }
        sql="DELETE FROM compdefrects WHERE compdefimage="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        query.exec(sql);
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
    }
    //update or insert rectangles
    for(int i=0; i<rect.count(); i++) {
        if(isNewImage)
            rect[i].id=-1;
        if(rect[i].id<0){
            sql="INSERT INTO compdefrects (x, y, w, h, orientation, rectangletype, compdefimage)"
                    "VALUES ("+QString::number(rect[i].x)+","+
                    QString::number(rect[i].y)+","+
                    QString::number(rect[i].w)+","+
                    QString::number(rect[i].h)+","+
                    QString::number(rect[i].orientation)+","+
                    QString::number(rect[i].rectangleType)+","+
                    QString::number(id)+
                    ")";
        } else {
            //image is not changing
            sql="UPDATE compdefrects SET "
                    "x="+QString::number(rect[i].x)+", "+
                    "y="+QString::number(rect[i].y)+", "+
                    "w="+QString::number(rect[i].w)+", "+
                    "h="+QString::number(rect[i].h)+", "+
                    "orientation="+QString::number(rect[i].orientation)+", "+
                    "rectangletype="+QString::number(rect[i].rectangleType)+
                    " WHERE id="+QString::number(rect[i].id);
        }
        qDebug()<<sql;
        if(!query.exec(sql)){
            qDebug()<<query.lastError().text();
        }
        if(rect[i].id<0){
            rect[i].id=query.lastInsertId().toLongLong();
        }
    }

}
