#ifndef BARCODE_H
#define BARCODE_H
#include <QImage>
#include <QString>
#include <QDebug>
#ifdef ZBAR
#include <zbar.h>
#endif
#ifdef DMTX
#include <dmtx.h>
#endif
#include <QStringList>


class Barcode
{
public:
    Barcode();
    enum BCType {ALLCODES=0, DATAMATRIX=1, QRCODE=2, CODE39=3, CODE128=4} ;
    static const QStringList bcTypes;
    static QString decodeBC(const QImage img, int &errorCode, BCType symbology=ALLCODES);
private:
#ifdef ZBAR
    static QString decodeBCZbar(const uchar * dataPtr, const int w, const int h, const int l,
                                const BCType symbology, int & errorCode);
#endif
#ifdef DMTX
    static QString decodeBCDmtx(uchar *dataPtr, const int w, const int h, int & errorCode);
#endif
};

#endif // BARCODE_H
