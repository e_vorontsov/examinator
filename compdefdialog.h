/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMPDEFDIALOG_H
#define COMPDEFDIALOG_H

#include <QDialog>
#include <QAbstractButton>
#include <QtConcurrent/QtConcurrentRun>
#include <QTime>
#include <QTimer>
#include <QDoubleSpinBox>
#include <QComboBox>

#include "compdef.h"
//return value after importing autoinspect data
#define IMPORTAI 1001

namespace Ui {
class CompDefDialog;
}

/**
 * @brief The CompDefDialog class is a simple dialog used to configure a component definition
 *
 * This class receives a component definition trough the function setCompDef, displays the
 * relevant configuration parameters using input controls, and saves the changes back to
 * the component definition when requested.
 */
class CompDefDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CompDefDialog(QWidget *parent = nullptr);
    ~CompDefDialog();
    /**
     * @brief load the component definition
     * @param comp pointer to the component definition to be configured
     */
    void setCompDef(CompDef* comp);

private:
    Ui::CompDefDialog *ui;
    CompDef * compDef;
    void storeConfig();
    QFuture<void> trainFuture;
    int trainingTab=1;
    QTimer timer;
    QTime timeRunning;
    bool setValueSpin(QString text, QDoubleSpinBox * spinBox);
    bool setDeviationSpin(QString text, QDoubleSpinBox *spinBox);
    void loadBoarListDNN(QComboBox * comboBox, long long selectedId=-1);
private slots:
    void accept(void);
//    void reject(void);
    /**
     * @brief check if to save or discard the changes, do as requested and close the dialog
     * @param button
     */
    void on_buttonBox_clicked(QAbstractButton *button);
    void on_fhogTrainBtn_clicked();
    void on_importBtn_clicked();
    void timerSlot(void);
    void on_applyColorBtn_clicked();
    void on_saveDatasetBtn_clicked();
    void on_vValLbl_clicked();
    void on_sValLbl_clicked();
    void on_hValLbl_clicked();
    void on_bValLbl_clicked();
    void on_gValLbl_clicked();
    void on_rValLbl_clicked();
    void on_rDevLbl_clicked();
    void on_gDevLbl_clicked();
    void on_bDevLbl_clicked();
    void on_hDevLbl_clicked();
    void on_sDevLbl_clicked();
    void on_vDevLbl_clicked();
    void showR();
    void showG();
    void showB();
    void showH();
    void showS();
    void showV();
    void on_rValSpin_valueChanged(double arg1);
    void on_gValSpin_valueChanged(double arg1);
    void on_bValSpin_valueChanged(double arg1);
    void on_hValSpin_valueChanged(double arg1);
    void on_sValSpin_valueChanged(double arg1);
    void on_vValSpin_valueChanged(double arg1);
    void on_rDevSpin_valueChanged(double arg1);
    void on_gDevSpin_valueChanged(double arg1);
    void on_bDevSpin_valueChanged(double arg1);
    void on_hDevSpin_valueChanged(double arg1);
    void on_sDevSpin_valueChanged(double arg1);
    void on_vDevSpin_valueChanged(double arg1);
    void on_spTrainBtn_clicked();
    void on_dnnTrainBtn_clicked();
    void on_importDNNBtn_clicked();
    void on_exportDNNBtn_clicked();
};

#endif // COMPDEFDIALOG_H
