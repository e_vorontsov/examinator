/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "boarddef.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QDateTime>
#include <QLabel>
#include <dlib/opencv/cv_image.h>
#include <dlib/gui_widgets.h>
#ifdef BARCODEREAD
#include "barcode.h"
#endif
//#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

//for estimateRigidTransform
#include <opencv2/video/tracking.hpp>
#ifndef CV_RANSAC
#define CV_RANSAC cv::RANSAC
#endif
#ifndef CV_FILLED
#define CV_FILLED cv::FILLED
#endif

#ifdef PARALLELCOMPDET
#include <QtConcurrent>
#endif
BoardDef::BoardDef()
{
    /*
    result=query.exec("CREATE TABLE IF NOT EXISTS boards "
              "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
              "refimagepath VARCHAR(256), "
                      // reference object image location
              "x DOUBLE, "
              "y DOUBLE, "
              "w DOUBLE, "
              "h DOUBLE, "
              "orientation INTEGER, "
              "sizetol DOUBLE, "

              "refclassifier INTEGER"
              ")");
    */
    compType=nullptr;
    id=-1;
    dirty=true;
    refRect.rectangleType=CompDefRect::BOARDRECT;
    refRect.x=0;
    refRect.y=0;
    refRect.w=0;
    refRect.h=0;
    refRect.orientation=CompDefRect::N;
    minArea=6400;
    maxArea=4000000;
    name=QDateTime::currentDateTime().toString();
    recentComp.w = 20;
    recentComp.h = 20;
    recentComp.orientation=CompDefRect::N;
}

void BoardDef::saveToDB()
{
    qDebug()<<"saving boardcomptype";
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    if(id>=0){
        //update already existing board definition
        sql="UPDATE boards SET refimagepath= :refimagepath"
                ", name='"+name+"', x="+
                QString::number(refRect.x)+", y="+
                QString::number(refRect.y)+", w="+
                QString::number(refRect.w)+", h="+
                QString::number(refRect.h)+", orientation="+
                QString::number(refRect.orientation)+", minarea="+
                QString::number(minArea)+", maxarea="+
                QString::number(maxArea)+", refclassifier="+
                QString::number(refClassifier.id)+
                ", alignmethod=" + QString::number(alignMethod) +
                ", testcodeoverride='" + testCodeOverride +"'"+
                ", snvalidator=:snvalidator"+
                " WHERE id="+
                QString::number(id);

    } else {
        //save new board
        sql="INSERT INTO boards (refimagepath, name, x, y, w, h, orientation, "
            "minarea, maxarea, refclassifier, alignmethod, snvalidator, testcodeoverride)"
            "VALUES ( :refimagepath, '"+
                name+"', "+
                QString::number(refRect.x)+", "+
                QString::number(refRect.y)+", "+
                QString::number(refRect.w)+", "+
                QString::number(refRect.h)+", "+
                QString::number(refRect.orientation)+", "+
                QString::number(minArea)+", "+
                QString::number(maxArea)+", "+
                QString::number(refClassifier.id)+", "+
                QString::number(alignMethod)+", "+
                "'"+testCodeOverride+"', "+
                ":snvalidator"+
                            ")";
    }
    query.prepare(sql);
    query.bindValue(":refimagepath", refImagePath.toLatin1());
    query.bindValue(":snvalidator", snValidator.toLatin1());
    if(!query.exec()){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    if(id<0){
        id=query.lastInsertId().toLongLong();
    } else {
        //delete deleted component types from database
        QString idList;
        for(int i=0; i<compTypes.count(); i++) {
            if(compTypes.at(i).id<0)
                continue;
            if(!idList.isEmpty())
                idList.append(",");
            idList.append(QString::number(compTypes.at(i).id));
        }
        //delete board components
        sql="DELETE FROM boardcomponents WHERE comptype IN (SELECT id FROM boardcomptypes WHERE "
            "board="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        sql+=")";
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        //delete component type classifiers
        sql="DELETE FROM boardcompclassifiers WHERE comptype IN (SELECT id FROM boardcomptypes WHERE "
            "board="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        sql+=")";
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }
        //delete component types
        sql="DELETE FROM boardcomptypes WHERE board="+QString::number(id);
        if(!idList.isEmpty())
            sql.append(" AND id NOT IN ("+idList+")");
        if(!query.exec(sql)){
            qDebug()<<query.lastQuery();
            qDebug()<<query.lastError().text();
        }

    }
    for(int i=0; i<compTypes.count(); i++){
        compTypes[i].saveToDB(id);
    }
    sql="DELETE FROM boarddetails WHERE board="+ QString::number(id)+
            " AND type="+QString::number(STARTMETHOD);
    if(!query.exec(sql)){
        qDebug()<<"error while deleting ORB"<<query.lastError()<<query.lastQuery();
    }
    sql="INSERT INTO boarddetails (board, type, data) VALUES("+QString::number(id)+
            ", "+QString::number(STARTMETHOD)+", :startmethod)";
    query.prepare(sql);
    QString str;
    str=str.asprintf("%d %d",testStartType, testStartDelay);
    query.bindValue(":startmethod", str.toLatin1());
    if(!query.exec()){
        qDebug()<<"error while saving start type"<<query.lastError()<<query.lastQuery();
    }
    else {
        qDebug()<<"saved start type with"<<query.lastQuery()<<str.toLatin1();
    }

    dirty=false;
}

long long BoardDef::saveToDBAs(QString newName)
{
    qDebug()<<"saving boardcomptype copy";
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    name = newName;
    {
        //save board copy
        sql="INSERT INTO boards (refimagepath, name, x, y, w, h, orientation, "
            "minarea, maxarea, refclassifier, alignmethod, testcodeoverride, snvalidator)"
            " VALUES ( :refimagepath , '"+
                name+"', "+
                QString::number(refRect.x)+", "+
                QString::number(refRect.y)+", "+
                QString::number(refRect.w)+", "+
                QString::number(refRect.h)+", "+
                QString::number(refRect.orientation)+", "+
                QString::number(minArea)+", "+
                QString::number(maxArea)+", "+
                QString::number(refClassifier.id)+", "+
                QString::number(alignMethod)+", "+
                "'"+testCodeOverride+"', "+
                ":snvalidator"
                            " )";
    }
    query.prepare(sql);
    query.bindValue(":refimagepath", refImagePath.toLatin1());
    query.bindValue(":snvalidator", snValidator.toLatin1());
    if(!query.exec()){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return -1;
    }
    {
        id=query.lastInsertId().toLongLong();
    }
    for(int i=0; i<compTypes.count(); i++){
        compTypes[i].id=-1;
        compTypes[i].saveToDB(id);
    }
    sql="INSERT INTO boarddetails (board, type, data) VALUES("+QString::number(id)+
            ", "+QString::number(STARTMETHOD)+", :startmethod)";
    query.prepare(sql);
    QString str;
    str=str.asprintf("%d %d",testStartType, testStartDelay);
    query.bindValue(":startmethod", str.toLatin1());
    if(!query.exec()){
        qDebug()<<"error while saving start type"<<query.lastError()<<query.lastQuery();
    }
    else qDebug()<<"saved start type with"<<query.lastQuery()<<str.toLatin1();

    dirty=false;
    return id;
}

void BoardDef::loadFromDB(const long long id, bool loadForTest)
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    sql="SELECT * FROM boards WHERE id="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    if(!query.next()){
        qDebug()<<"no record found for"<<id;
        return;
    }
    //qDebug()<<"BoardDef::loadFromDB"<<query.record();
    dirty=false;
    this->id=id;
    QString str=query.value("refimagepath").toString();
    refImagePath=str;
    name=query.value("name").toString();
    refRect.x=query.value("x").toDouble();
    refRect.y=query.value("y").toDouble();
    refRect.w=query.value("w").toDouble();
    refRect.h=query.value("h").toDouble();
    refRect.orientation=static_cast<CompDefRect::Orientation>(query.value("orientation").toInt());
    minArea=query.value("minarea").toInt();
    maxArea=query.value("maxarea").toInt();
    alignMethod=static_cast<AlignMethod>( query.value("alignmethod").toInt());
    testCodeOverride=query.value("testcodeoverride").toString();
    snValidator=query.value("snvalidator").toString();
    long long cid=query.value("refclassifier").toLongLong();
    refClassifier.loadFromDB(cid, false);
    if(loadForTest)
        refClassifier.prepareToRun();
    sql="SELECT * FROM boardcomptypes WHERE board="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError();
        return;
    }
    while(query.next()){
        compTypes.append(BoardCompType(query.record(), loadForTest));
    }
    if(minArea<refClassifier.minArea)
        minArea=refClassifier.minArea;
    if(maxArea<minArea)
        maxArea=minArea;
    if((alignMethod==ORB)||(alignMethod==AKAZE)){
        loadBinaryFeatures();
    }
    sql="SELECT * FROM boarddetails WHERE board="+QString::number(id)+" AND type="+QString::number(STARTMETHOD);
    query.exec(sql);
    if(query.next()){
        QByteArray tmp=query.value("data").toByteArray();
        int found=std::sscanf(tmp.constData(),"%d %d", &testStartType, &testStartDelay);
        qDebug()<<"startmode"<<testStartType<<testStartDelay;
        if(found!=2){
            qDebug()<<"loading start mode failed, data is"<<tmp;
        }
    } else {
        //keep default values
        ;
    }
}

void BoardDef::removeFromDB()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql;
    //delete board components
    sql="DELETE FROM boardcomponents WHERE comptype IN (SELECT id FROM boardcomptypes WHERE "
        "board="+QString::number(id)+")";
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
    }
    //delete component type classifiers
    sql="DELETE FROM boardcompclassifiers WHERE comptype IN (SELECT id FROM boardcomptypes WHERE "
        "board="+QString::number(id)+")";
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
    }
    //delete component types
    sql="DELETE FROM boardcomptypes WHERE board="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
    }
    //delete boarddetails
    sql="DELETE FROM boarddetails WHERE board="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
    }
    //delete board
    sql="DELETE FROM boards WHERE id="+QString::number(id);
    if(!query.exec(sql)){
        qDebug()<<query.lastQuery();
        qDebug()<<query.lastError().text();
    }


}

QList<DetectedComponent> BoardDef::detectComponents(QVector<QImage> img, CompDef *comp, double scale,
                                                    bool displaySearchFrames, double adjustThreshold)
{
    qDebug()<<"detectComponents";
    QList<DetectedComponent> detected;
#if 0
    static QLabel lbl;
    lbl.setPixmap(QPixmap::fromImage( img ));
    lbl.show();
#endif
    if(comp!=nullptr){
        if(!comp->compDNNParams.enabled){
            if(comp->numFHOGDetectors()==4){
                img[1]=QImage();
                img[2]=QImage();
                img[3]=QImage();
            } else if(comp->numFHOGDetectors()==2){
                img[2]=QImage();
                img[3]=QImage();
            }
        }
    }
    int nImages=0;
    for(int i=0; i<4; i++){
        if(!img[i].isNull())
            nImages++;
    }
#ifdef PARALLELCOMPDET
    //don't run multiple dnn instances in parallel
    //don't run as thread if only one image is available;
    if((nImages>1)&&(!comp->compDNNParams.enabled)){
        if(comp!=nullptr){
            QThreadPool::globalInstance()->setMaxThreadCount(QThread::idealThreadCount());
        }
        QList<QFuture<QList<DetectedComponent>>> results;
        if((compType!=nullptr)||(comp!=nullptr)){
            //load classifiers
            if(comp!=nullptr){
                comp->prepareToRun();
            } else {
                foreach(auto classifier, compType->classifiers){
                    classifier->prepareToRun();
                }
            }

            if(!img[0].isNull()){
                results.append(QtConcurrent::run(this, &BoardDef::detectComponentsImg,
                                                 img[0], comp, displaySearchFrames, adjustThreshold));
            }
            if(!img[1].isNull()){
                results.append(QtConcurrent::run(this, &BoardDef::detectComponentsImg,
                                                 img[1], comp, displaySearchFrames, adjustThreshold));
            }
            if(!img[2].isNull()){
                results.append(QtConcurrent::run(this, &BoardDef::detectComponentsImg,
                                                 img[2], comp, displaySearchFrames, adjustThreshold));
            }
            if(!img[3].isNull()){
                results.append(QtConcurrent::run(this, &BoardDef::detectComponentsImg,
                                                 img[3], comp, displaySearchFrames, adjustThreshold));
            }
            qDebug()<<"results"<<results.count();
            if(!img[0].isNull()){
                results.first().waitForFinished();
                qDebug()<<"finished";
                QList<DetectedComponent> det;
                det=results.takeFirst().result();
                adjustDetectedComponents(det,img[0].width()*scale, img[0].height()*scale,
                        0, 0, 0, scale);
                detected=det;
            }
            qDebug()<<detected.count();
            if(!img[1].isNull()){
                QList<DetectedComponent> det;
                det=results.takeFirst().result();
                adjustDetectedComponents(det,img[1].width()*scale, img[1].height()*scale,
                        270, 0, 0, scale);
                detected.append(det);
            }
            qDebug()<<detected.count();
            if(!img[2].isNull()){
                QList<DetectedComponent> det;
                det=results.takeFirst().result();
                adjustDetectedComponents(det,img[2].width()*scale, img[2].height()*scale,
                        180, 0, 0, scale);
                detected.append(det);
            }
            qDebug()<<detected.count();
            if(!img[3].isNull()){
                QList<DetectedComponent> det;
                det=results.takeFirst().result();
                adjustDetectedComponents(det,img[3].width()*scale, img[3].height()*scale,
                        90, 0, 0, scale);
                detected.append(det);
            }
            qDebug()<<detected.count();
        }
    } else
#endif
    {
        if((compType!=nullptr)||(comp!=nullptr)){
            if(!img[0].isNull()){
                QList<DetectedComponent> det;
                //qDebug()<<"notnull";
                dlib::array2d<dlib::rgb_pixel> image;
                qImage2array2d(img[0], image);
                qDebug()<<"cv_image";
                if(comp!=nullptr){
                    qDebug()<<"Running classifier"<<comp->name;
                    det=comp->detectObjects(image, displaySearchFrames);
                    qDebug()<<"found"<<detected.count();
                } else {
                    foreach(auto classifier, compType->classifiers){
                        qDebug()<<"running classifier"<<classifier->name;
                        det.append(classifier->detectObjects(image, displaySearchFrames));
                        qDebug()<<"found"<<detected.count();
                    }
                }
                adjustDetectedComponents(det,img[0].width()*scale, img[0].height()*scale,
                        0, 0, 0, scale);
                detected=det;
            }
            if(!img[1].isNull()){
                QList<DetectedComponent> det;
                dlib::array2d<dlib::rgb_pixel> image;
                qImage2array2d(img[1], image);
                if(comp!=nullptr){
                    qDebug()<<"Running classifier"<<comp->name;
                    det.append(comp->detectObjects(image, displaySearchFrames));
                    qDebug()<<"found"<<det.count();
                } else {
                    foreach(auto classifier, compType->classifiers){
                        qDebug()<<"running classifier"<<classifier->name;
                        det.append(classifier->detectObjects(image, displaySearchFrames));
                        qDebug()<<"found"<<det.count();
                    }
                }
                adjustDetectedComponents(det,img[1].width()*scale, img[1].height()*scale,
                        270, 0, 0, scale);
                detected.append(det);
            }
            if(!img[2].isNull()){
                QList<DetectedComponent> det;
                dlib::array2d<dlib::rgb_pixel> image;
                qImage2array2d(img[2], image);
                if(comp!=nullptr){
                    qDebug()<<"Running classifier"<<comp->name;
                    det.append(comp->detectObjects(image, displaySearchFrames));
                    qDebug()<<"found"<<det.count();
                } else {
                    foreach(auto classifier, compType->classifiers){
                        qDebug()<<"running classifier"<<classifier->name;
                        det.append(classifier->detectObjects(image, displaySearchFrames));
                        qDebug()<<"found"<<det.count();
                    }
                }
                adjustDetectedComponents(det,img[2].width()*scale, img[2].height()*scale,
                        180, 0, 0, scale);
                detected.append(det);
            }
            if(!img[3].isNull()){
                QList<DetectedComponent> det;
                dlib::array2d<dlib::rgb_pixel> image;
                qImage2array2d(img[3], image);
                if(comp!=nullptr){
                    qDebug()<<"Running classifier"<<comp->name;
                    det.append(comp->detectObjects(image, displaySearchFrames));
                    qDebug()<<"found"<<det.count();
                } else {
                    foreach(auto classifier, compType->classifiers){
                        qDebug()<<"running classifier"<<classifier->name;
                        det.append(classifier->detectObjects(image, displaySearchFrames));
                        qDebug()<<"found"<<det.count();
                    }
                }
                adjustDetectedComponents(det,img[3].width()*scale, img[3].height()*scale,
                        90, 0, 0, scale);
                detected.append(det);
            }
        }
    }
    return detected;
}

//run single classifier on single image
//special case is when this is run from the board editor, when all the classifiers are run.
QList<DetectedComponent> BoardDef::detectComponentsImg(const QImage img, CompDef *comp, bool displaySearchFrames, double adjustThreshold)
{
    QList<DetectedComponent> det;
    //qDebug()<<"notnull";
    dlib::array2d<dlib::rgb_pixel> image;
    qImage2array2d(img, image);
    //qDebug()<<"cv_image";
    if(comp!=nullptr){
        qDebug()<<"Running classifier"<<comp->name;
        det=comp->detectObjects(image, displaySearchFrames, img, adjustThreshold);
        qDebug()<<"found"<<det.count();
    } else {
        foreach(auto classifier, compType->classifiers){
            qDebug()<<"running classifier"<<classifier->name;
            det.append(classifier->detectObjects(image, displaySearchFrames, img, adjustThreshold));
            qDebug()<<"found"<<det.count();
        }
    }
    return det;
}

QList<DetectedComponent> BoardDef::testBoard(const QImage & img, DetectedComponent &boardRect, bool displaySearchFrames)
{
    std::vector<cv::Point2f>refPts= rectPointList(refRect);
    std::vector<cv::Point2f>brdPts=rectPointList(boardRect);
    qDebug()<<"boardrect x"<<boardRect.x<<"y"<<boardRect.y<<boardRect.w<<boardRect.h<<boardRect.orientation;
    qDebug()<<"refpts"<<refPts[0].x<<refPts[0].y<<refPts[1].x<<refPts[1].y<<refPts[2].x<<refPts[2].y<<refPts[3].x<<refPts[3].y;
    qDebug()<<"brdpts"<<brdPts[0].x<<brdPts[0].y<<brdPts[1].x<<brdPts[1].y<<brdPts[2].x<<brdPts[2].y<<brdPts[3].x<<brdPts[3].y;
    /* da eroare de dealocare pe windows cu 2.4.2 */
    cv::Mat transform=cv::findHomography(refPts,brdPts);
    cv::Mat bestTransform=transform;
    double bestError=transformError(refPts,brdPts,transform);
    //static cv::Mat transform=cv::findHomography(brdPts,refPts);
    qDebug()<<transform.at<double>(0,0)<<transform.at<double>(0,1)<<transform.at<double>(0,2);
    qDebug()<<transform.at<double>(1,0)<<transform.at<double>(1,1)<<transform.at<double>(1,2);
    qDebug()<<transform.at<double>(2,0)<<transform.at<double>(2,1)<<transform.at<double>(2,2);
    qDebug()<<"rectangle error"<<bestError;
    QList<DetectedComponent> foundComponents;
    if(alignMethod==FIDUCIAL){
        // search the fiducials
        foundComponents = findBoardComponents(img,transform, boardRect,true, displaySearchFrames);
        refPts.clear();
        brdPts.clear();
        for(int i=0; i<foundComponents.count(); i++){
            if((foundComponents[i].status==DetectedComponent::DetectedFiducial)||
                    (foundComponents[i].status==DetectedComponent::Detected)){
                refPts.push_back(cv::Point2f(foundComponents[i].expected.xCenter(),
                                             foundComponents[i].expected.yCenter()));
                brdPts.push_back(cv::Point2f(foundComponents[i].xCenter(),
                                             foundComponents[i].yCenter()));
                qDebug()<<"found fiducial "<<foundComponents[i].expected.xCenter()<<foundComponents[i].expected.yCenter()<<"->"<<foundComponents[i].xCenter()<<foundComponents[i].yCenter();
            }
        }
        if(brdPts.size()>0)
            bestError=transformError(refPts,brdPts,transform);
        qDebug()<<"base error"<<bestError<<"points"<<refPts.size()<<brdPts.size();
        if(refPts.size()>=4){//4 should be enough, but results are sometimes bad
            // enough fiducials have been found to try to find perspective transform
            //transform=cv::findHomography(refPts,brdPts,CV_RANSAC);
            transform=cv::findHomography(refPts,brdPts);
            //qDebug()<<refPts[0].x<<refPts[0].y<<refPts[1].x<<refPts[1].y<<refPts[2].x<<refPts[2].y<<refPts[3].x<<refPts[3].y;
            //qDebug()<<brdPts[0].x<<brdPts[0].y<<brdPts[1].x<<brdPts[1].y<<brdPts[2].x<<brdPts[2].y<<brdPts[3].x<<brdPts[3].y;
            qDebug()<<transform.at<double>(0,0)<<transform.at<double>(0,1)<<transform.at<double>(0,2);
            qDebug()<<transform.at<double>(1,0)<<transform.at<double>(1,1)<<transform.at<double>(1,2);
            qDebug()<<transform.at<double>(2,0)<<transform.at<double>(2,1)<<transform.at<double>(2,2);
            double error=transformError(refPts,brdPts,transform)+0.25*transformError(rectPointList(refRect),rectPointList(boardRect),transform);
            qDebug()<<"error"<<error<<transformError(rectPointList(refRect),rectPointList(boardRect),transform);
            if(error<bestError){
                bestError=error;
                bestTransform=transform;
            }
        }// else {
        //try affine transform even if there are enough points for a perspective transform
        if(refPts.size()>=2){
            //only enough fiducial have been found to try to find a affine transform
            if(refPts.size()>=3){
                qDebug()<<"estimateRigidTransform";
#if ((CV_VERSION_MAJOR == 3) && (CV_VERSION_MINOR >= 2))||(CV_VERSION_MAJOR > 3)
                transform=cv::estimateAffine2D(refPts, brdPts);
#else
                transform=cv::estimateRigidTransform(refPts, brdPts, true);
                if(transform.empty()){
                    transform=cv::estimateRigidTransform(refPts, brdPts, false);
                }
#endif
            } else if(refPts.size()==2){
                qDebug()<<"estimateAffinePartial2D"<<refPts.size()<<brdPts.size();
#if ((CV_VERSION_MAJOR == 3) && (CV_VERSION_MINOR >= 2))||(CV_VERSION_MAJOR > 3)
                transform=cv::estimateAffinePartial2D(refPts, brdPts);
#else
                /// @todo find a working 2 point transform
                //ignore - not working!
                //transform=cv::estimateRigidTransform(refPts, brdPts, false);
#endif
            }
            if(!transform.empty()){
                //make the affine transform matrix the same size as a perspective transform matrix
                transform.resize(3,0);
                transform.at<double>(2,2)=1;
                //            }
                double error=transformError(refPts,brdPts,transform)+0.25*transformError(rectPointList(refRect),rectPointList(boardRect),transform);
                qDebug()<<"affine error"<<error<<transformError(rectPointList(refRect),rectPointList(boardRect),transform);
                if(error<bestError){
                    bestError=error;
                    bestTransform=transform;
                }
            }
        }
        transform=bestTransform;
    } else if((alignMethod==ORB)||(alignMethod==AKAZE)){
        alignBinaryFeatures(img, boardRect, transform);
        qDebug()<<transform.at<double>(0,0)<<transform.at<double>(0,1)<<transform.at<double>(0,2);
        qDebug()<<transform.at<double>(1,0)<<transform.at<double>(1,1)<<transform.at<double>(1,2);
        qDebug()<<transform.at<double>(2,0)<<transform.at<double>(2,1)<<transform.at<double>(2,2);
    }
    //now we have the alignment function to map the expected board components to the image for the current board rectangle
    //test all components from the list of board components for this board
    foundComponents.append(findBoardComponents(img,transform, boardRect,false, displaySearchFrames));
    return foundComponents;
}

void BoardDef::adjustDetectedComponents(QList<DetectedComponent> &dets,
                                        double chipwidth, double chipheight,
                                        int angle, double xoffset, double yoffset, double scale)
{
    assert(chipwidth>0);
    assert(chipheight>0);
    assert(scale>0);
    if(scale!=1){
        for(int i=0; i<dets.count(); i++) {
            DetectedComponent & comp=dets[i];
            comp.x *= scale;
            comp.y *= scale;
            comp.w *= scale;
            comp.h *= scale;
        }
        //chipwidth *= scale;
        //chipheight *= scale;
        //xoffset *= scale;
        //yoffset *= scale;
    }
    double x, y;
    if(angle==0){
        for(int i=0; i<dets.count(); i++) {
            DetectedComponent & comp=dets[i];
            switch(comp.orientation){
            case CompDefRect::N:
            case CompDefRect::S:
            default:
                break;
            case CompDefRect::E:
            case CompDefRect::W:
                std::swap(comp.w, comp.h);
                break;
            }
        }
    } else if(angle==90){
        for(int i=0; i<dets.count(); i++) {
            DetectedComponent & comp=dets[i];
            y=comp.x;
            x=chipheight-comp.y-comp.h;
            comp.y=y;
            comp.x=x;
            switch(comp.orientation){
            case CompDefRect::N:
            default:
                comp.orientation=CompDefRect::E;
                break;
            case CompDefRect::S:
                comp.orientation=CompDefRect::W;
                break;
            case CompDefRect::E:
                comp.orientation=CompDefRect::S;
                std::swap(comp.w, comp.h);
                break;
            case CompDefRect::W:
                comp.orientation=CompDefRect::N;
                std::swap(comp.w, comp.h);
                break;
            }
        }
    } else if(angle==180){
        for(int i=0; i<dets.count(); i++) {
            DetectedComponent & comp=dets[i];
            comp.x=chipwidth-comp.w-comp.x;
            comp.y=chipheight-comp.h-comp.y;
            switch(comp.orientation){
            case CompDefRect::N:
            default:
                comp.orientation=CompDefRect::S;
                break;
            case CompDefRect::S:
                comp.orientation=CompDefRect::N;
                break;
            case CompDefRect::E:
                comp.orientation=CompDefRect::W;
                std::swap(comp.w, comp.h);
                break;
            case CompDefRect::W:
                comp.orientation=CompDefRect::E;
                std::swap(comp.w, comp.h);
                break;
            }
        }
    } else {
        for(int i=0; i<dets.count(); i++) {
            DetectedComponent & comp=dets[i];
            x=comp.y;
            y=chipwidth-comp.x-comp.w;
            comp.y=y;
            comp.x=x;
            switch(comp.orientation){
            case CompDefRect::N:
            default:
                comp.orientation=CompDefRect::W;
                break;
            case CompDefRect::S:
                comp.orientation=CompDefRect::E;
                break;
            case CompDefRect::E:
                comp.orientation=CompDefRect::N;
                std::swap(comp.w, comp.h);
                break;
            case CompDefRect::W:
                comp.orientation=CompDefRect::S;
                std::swap(comp.w, comp.h);
                break;
            }
        }
    }
    for(int i=0; i<dets.count(); i++) {
        DetectedComponent & comp=dets[i];
        comp.x+=xoffset;
        comp.y+=yoffset;
    }
}

int BoardDef::removeOverlaps(QList<DetectedComponent> &components)
{
    const double OVERLAPTHRESH=0.5;
    int deleted=0;
    for(int i=0; i<components.length(); i++){
        DetectedComponent & comp=components[i];
        for(int j=i+1; j<components.length(); j++){
            double overlap=comp.checkOverlap(components[j],true);
            qDebug()<<"removeOverlaps"<<overlap<<comp.score[0]<<components[j].score[0];
            if(overlap>OVERLAPTHRESH){
                //found overlapping detections. Delete the rectangle with the lower score
                deleted++;
                if(comp.score[0]>=components[j].score[0]){
                    //delete rectangle j and continue the inner loop from the same index
                    comp.score[1]=components[j].score[0];
                    components.removeAt(j);
                    j--;
                    continue;
                } else {
                    //delete rectangle i and continue the outer loop from the same index
                    components[j].score[1]=comp.score[0];
                    components.removeAt(i);
                    i--;
                    break;
                }
            }
        }
    }
    return deleted;
}

QList<DetectedComponent>
BoardDef::findBoardComponents(const QImage & img, const cv::Mat &transform,
                              const DetectedComponent &boardRect, bool fiducial, bool displaySearchFrames)
{
    int orientationDiff=CompDefRect::orientationDifference(this->refRect.orientation,boardRect.orientation);
    QList<DetectedComponent> foundComponents;
    QList<QFuture<DetectedComponent>> results;
    foreach(const BoardCompType & cT, compTypes){
        if((cT.componentType==BoardCompType::FIDUCIAL)!=fiducial)
            continue;
        //for regular (not fiducial) components, we will have components and barcodes here
        foreach (const BoardComp & c, cT.components) {
            qDebug()<<"maxThreadCount"<<QThreadPool::globalInstance()->maxThreadCount();
            //extract an image chip from the entire image around the expected position of the component, scale and rotate to have north roation, and run object detection on this chip

            //calculate scale factor for rectangle by measuring the length of two orthogonal
            //unit segments at the center of the object to be detected
            std::vector<cv::Point2f>center;
            cv::Point2f pt(c.xCenter(),c.yCenter());
            center.push_back(pt);
            center.push_back(cv::Point2f(pt.x+1,pt.y));
            center.push_back(cv::Point2f(pt.x,pt.y+1));
            cv::perspectiveTransform(center,center,transform);
            double scale=(norm(center[0]-center[1])+norm(center[0]-center[2]))/2;
            qDebug()<<"findcomp"<<cT.name<<c.x<<c.y<<c.w<<c.h<<c.orientation<<
                      c.xCenter()<<c.yCenter()<<"->"<<center[0].x<<center[0].y<<scale;
            //qDebug()<<center[0].x<<center[0].y<<center[1].x<<center[1].y<<norm(center[0]-center[1])<<norm(center[0]-center[2]);
            DetectedComponent comp;
            double w, h;
            w=c.w*scale;
            h=c.h*scale;
            comp.orientation=c.orientation;
            comp.addOrientationDifference(orientationDiff);
            //qDebug()<<"orientation"<<c.orientation<<comp.orientation<<orientationDiff;
            int rot90=orientationDiff%2;
            if((c.orientation==CompDefRect::E)||
                    (c.orientation==CompDefRect::W)||
                    (c.orientation==CompDefRect::EW)){
                rot90=(rot90+1)%2;
            }
            if(rot90){
                comp.x=center[0].x-h/2;
                comp.y=center[0].y-w/2;
            } else {
                comp.x=center[0].x-w/2;
                comp.y=center[0].y-h/2;
            }
            comp.w=w;
            comp.h=h;
            //comp now contains the location, orientation and size of the expected object
            double chipScale=1;

            if(cT.classifiers.count()>0){
                chipScale=sqrt(cT.minArea()/(w*h));
                for(unsigned upsample=cT.upsample(); upsample>0; upsample--){
                    chipScale *= 2;
                }
                chipScale*=1+0.01*c.sizeTol+0.1;//add 10% to be safe
                //qDebug()<<"chipscale"<<sqrt(cT.minArea()/(w*h))<<c.sizeTol<<chipScale<<comp.w*chipScale<<comp.h*chipScale;
                //qDebug()<<c.w<<c.h<<scale<<w<<h<<cT.minArea();
            }
            //repurpose w, h, rot90 for
            double xoffset, yoffset, wChip, hChip;
            if((comp.orientation==CompDefRect::E)||
                    (comp.orientation==CompDefRect::W)||
                    (comp.orientation==CompDefRect::EW)){
                rot90=1;
                wChip=comp.h;
                hChip=comp.w;
            } else {
                rot90=0;
                wChip=comp.w;
                hChip=comp.h;
            }
            double xTol=c.xTol/100;
            double yTol=c.yTol/100;
            if(rot90){
                xoffset=comp.x-wChip*(yTol+padding);
                yoffset=comp.y-hChip*(xTol+padding);
                wChip *= (1+2*(yTol+padding));
                hChip *= (1+2*(xTol+padding));
            } else {
                xoffset=comp.x-wChip*(xTol+padding);
                yoffset=comp.y-hChip*(yTol+padding);
                wChip *= (1+2*(xTol+padding));
                hChip *= (1+2*(yTol+padding));
            }
            //qDebug()<<"center"<<center[0].x<<center[0].y<<"comp"<<comp.x<<comp.y<<comp.w<<comp.h<<"chip"<<xoffset<<yoffset<<wChip<<hChip;
            //now we have the desired position of the chip in the image, get the image chip
            QImage qChip=img.copy(xoffset, yoffset, wChip, hChip);
            //qDebug()<<"chip"<<xoffset<<yoffset<<wChip<<hChip<<xTol<<yTol;
            int angle=0;
            QImage qChip2;
            if(cT.componentType!=BoardCompType::BARCODE){
                //don't rotate and scale barcodes. Anything else needs to be rotated and scaled
                if((comp.orientation==CompDefRect::E)||
                        (comp.orientation==CompDefRect::EW)){
                    QTransform t2;
                    QTransform t=t2.rotate(270).scale(chipScale,chipScale);
                    qChip2=qChip.transformed(t,Qt::SmoothTransformation);
                    angle=90;
                } else if(comp.orientation==CompDefRect::S){
                    QTransform t2;
                    QTransform t=t2.rotate(180).scale(chipScale,chipScale);
                    qChip2=qChip.transformed(t,Qt::SmoothTransformation);
                    angle=180;
                } else if(comp.orientation==CompDefRect::W){
                    QTransform t2;
                    QTransform t=t2.rotate(90).scale(chipScale,chipScale);
                    qChip2=qChip.transformed(t,Qt::SmoothTransformation);
                    angle=270;
                } else {
                    QTransform t2;
                    QTransform t=t2.scale(chipScale,chipScale);
                    qChip2=qChip.transformed(t,Qt::SmoothTransformation);
                    angle=0;
                }
            }
            //QList<DetectedComponent> dc;
            if((cT.componentType==BoardCompType::COMPONENT)||(cT.componentType==BoardCompType::FIDUCIAL)){
                //run the classifiers on the image chip for normal components
#if 1
//                foundComponents.append(findOneBoardComponent(comp, c, xoffset, yoffset, angle, rot90, wChip, hChip, chipScale, cT, fiducial, qChip2, displaySearchFrames));
                results.append(QtConcurrent::run(std::bind(&BoardDef::findOneBoardComponent, this, comp, c,
                                              xoffset, yoffset, angle, rot90, wChip, hChip, chipScale,
                                              cT, fiducial, qChip2, displaySearchFrames)));

#else
                dlib::array2d<dlib::rgb_pixel> image;
                qImage2array2d(qChip2, image);
                bool found=false;
                foreach(QSharedPointer<CompDef> classifier, cT.classifiers){
                    /// @todo use all classifiers in a single step - abandoned, the classifier object might already run multiple classifiers if more orienations have been trained
                    comp.score[0]=-1;
                    double wErr=0, hErr=0, sizeErr=0;//initialization not needed, but the compiler complains
                    auto dc=classifier->detectObjects(image, displaySearchFrames, qChip2);
                    if((angle==90)||(angle==270))
                        adjustDetectedComponents(dc, hChip, wChip, angle, xoffset, yoffset, 1/chipScale);
                    else
                        adjustDetectedComponents(dc, wChip, hChip, angle, xoffset, yoffset, 1/chipScale);
                    removeOverlaps(dc);
                    // verify component size and position
                    for(int i=0; i<dc.count(); ){
                        if(rot90){
                            hErr=(dc[i].xCenter()-comp.xCenter())/comp.h;
                            wErr=(dc[i].yCenter()-comp.yCenter())/comp.w;
                        } else {
                            wErr=(dc[i].xCenter()-comp.xCenter())/comp.w;
                            hErr=(dc[i].yCenter()-comp.yCenter())/comp.h;
                        }
                        double cSize=sqrt(comp.w*comp.h);
                        sizeErr=(sqrt(dc[i].w*dc[i].h) - cSize)/cSize;
                        /*
                        qDebug()<<"center->center"<<dc[i].xCenter()<<dc[i].yCenter()<<comp.xCenter()<<comp.yCenter();
                        qDebug()<<"error"<<std::abs(dc[i].xCenter()-comp.xCenter())<<
                                  std::abs(dc[i].yCenter()-comp.yCenter())<<
                                  "size"<<dc[i].w<<dc[i].h<<
                                  comp.w<<comp.h<<
                                  "tol"<<xTol<<yTol<<c.sizeTol<<
                                  "deviation"<<wErr<<hErr<<sizeErr;
                        */

                        if((std::abs(wErr)>xTol)||(std::abs(hErr)>yTol)||(std::abs(sizeErr)*100>c.sizeTol)||
                                (comp.orientationDifference(dc[i].orientation,comp.orientation)!=0)){
                            if(comp.orientationDifference(dc[i].orientation,comp.orientation)!=0){
                                comp.score[0] = dc[i].score[0]-20;
                                qDebug()<<"orientation mismatch"<<comp.orientation<<dc[i].orientation;
                            } else {
                                comp.score[0] = dc[i].score[0]-10;
                            }
                            dc.removeAt(i);
                            comp.xerr=wErr;
                            comp.yerr=hErr;
                            comp.scaleerr=sizeErr;
                        } else {
                            //we found an accepted component, possibly with a core below acceptable
                            dc[i].xerr=wErr;
                            dc[i].yerr=hErr;
                            dc[i].scaleerr=sizeErr;
                            i++;
                        }
                    }
                    if(dc.count()>0){
                        //only components with acceptable position remained in dc
                        /// @todo use classifier threshold
                        if(fiducial){
                            if(dc[0].score[0] < -50)
                                dc[0].status=DetectedComponent::ExpectedFiducial;
                            else
                                dc[0].status=DetectedComponent::DetectedFiducial;
                        } else{
                            if(dc[0].score[0] < -50)
                                dc[0].status=DetectedComponent::Expected;
                            else
                                dc[0].status=DetectedComponent::Detected;
                        }
                        dc[0].expected=c;
                        dc[0].boardComp=c;
                        //dc[0].xerr=wErr;
                        //dc[0].yerr=hErr;
                        //dc[0].scaleerr=sizeErr;
                        //qDebug()<<c.xCenter()<<c.yCenter();
                        foundComponents.append(dc[0]);
                        found=true;
                        //we found a component and appended it to the foundComponents list, so we don't have to look further
                        break;
                    }

                }
                if(!found){
                    comp.boardComp=c;
                    if(fiducial)
                        comp.status=DetectedComponent::ExpectedFiducial;
                    foundComponents.append(comp);
                }
#endif
            } else if(cT.componentType==BoardCompType::BARCODE){
#ifdef BARCODEREAD
                // run the barcode detector on the image chip
                int errorCode=0;
                QString barCode=Barcode::decodeBC(qChip,errorCode, static_cast<Barcode::BCType>(c.symbology));
                if(!barCode.isEmpty()){
                    comp.status=DetectedComponent::DetectedBarcode;
                } else {
                    comp.status=DetectedComponent::ExpectedBarcode;
                }
                //store the decoded barcode in a fake detectedComponent in a fake boardComp as name, as the only part of the current detectedComponent
                DetectedComponent tmp;
                tmp.boardComp.name=barCode;
                comp.parts.append(tmp);
#endif
                comp.boardComp=c;
                foundComponents.append(comp);
            }
#if 0
            //display results
            dlib::image_window win;
            win.clear_overlay();
            win.set_image(image);
            /*
            win.add_overlay(objectLocations[i], dlib::rgb_pixel(255,0,0));
            win.add_overlay(ignoredRects[i], dlib::rgb_pixel(0,255,0));
            for(int i=0; i<dets.size(); i++){
                win.add_overlay(dets[i].second, dlib::rgb_pixel(255,0,0));
            }
            for(int j=0; j<ignoredRects[i].size(); j++){
                win.add_overlay(ignoredRects[i][j], dlib::rgb_pixel(0,255,0));
                qDebug()<<"ign"<<ignoredRects[i][j].rect.left()<<ignoredRects[i][j].rect.top()<<ignoredRects[i][j].rect.width()<<ignoredRects[i][j].rect.height();
            }
            */
            win.wait_until_closed();

#endif

        }
    }
    foreach(auto result, results){
        if(!result.isFinished())
            result.waitForFinished();
        foundComponents.append(result.result());
    }
    return foundComponents;
}

DetectedComponent BoardDef::findOneBoardComponent(DetectedComponent comp,
                                         const BoardComp & c, double xoffset, double yoffset, int angle, int rot90,
                                        double wChip, double hChip, double chipScale, const BoardCompType &cT,
                                        bool fiducial, const QImage qChip2, bool displaySearchFrames)
{
    dlib::array2d<dlib::rgb_pixel> image;
    qImage2array2d(qChip2, image);
    bool found=false;
    foreach(QSharedPointer<CompDef> classifier, cT.classifiers){
        /// @todo use all classifiers in a single step - abandoned, the classifier object might already run multiple classifiers if more orienations have been trained
        comp.score[0]=-1;
        double wErr=0, hErr=0, sizeErr=0;//initialization not needed, but the compiler complains
        auto dc=classifier->detectObjects(image, displaySearchFrames, qChip2);
        if((angle==90)||(angle==270))
            adjustDetectedComponents(dc, hChip, wChip, angle, xoffset, yoffset, 1/chipScale);
        else
            adjustDetectedComponents(dc, wChip, hChip, angle, xoffset, yoffset, 1/chipScale);
        removeOverlaps(dc);
        // verify component size and position
        for(int i=0; i<dc.count(); ){
            if(rot90){
                hErr=(dc[i].xCenter()-comp.xCenter())/comp.h;
                wErr=(dc[i].yCenter()-comp.yCenter())/comp.w;
            } else {
                wErr=(dc[i].xCenter()-comp.xCenter())/comp.w;
                hErr=(dc[i].yCenter()-comp.yCenter())/comp.h;
            }
            double cSize=sqrt(comp.w*comp.h);
            sizeErr=(sqrt(dc[i].w*dc[i].h) - cSize)/cSize;
            /*
            qDebug()<<"center->center"<<dc[i].xCenter()<<dc[i].yCenter()<<comp.xCenter()<<comp.yCenter();
            qDebug()<<"error"<<std::abs(dc[i].xCenter()-comp.xCenter())<<
                      std::abs(dc[i].yCenter()-comp.yCenter())<<
                      "size"<<dc[i].w<<dc[i].h<<
                      comp.w<<comp.h<<
                      "tol"<<xTol<<yTol<<c.sizeTol<<
                      "deviation"<<wErr<<hErr<<sizeErr;
            */

            if((std::abs(wErr)>c.xTol/100)||(std::abs(hErr)>c.yTol/100)||(std::abs(sizeErr)*100>c.sizeTol)||
                    (comp.orientationDifference(dc[i].orientation,comp.orientation)!=0)){
                if(comp.orientationDifference(dc[i].orientation,comp.orientation)!=0){
                    comp.score[0] = dc[i].score[0]-20;
                    qDebug()<<"orientation mismatch"<<comp.orientation<<dc[i].orientation;
                } else {
                    comp.score[0] = dc[i].score[0]-10;
                }
                dc.removeAt(i);
                comp.xerr=wErr;
                comp.yerr=hErr;
                comp.scaleerr=sizeErr;
            } else {
                //we found an accepted component, possibly with a core below acceptable
                dc[i].xerr=wErr;
                dc[i].yerr=hErr;
                dc[i].scaleerr=sizeErr;
                i++;
            }
        }
        if(dc.count()>0){
            //only components with acceptable position remained in dc
            /// @todo use classifier threshold
            if(fiducial){
                if(dc[0].score[0] < -50)
                    dc[0].status=DetectedComponent::ExpectedFiducial;
                else
                    dc[0].status=DetectedComponent::DetectedFiducial;
            } else{
                if(dc[0].score[0] < -50)
                    dc[0].status=DetectedComponent::Expected;
                else
                    dc[0].status=DetectedComponent::Detected;
            }
            dc[0].expected=c;
            dc[0].boardComp=c;
            //dc[0].xerr=wErr;
            //dc[0].yerr=hErr;
            //dc[0].scaleerr=sizeErr;
            //qDebug()<<c.xCenter()<<c.yCenter();
            return dc[0];
            found=true;
            //we found a component and appended it to the foundComponents list, so we don't have to look further
            break;
        }

    }
    if(!found){
        comp.boardComp=c;
        if(fiducial)
            comp.status=DetectedComponent::ExpectedFiducial;
        return comp;
    }

}

void BoardDef::qImage2array2d(const QImage &src, dlib::array2d<dlib::rgb_pixel> &dst)
{
    //qDebug()<<"qImage2array2d"<<src.width()<<src.width()*3<<src.bytesPerLine()<<src.format();
    if((src.format()==QImage::Format_ARGB32)||
            (src.format()==QImage::Format_ARGB32_Premultiplied)||
            (src.format()==QImage::Format_RGB32)||
            (src.format()==QImage::Format_Grayscale8)||
            (src.format()==QImage::Format_Indexed8)){
        qImage2array2d(src.convertToFormat(QImage::Format_RGB888),dst);
        return;
    }
    if(src.format()!=QImage::Format_RGB888){
        qDebug()<<src.format()<<src.width()<<src.height();
    }
    assert(src.format()==QImage::Format_RGB888);
    if(src.bytesPerLine()!=src.width()*3){
        //qDebug()<<"memcpy";
        int w=src.width();
        int h=src.height();
        //qDebug()<<w<<h;
        /* stack allocation is not working */
        char * buf=new char[src.width()*src.height()*3];
        //qDebug()<<"startfor";
        for(int r=0; r<h; r++){
            int doffset = r*w*3;
            int soffset = r*src.bytesPerLine();
            //qDebug()<<"offset"<<r<<doffset << soffset<<w*h*3;
            memcpy(buf+doffset,src.constBits()+soffset,w*3);
        }
        //dst.set_size(src.height(),src.width());
        dlib::assign_image(dst,
                           dlib::mat((const dlib::rgb_pixel *)buf,
                                     src.height(), src.width()));

        delete[] buf;
    } else {
        //qDebug()<<"qImage2array2d simple";
        dlib::assign_image(dst,
                           dlib::mat((const dlib::rgb_pixel *)src.bits(),
                                     src.height(), src.width()));
    }
}

std::vector<cv::Point2f> BoardDef::rectPointList(const CompDefRect &rect)
{
    std::vector<cv::Point2f> result;
    switch(rect.orientation){
    case CompDefRect::N:
    case CompDefRect::NS:
    default:
        result.push_back(cv::Point2f(rect.x, rect.y));
        result.push_back(cv::Point2f(rect.x+rect.w, rect.y));
        result.push_back(cv::Point2f(rect.x+rect.w, rect.y+rect.h));
        result.push_back(cv::Point2f(rect.x, rect.y+rect.h));
        break;
    case CompDefRect::E:
    case CompDefRect::EW:
        result.push_back(cv::Point2f(rect.x+rect.h, rect.y));
        result.push_back(cv::Point2f(rect.x+rect.h, rect.y+rect.w));
        result.push_back(cv::Point2f(rect.x, rect.y+rect.w));
        result.push_back(cv::Point2f(rect.x, rect.y));
        break;
    case CompDefRect::S:
        result.push_back(cv::Point2f(rect.x+rect.w, rect.y+rect.h));
        result.push_back(cv::Point2f(rect.x, rect.y+rect.h));
        result.push_back(cv::Point2f(rect.x, rect.y));
        result.push_back(cv::Point2f(rect.x+rect.w, rect.y));
        break;
    case CompDefRect::W:
        result.push_back(cv::Point2f(rect.x, rect.y+rect.w));
        result.push_back(cv::Point2f(rect.x, rect.y));
        result.push_back(cv::Point2f(rect.x+rect.h, rect.y));
        result.push_back(cv::Point2f(rect.x+rect.h, rect.y+rect.w));
        break;
    }
    return result;
}

bool BoardDef::alignBinaryFeatures(const QImage img, const DetectedComponent &boardRect, cv::Mat &transform)
{
    cv::Mat image=qImage2Mat(img,true);
    Features features;
    binaryDescriptors(features, image, boardRect);
    qDebug()<<"ORB Descriptors"<<features.keypoints.size()<<refFeatures.keypoints.size()<<
              features.descriptors.rows<<features.descriptors.cols<<
              refFeatures.descriptors.rows<<refFeatures.descriptors.cols;
    if(features.descriptors.empty())
        return false;
    if(refFeatures.descriptors.empty())
        return false;
    // https://docs.opencv.org/3.0-beta/doc/tutorials/features2d/akaze_matching/akaze_matching.html
    const float nn_match_ratio = 0.8f;   // Nearest neighbor matching ratio
    cv::BFMatcher matcher(cv::NORM_HAMMING,false);
    std::vector< std::vector<cv::DMatch> > nn_matches;
    //std::vector<cv::DMatch> nn_matches;
    matcher.knnMatch(refFeatures.descriptors, features.descriptors, nn_matches, 2);
    //matcher.match(refFeatures.descriptors, features.descriptors, nn_matches);
    std::vector<cv::Point2f> matched1, matched2;
    for(size_t i = 0; i < nn_matches.size(); i++) {
        cv::DMatch first = nn_matches[i][0];

        float dist1 = nn_matches[i][0].distance;
        float dist2 = nn_matches[i][1].distance;

        if(dist1 < nn_match_ratio * dist2) {
            matched1.push_back(refFeatures.keypoints[first.queryIdx].pt);
            matched2.push_back(features.keypoints[first.trainIdx].pt);
            qDebug()<<matched1.back().x<<matched1.back().y<<matched2.back().x<<matched2.back().y;
            //matched1.push_back(refFeatures.keypoints[first.queryIdx].pt);
            //matched2.push_back(features.keypoints[first.trainIdx].pt);
        }
    }
    if(matched1.size()<4)
        return false;
    cv::Mat tr;
    if(0)
        tr = cv::findHomography( matched1, matched2, CV_RANSAC );
    else {
#if ((CV_VERSION_MAJOR == 3) && (CV_VERSION_MINOR>2)) || (CV_VERSION_MAJOR > 3)
        tr=cv::estimateAffine2D(matched1, matched2);
#else
        transform=cv::estimateRigidTransform(matched1, matched2, true);
#endif
        if(tr.empty())
            return false;
        //make the affine transform matrix the same size as a perspective transform matrix
        tr.resize(3,0);
        tr.at<double>(2,2)=1;

    }
    if(tr.empty())
        return false;
    transform=tr;
    qDebug()<<"matched"<<matched1.size();

    return true;
}

double BoardDef::transformError(std::vector<cv::Point2f> srcPts, std::vector<cv::Point2f> dstPts, cv::Mat transform)
{
    if(srcPts.size()<=0){
        qDebug()<<"request to calculate error on zero points";
        return 1000000.0;
    }
    double error=0;
    assert(srcPts.size()==dstPts.size());
    assert(transform.cols==3);
    assert(transform.rows==3);
    auto calcPts=srcPts;
    cv::perspectiveTransform(srcPts, calcPts, transform);
    for(unsigned int i=0; i<srcPts.size(); i++){
        error+=cv::norm(calcPts[i]-dstPts[i]);
    }
    return error;
}

int BoardDef::mergeResults(QList<DetectedComponent> &results, const QList<DetectedComponent> &oldResults)
{
    if(oldResults.isEmpty())
        return -1;
    if(results.isEmpty()){
        ///@todo
        return -1;
    }
    ///@todo
    if(results.count()!=oldResults.count())
        return -1;
    auto old=oldResults[0].parts;
    auto &res=results[0].parts;
    for(int i=0; i<res.count(); i++){
        int id=res[i].boardComp.id;
        for(int j=0; j<old.count(); j++){
            if(old[j].boardComp.id==id){
                //found
                if(res[i].status!=old[j].status){
                    if(old[j].decision!=-1){
                        //change decision
                        res[i].decision=1-old[j].decision;
                    }
                } else {
                    //keep decision
                    res[i].decision=old[j].decision;
                }
            }
        }
    }
    int decision=calculateDecision(results);
    return decision;
}

void BoardDef::binaryDescriptors(Features &features, cv::Mat image, const CompDefRect & roi)
{
    features.keypoints.clear();
    cv::Mat mask=cv::Mat::zeros(image.rows, image.cols, CV_8UC1);
    cv::rectangle(mask, cv::Rect(roi.x, roi.y, roi.w, roi.h), cv::Scalar(255), CV_FILLED);
    if(alignMethod==ORB){
        cv::Ptr<cv::ORB> orb = cv::ORB::create();
        //orb->detectAndCompute(image,,features.keypoints,features.descriptors);
        orb->detect(image,features.keypoints);
        orb->compute(image,features.keypoints, features.descriptors);
    } else if(alignMethod==AKAZE){
        cv::Ptr<cv::AKAZE> akaze = cv::AKAZE::create();
        //akaze->detectAndCompute(image,,features.keypoints,features.descriptors);
        std::vector<cv::KeyPoint> keypoints;
        akaze->detect(image,keypoints);
        const int target=500;
        if(keypoints.size()>target){
            float lo=std::numeric_limits<float>::max(), hi=std::numeric_limits<float>::min();
            for(unsigned i=0; i<keypoints.size(); i++){
                if(keypoints[i].response>hi)
                    hi=keypoints[i].response;
                if(keypoints[i].response<lo)
                    lo=keypoints[i].response;
            }
            float lim=lo;
            int numGreater=keypoints.size();
            float loNumGreater=numGreater;
            float hiNumGreater=0;
            const float targetTol=0.01;
            for(int n=0; n<10; n++){
                if((numGreater>=target*(1-targetTol))&&(numGreater<=target*(1+targetTol)))
                    break;
                if(numGreater>target){
                    loNumGreater=numGreater;
                    lo=lim;
                    lim=lo+(hi-lo)*(0.25+0.5*(loNumGreater-target)/(loNumGreater-hiNumGreater));
                } else {
                    hiNumGreater=numGreater;
                    hi=lim;
                    lim=lo+(hi-lo)*(0.25+0.5*(loNumGreater-target)/(loNumGreater-hiNumGreater));
                }
                qDebug()<<lim<<hi<<lo<<numGreater<<loNumGreater<<target<<hiNumGreater;
                numGreater=0;
                for(unsigned i=0; i<keypoints.size(); i++){
                    if(keypoints[i].response>=lim)
                        numGreater++;
                }
            }
            for(unsigned i=0; i<keypoints.size(); i++){
                if(keypoints[i].response>=lim)
                    features.keypoints.push_back(keypoints[i]);
            }
        } else {
            features.keypoints=keypoints;
        }
        akaze->compute(image,features.keypoints, features.descriptors);
    }
}

void BoardDef::saveBinaryFeatures()
{
    cv::FileStorage fs(".yml", cv::FileStorage::WRITE + cv::FileStorage::MEMORY);
    fs << "kp" << refFeatures.keypoints << "descriptors" << refFeatures.descriptors;
    std::string buf = fs.releaseAndGetString();
    //qDebug()<<QString::fromStdString( buf);
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql="DELETE FROM boarddetails WHERE board="+ QString::number(id)+
            " AND type="+QString::number(alignMethod);
    if(!query.exec(sql)){
        qDebug()<<"error while deleting ORB"<<query.lastError()<<query.lastQuery();
    }
    sql="INSERT INTO boarddetails (board, type, data) VALUES("+QString::number(id)+
            ", "+QString::number(alignMethod)+", :kp)";
    query.prepare(sql);
    query.bindValue(":kp", QByteArray(buf.c_str(),buf.length()));
    if(!query.exec()){
        qDebug()<<"error while saving ORB"<<query.lastError()<<query.lastQuery();
    }

}

void BoardDef::loadBinaryFeatures()
{
    QSqlDatabase defaultdb=QSqlDatabase::database();
    if(!defaultdb.isOpen())
        defaultdb.open();
    QSqlQuery query;
    QString sql="SELECT data FROM boarddetails WHERE board="+QString::number(id)+
            " AND type="+QString::number(alignMethod);
    query.exec(sql);
    if(query.next()){
        QByteArray tmp=query.value(0).toByteArray();
        std::string buf(tmp.constData(), tmp.length());
        cv::FileStorage fs(buf, cv::FileStorage::READ + cv::FileStorage::MEMORY);
        qDebug()<<"buffer"<<tmp.length()<<buf.size()<<fs.isOpened();
        fs["kp"] >> refFeatures.keypoints;
        fs["descriptors"] >> refFeatures.descriptors;
    } else {
        refFeatures.keypoints.clear();
        refFeatures.descriptors=cv::Mat();
    }
    qDebug()<<"loaded"<<refFeatures.keypoints.size()<<sql;
}

cv::Mat BoardDef::qImage2Mat(const QImage &src, bool switchRedBlue)
{
#if 1
    if(src.format()!=QImage::Format_RGB888){
        return qImage2Mat(src.convertToFormat(QImage::Format_RGB888),switchRedBlue);
    }
    qDebug()<<"qImage2Mat"<<src.height() << src.width()<< src.bytesPerLine() << src.byteCount() << src.bytesPerLine()*src.height() << src.format();
    cv::Mat tmp=cv::Mat(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine()).clone();
    qDebug()<<"tmp created"<<tmp.rows<<tmp.cols<<tmp.elemSize();
    qDebug()<<"tmp ok?";
    if(switchRedBlue)
        cvtColor(tmp, tmp,CV_BGR2RGB);
    else {
        /*nothing*/;
    }
    return tmp;
#else
    qDebug()<<"qImage2Mat"<<src.height() << src.width()<< src.bytesPerLine() << src.byteCount() << src.bytesPerLine()*src.height() << src.format();
    //cv::Mat tmp=cv::Mat(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine()).clone();
    dlib::array2d<dlib::rgb_pixel> arr;
    dlib::assign_image(arr, dlib::mat((const dlib::rgb_pixel *)src.bits(), src.width(), src.height()));
    usleep(5000000);
    cv::Mat tmp(1,1,CV_8UC3);
    usleep(5000000);
    qDebug()<<"tmp created"<<tmp.rows<<tmp.cols<<tmp.elemSize();

    return tmp;
    tmp=cv::Mat(src.height(),src.width(),CV_8UC3);
    memcpy(tmp.data, src.constBits(), src.byteCount());
    qDebug()<<"tmp ok?";
    if(switchRedBlue)
        cvtColor(tmp, tmp,CV_BGR2RGB);
    else {
        /*nothing*/;
    }
    return tmp;
#endif
}

bool BoardDef::hasBarcodeSN()
{
    foreach(const BoardCompType & cT, compTypes){
        if(BoardCompType::BARCODE!=cT.componentType)
            continue;
        foreach(const auto c, cT.components){
            if(c.name.compare("barcode",Qt::CaseInsensitive)==0){
                //found a definition of the barcode for the serial number
                return true;
            }
        }
    }
    return false;
}

QList<DetectedComponent> BoardDef::runOnImage(QImage image, bool displaySearchFrames,
                                              bool &testOk, QString resultsFile, const QList<DetectedComponent> &oldResults,
                                              int *finalDecision)
{
    const double safetyFactor=1.05;
    double scale;
    scale=safetyFactor * sqrt(static_cast<double>(this->refClassifier.minArea)/this->minArea);
    QVector<QImage> img(4);
    img[0]=image;
    {
        QTransform t2;
        QTransform t=t2.scale(scale, scale);
        img[0]=image.transformed(t);
    }
    {
        QTransform t2;
        QTransform t=t2.rotate(90);
        img[1]=img[0].transformed(t);
    }
    {
        QTransform t2;
        QTransform t=t2.rotate(180);
        img[2]=img[0].transformed(t);
    }
    {
        QTransform t2;
        QTransform t=t2.rotate(270);
        img[3]=img[0].transformed(t);
    }
    QList<DetectedComponent> boardRects = this->detectComponents(img,&(this->refClassifier),1/scale, displaySearchFrames, 0);
    removeOverlaps( boardRects );
    testOk=(boardRects.count()>0);
    for(int i=0; i<boardRects.count(); i++){
        //get a convenince alias to the list of components
        QList<DetectedComponent> & foundComponents = boardRects[i].parts;
        foundComponents = testBoard(image,boardRects[i], displaySearchFrames);
        //components have been found, now put them onto the display
        bool boardOk=true;
        for(int j=0; j<foundComponents.count(); j++){
            //get a convenience alias to the current component
            DetectedComponent & comp=foundComponents[j];
            qDebug()<<comp.x<<comp.y<<comp.w<<comp.h<<comp.orientation<<
                      comp.boardComp.name<<comp.status;
            if((comp.status==DetectedComponent::Expected)||(comp.status==DetectedComponent::ExpectedBarcode))
                boardOk=false;
        }
        if(boardOk)
            boardRects[i].status=DetectedComponent::Detected;
        else {
            boardRects[i].status=DetectedComponent::Expected;
            testOk=false;
        }
    }
    if(!resultsFile.isEmpty()){
        std::ofstream out((resultsFile).toStdString(), std::ofstream::out|std::ofstream::binary);
        if(out.is_open()){
            int decision;
            if(boardRects.isEmpty() && oldResults.isEmpty()){
                //nothing changed, keep old decision
                decision= *finalDecision;
            } else {
                decision=mergeResults(boardRects, oldResults);
            }
            serialize(boardRects, out);
            dlib::serialize(decision, out);
            out.close();
            if(finalDecision!=nullptr)
                *finalDecision=decision;
        }
    }
    return boardRects;
}

const QList<QSharedPointer <CompDef>> BoardDef::classifierFromComponent(long long component)
{
    bool found=false;
    for(int i=0; i<compTypes.count(); i++){
        const auto & cT=compTypes[i];
        for(int j=0; j<cT.components.count(); j++){
            if(cT.components[j].id==component){
                found=true;
                break;
            }
        }
        if(found){
            return cT.classifiers.values();
        }
    }
    return QList<QSharedPointer <CompDef>>();
}

bool Features::isEmpty()
{
    return(keypoints.size()<=0);
}

int BoardDef::calculateDecision(const QList<DetectedComponent> &boards)
{
    int decision=-1;
    int wrong=0, unknown=0;
    if(boards.count()<=0){
        decision=-1;
        return decision;
    }
    for(int i=0; i<boards[0].parts.count(); i++){
        switch(boards[0].parts[i].decision){
        case 0:
            wrong++;
            break;
        case -1:
            unknown++;
            break;
        default:
            //ignore
            ;
        }
    }
    if(wrong>0){
        decision=0;
    } else if(unknown>0){
        decision=-1;
    } else {
        decision=1;
    }
    return decision;
}
