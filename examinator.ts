<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="ro_RO">
<context>
    <name>BoardCompAltDialog</name>
    <message>
        <location filename="boardcompaltdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="boardcompaltdialog.ui" line="44"/>
        <source>Clasificator componenta</source>
        <translation>Component classifier</translation>
    </message>
</context>
<context>
    <name>BoardCompTypeConfig</name>
    <message>
        <location filename="boardcomptypeconfig.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="20"/>
        <source>Denumire tip componenta</source>
        <translation>Component type name</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="30"/>
        <source>Clasa componenta</source>
        <translation>Component class</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="44"/>
        <source>Componenta</source>
        <translation>Component</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="49"/>
        <source>Fiducial</source>
        <translation>Fiducial</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="72"/>
        <source>Salveaza</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="82"/>
        <source>Renunta</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="boardcomptypeconfig.ui" line="85"/>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BoardConfig</name>
    <message>
        <location filename="boardconfig.ui" line="14"/>
        <source>Definire caracteristici placa</source>
        <translation>Define board specifics</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="23"/>
        <source>Denumire placa</source>
        <translation>Board name</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="33"/>
        <source>Clasificator</source>
        <translation>Classifier</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="43"/>
        <source>Aliniere</source>
        <translation>Alignment</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="69"/>
        <source>Salveaza</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="boardconfig.ui" line="76"/>
        <source>Renunta</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>BoardSelect</name>
    <message>
        <location filename="boardselect.ui" line="14"/>
        <source>Configurare </source>
        <translation>Configure </translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="28"/>
        <source>Alege &amp;placa</source>
        <translation>Select &amp;board</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="43"/>
        <source>Afișeaza ferestrele de căutare</source>
        <translation>Display search windows</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="50"/>
        <source>fiducial</source>
        <translation>fiducal</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="53"/>
        <source>Afișează puncte reper aliniere</source>
        <translation>Show fiducials</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="68"/>
        <source>Sal&amp;vare automată imagini</source>
        <translation>Autimaticaly sa&amp;ve images</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="79"/>
        <source>Nu</source>
        <translation>none</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="84"/>
        <source>Doar cele fail</source>
        <translation>failed images</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="89"/>
        <source>Toate</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="101"/>
        <source>&amp;în directorul</source>
        <translation>&amp;into folder</translation>
    </message>
    <message>
        <location filename="boardselect.ui" line="119"/>
        <source>Setari</source>
        <translation>Settings</translation>
    </message>
</context>
<context>
    <name>CameraConfig</name>
    <message>
        <location filename="cameraconfig.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="20"/>
        <source>&amp;Tip Camera</source>
        <translation>Camera &amp;Type</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="49"/>
        <source>Imagine</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="72"/>
        <location filename="cameraconfig.ui" line="167"/>
        <source>Ca&amp;mera</source>
        <translation>Ca&amp;mera</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="82"/>
        <location filename="cameraconfig.ui" line="180"/>
        <source>Re&amp;zoluție</source>
        <translation>Re&amp;solution</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="105"/>
        <location filename="cameraconfig.ui" line="193"/>
        <source>&amp;Nr. Imagini</source>
        <translation>Images &amp;qty.</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="121"/>
        <source>Adresa</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="128"/>
        <source>http://192.168.43.1:8080/photo.jpg</source>
        <translation>http://192.168.43.1:8080/photo.jpg</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aceasta nu e o cameră reală.&lt;/p&gt;&lt;p&gt;E folosită pentru încercări, imaginile fiind incărcate de pe disc.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is not a real camera.&lt;/p&gt;&lt;p&gt;It&apos;s used for trials, the images are loaded from disk.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="220"/>
        <source>Nr. img. &amp;ignorate</source>
        <translation>&amp;Ignored img. qty.</translation>
    </message>
    <message>
        <location filename="cameraconfig.ui" line="234"/>
        <source>Încearcă</source>
        <translation>Try</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="51"/>
        <source>camera web</source>
        <translation>Webcam</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="52"/>
        <source>IP Webcam</source>
        <translation>Webcam IP</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="53"/>
        <source>imagini disc</source>
        <translation>Disk images</translation>
    </message>
    <message>
        <location filename="cameraconfig.cpp" line="55"/>
        <source>camera OPENCV</source>
        <translation>OPENCV camera</translation>
    </message>
</context>
<context>
    <name>CompDef</name>
    <message>
        <location filename="compdef.cpp" line="1647"/>
        <location filename="compdef.cpp" line="1680"/>
        <location filename="compdef.cpp" line="1785"/>
        <source>eroare</source>
        <translation>error</translation>
    </message>
    <message>
        <location filename="compdef.cpp" line="1647"/>
        <location filename="compdef.cpp" line="1680"/>
        <location filename="compdef.cpp" line="1785"/>
        <source>nu se poate deschide baza de date </source>
        <translation>can&apos;t open the database </translation>
    </message>
</context>
<context>
    <name>CompDefDialog</name>
    <message>
        <location filename="compdefdialog.ui" line="14"/>
        <source>Definire parametri componenta</source>
        <translation>Define component parameters</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1696"/>
        <source>Oglindire orizontala exemple pozitive</source>
        <translation>positive samples horizontal mirroring</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="55"/>
        <source>Oglindire verticala exemple pozitive</source>
        <translation>positive samples vertical mirroring</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="75"/>
        <source>Suprafata &amp;minima</source>
        <oldsource>Suprafata minima</oldsource>
        <translation>&amp;minimal area</translation>
    </message>
    <message>
        <source>Dublare rezolutie/interpolare</source>
        <translation type="vanished">resolution doubling/interpolation</translation>
    </message>
    <message>
        <source>Denumire Componenta</source>
        <translation type="vanished">componant name</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="96"/>
        <source>FHOG</source>
        <translation>FHOG</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="99"/>
        <source>Histogram of oriented gradients</source>
        <translation>Histogram of oriented gradients</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="117"/>
        <source>Invatare clasificator</source>
        <translation>Train classifier</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="124"/>
        <source>Prag acceptare</source>
        <translation>acceptance threshold</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="163"/>
        <source>Parametru &amp;C pentru SVM</source>
        <translation>SVM &amp;C parameter</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="173"/>
        <source>valori mai mici fac clasificatorul mai permisiv</source>
        <translation>lower values make the classifier more permissive</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="192"/>
        <source>Parametru &amp;eps pentru SVM</source>
        <translation>SVM &amp;eps parameter</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="221"/>
        <source>Numar fire e&amp;xecutie SVM</source>
        <translation>&amp;number of training threads</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="258"/>
        <source>DNN</source>
        <translation>DNN</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="261"/>
        <source>Deep Neural Network</source>
        <translation>Deep Neural Network</translation>
    </message>
    <message>
        <source>pasi fără progres</source>
        <translation type="vanished">steps without progress</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="414"/>
        <location filename="compdefdialog.ui" line="484"/>
        <source>număr mostre de imagine pentru un pas de învățare</source>
        <translation>minibatch size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="343"/>
        <source>rată învățare</source>
        <translation>learning rate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="462"/>
        <location filename="compdefdialog.ui" line="576"/>
        <source>număr pași de învățare după care se modifică numărul de mostre pentru un pas</source>
        <oldsource>număr pași de învățare pentru care se după care se modifică numărul de mostre pentru un pas</oldsource>
        <translation type="unfinished">number of steps after which the minibatch size is mofified</translation>
    </message>
    <message>
        <source>dim. lot</source>
        <translation type="vanished">minibatch size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="388"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;număr mostre de imagine pentru un pas de învățare&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>number of steps after which the minibatch size is mofified</translation>
    </message>
    <message>
        <source>dim. mostra</source>
        <translation type="vanished">image chip size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="660"/>
        <source>de la zero</source>
        <translation>from scratch</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="665"/>
        <source>reînvâțare</source>
        <translation>relearn</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="670"/>
        <source>folosind</source>
        <translation>using</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="595"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;dimensiune orizontală a mostrelor de imagine folosite la învățare&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>image chip width</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="319"/>
        <location filename="compdefdialog.ui" line="1606"/>
        <source>învățare</source>
        <translation>train</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="282"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;dimensiune verticală a mostrelor de imagine folosite la învățare&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>image chip height</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="614"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;factor de multiplicare pentru reducerea ratei de învățare după efectuarea numărului de pași făra progres&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>learning rate shrink factor</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="452"/>
        <source>rotație</source>
        <translation>rotation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="378"/>
        <source>modificare culori</source>
        <translation>disturb colors</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="353"/>
        <source>număr de pași fără progres după care se reduce rata de învățare</source>
        <translation>training steps without progress after which the learning rate is shrunken</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="232"/>
        <location filename="compdefdialog.ui" line="711"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="237"/>
        <location filename="compdefdialog.ui" line="716"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="242"/>
        <location filename="compdefdialog.ui" line="721"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="250"/>
        <location filename="compdefdialog.ui" line="703"/>
        <source>Număr orientări</source>
        <translation>Number of orientations</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="333"/>
        <source>dimensiune mostră</source>
        <translation>sample size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="372"/>
        <source>alterare aleatorie culoare mostre pentru crestere varietate</source>
        <translation>randomly alter sample colors to increase variety</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="404"/>
        <source>dimensiune lot</source>
        <translation>batch size</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="430"/>
        <source>rată de învățare inițială</source>
        <translation>initial learning rate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="503"/>
        <source>rată de învățare minimă</source>
        <translation>minumal learning rate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="543"/>
        <source>salvare set date</source>
        <translation>save dataset</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="550"/>
        <source>rotire aleatorie mostre pentru crestere varietate</source>
        <translation>max random rotation angle to increase variety</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="566"/>
        <source>pași fără progres</source>
        <translation>steps without progress</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="633"/>
        <source>dezechilibru</source>
        <translation>bias</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="643"/>
        <source>Favorizare exemple pozitive. Uneori e nevoie de aceasta pentru a face posibilă învățarea</source>
        <translation>Favor positive samples. Sometimes this is necessary in order to obtain convergence</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="675"/>
        <source>continuare</source>
        <translation>continue</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="683"/>
        <source>mod învățare</source>
        <translation>learning mode</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="693"/>
        <source>Poziționare mai exactă, îngreunează învățarea</source>
        <translation>Learn bounding box regression - makes learning harder</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="730"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="736"/>
        <source>Import Autoinspect</source>
        <translation>Import Autoinspect</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="756"/>
        <source>import retea neuronala</source>
        <translation>Import neural network</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="763"/>
        <source>export retea neuronala</source>
        <translation>Export neural network</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="771"/>
        <source>culoare</source>
        <translation>color</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="795"/>
        <location filename="compdefdialog.ui" line="824"/>
        <location filename="compdefdialog.ui" line="865"/>
        <location filename="compdefdialog.ui" line="897"/>
        <location filename="compdefdialog.ui" line="1065"/>
        <location filename="compdefdialog.ui" line="1211"/>
        <source>toleranță</source>
        <translation>tolerance</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="808"/>
        <location filename="compdefdialog.ui" line="1138"/>
        <source>Nuanță</source>
        <translation>Hue</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="884"/>
        <location filename="compdefdialog.ui" line="919"/>
        <location filename="compdefdialog.ui" line="941"/>
        <location filename="compdefdialog.ui" line="976"/>
        <location filename="compdefdialog.ui" line="1033"/>
        <location filename="compdefdialog.ui" line="1087"/>
        <source>deviație standard</source>
        <translation>standard deviation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="954"/>
        <location filename="compdefdialog.ui" line="1119"/>
        <source>Intensitate</source>
        <translation>value</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1014"/>
        <location filename="compdefdialog.ui" line="1343"/>
        <source>Verde</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1052"/>
        <location filename="compdefdialog.ui" line="1170"/>
        <source>Albastru</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1100"/>
        <location filename="compdefdialog.ui" line="1271"/>
        <source>Roșu</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1157"/>
        <location filename="compdefdialog.ui" line="1249"/>
        <source>Saturație</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1185"/>
        <source>Activare validator</source>
        <translation>Enable validator</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1192"/>
        <source>Procent din dimensiunea componentei</source>
        <translation>percentage of component size</translation>
    </message>
    <message>
        <source>Aplică</source>
        <translation type="vanished">Apply</translation>
    </message>
    <message>
        <source>R</source>
        <translation type="vanished">R</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1274"/>
        <source>RVAL</source>
        <translation>RVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="922"/>
        <source>RDEV</source>
        <translation>RDEV</translation>
    </message>
    <message>
        <source>G</source>
        <translation type="vanished">G</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1346"/>
        <source>GVAL</source>
        <translation>GVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="979"/>
        <source>GDEV</source>
        <translation>GDEV</translation>
    </message>
    <message>
        <source>B</source>
        <translation type="vanished">B</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1055"/>
        <source>BVAL</source>
        <translation>BVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1090"/>
        <source>BDEV</source>
        <translation>BDEV</translation>
    </message>
    <message>
        <source>H</source>
        <translation type="vanished">H</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1141"/>
        <source>HVAL</source>
        <translation>HVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1036"/>
        <source>HDEV</source>
        <translation>HDEV</translation>
    </message>
    <message>
        <source>S</source>
        <translation type="vanished">S</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1160"/>
        <source>SVAL</source>
        <translation>SVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="887"/>
        <source>SDEV</source>
        <translation>SDEV</translation>
    </message>
    <message>
        <source>V</source>
        <translation type="vanished">V</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1122"/>
        <source>VVAL</source>
        <translation>VVAL</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="944"/>
        <source>VDEV</source>
        <translation>VDEV</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1676"/>
        <source>De&amp;numire Componenta</source>
        <translation>Component &amp;name</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="29"/>
        <source>Dublare re&amp;zolutie/interpolare</source>
        <translation>resolution doubling/interpolation</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1202"/>
        <source>Calculează</source>
        <translation>Calculate</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="696"/>
        <location filename="compdefdialog.ui" line="1388"/>
        <source>poziționare</source>
        <translation>location</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1420"/>
        <source>tree depth</source>
        <translation>tree depth</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1440"/>
        <source>nu</source>
        <translation>nu</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1466"/>
        <source>cascade depth</source>
        <translation>cascade depth</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1473"/>
        <source>recomandat 6-18</source>
        <translation>reccomended 6-18</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1489"/>
        <source>feature poolsize</source>
        <translation>feature poolsize</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1496"/>
        <source>viteză vs acuratețe</source>
        <translation>speed vs accuracy</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1515"/>
        <source>num test splits</source>
        <translation>num test splits</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1522"/>
        <source>viteză vs acuratețe la învățare</source>
        <translation>speed vs accuracy while training</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1541"/>
        <source>oversampling amount</source>
        <translation>oversampling amount</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1561"/>
        <source>oversampling translation jitter</source>
        <translation>oversampling translation jitter</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="326"/>
        <location filename="compdefdialog.ui" line="1586"/>
        <source>activare</source>
        <translation>enable</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1614"/>
        <source>ocupat</source>
        <translation>busy</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1632"/>
        <source>timeLabel</source>
        <translation>timeLabel</translation>
    </message>
    <message>
        <location filename="compdefdialog.ui" line="1650"/>
        <source>Memorie date folosita</source>
        <translation>used memory</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="317"/>
        <source>Nume fișier unde se va salva setul de date</source>
        <translation>dataset filename</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="318"/>
        <source>fișiere xml (*.xml);;Toate fișierele (*.*)</source>
        <translation>xml files (*.xml);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="681"/>
        <source>Nume fișier de încărcat</source>
        <translation>File to be loaded</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="682"/>
        <location filename="compdefdialog.cpp" line="693"/>
        <source>fișiere dat (*.dat);;Toate fișierele (*.*)</source>
        <translation>dat files (*.dat);;All files (*.*</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="686"/>
        <location filename="compdefdialog.cpp" line="697"/>
        <source>eroare</source>
        <translation>error</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="686"/>
        <source>Nu s-a putut încărca rețeaua neuronala din fișierul </source>
        <translation>Unable to load he neural netwok from he file </translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="692"/>
        <source>Nume fișier pentru salvare rețea neuronală</source>
        <translation>File name to save the neual network</translation>
    </message>
    <message>
        <location filename="compdefdialog.cpp" line="697"/>
        <source>Nu s-a putut salva rețeaua neuronala în fișierul </source>
        <translation>Unable to save the neural network to the file </translation>
    </message>
</context>
<context>
    <name>CompStatModel</name>
    <message>
        <location filename="compstatmodel.cpp" line="219"/>
        <source>corect</source>
        <translation>correct</translation>
    </message>
    <message>
        <location filename="compstatmodel.cpp" line="220"/>
        <source>orientare</source>
        <translation>orientation</translation>
    </message>
    <message>
        <location filename="compstatmodel.cpp" line="221"/>
        <source>suplimentar</source>
        <translation>extra</translation>
    </message>
    <message>
        <location filename="compstatmodel.cpp" line="222"/>
        <source>prag</source>
        <translation>threshold</translation>
    </message>
</context>
<context>
    <name>ComponentPathCorrection</name>
    <message>
        <location filename="componentpathcorrection.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="22"/>
        <source>Cale inițială</source>
        <translation>Initial path</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="36"/>
        <source>Cale nouă</source>
        <translation>New path</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="53"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="81"/>
        <source>Actualizează</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.ui" line="88"/>
        <source>Renunță</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="componentpathcorrection.cpp" line="83"/>
        <source>Fisiere Imagine(*.png *.jpg *.bmp *.jpeg);;Toate fișierele(*.*)</source>
        <translation>Image files(*.png *.jpg *.bmp *.jpeg);;All files(*.*)</translation>
    </message>
</context>
<context>
    <name>FileDeleter</name>
    <message>
        <location filename="filedeleter.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Form</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="20"/>
        <source>&amp;Director</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="33"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="47"/>
        <source>Șterge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filedeleter.ui" line="67"/>
        <source>Închide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filedeleter.cpp" line="45"/>
        <source>director de analizat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="36"/>
        <source>Numele programului de inspecție optică de încărcat</source>
        <translation>Name of the visual inspection program to be loaded</translation>
    </message>
    <message>
        <location filename="main.cpp" line="37"/>
        <source>Fișier bază de date sqlite de deschis</source>
        <translation>Sqlite database file to be opened</translation>
    </message>
    <message>
        <location filename="main.cpp" line="39"/>
        <source>Limbă folosită pentru interfața programului</source>
        <translation>User interface language</translation>
    </message>
</context>
<context>
    <name>TestInterface</name>
    <message>
        <location filename="testinterface.ui" line="14"/>
        <source>Alegeti placa de inspectat</source>
        <translation>select board to be inspected</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="380"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="669"/>
        <source>13</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="832"/>
        <source>19</source>
        <translation>19</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="333"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="713"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="908"/>
        <source>Placa</source>
        <translation>Board</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="785"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="647"/>
        <source>17</source>
        <translation>17</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="358"/>
        <source>23</source>
        <translation>23</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="898"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="854"/>
        <source>21</source>
        <translation>21</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="876"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="528"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="807"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="597"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="735"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="550"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="428"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="622"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="691"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="453"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="503"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="760"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="478"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="575"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="100"/>
        <source>editor plăci și componente</source>
        <translation>board and component editor</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="106"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="117"/>
        <source>încarcă imagine</source>
        <translation>Load image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="123"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="134"/>
        <source>salvează imagine</source>
        <translation>Save image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="140"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="151"/>
        <source>achiziționează imagine</source>
        <translation>acquire image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="157"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="171"/>
        <source>analiză imagine</source>
        <translation>analyze image</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="177"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="188"/>
        <source>alegere placă</source>
        <translation>select board</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="194"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="218"/>
        <source>PASS</source>
        <translation>PASS</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="238"/>
        <source>Serie</source>
        <translation>Serial number</translation>
    </message>
    <message>
        <location filename="testinterface.ui" line="261"/>
        <source>Execută test</source>
        <translation>Run test</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="68"/>
        <source>configurează camera</source>
        <oldsource>configureaza camera</oldsource>
        <translation>configure camera</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="154"/>
        <source>Fișiere Imagine (*.png *.jpg *.jpeg *.bmp);;Toate fișierele (*.*)</source>
        <translation>Image files(*.png *.jpg *.bmp *.jpeg);;All files(*.*)</translation>
    </message>
    <message>
        <source>Numele programului de inspecție optică de încărcat</source>
        <translation type="vanished">Name of the visual inspection program to be loaded</translation>
    </message>
    <message>
        <source>Fișier bază de date sqlite de deschis</source>
        <translation type="vanished">Sqlite database file to be opened</translation>
    </message>
    <message>
        <source>Limbă folosită pentru interfața programului</source>
        <translation type="vanished">User interface language</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="154"/>
        <source>Imagine</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="862"/>
        <source>nu e incărcat nici un program</source>
        <translation>no program loaded</translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="863"/>
        <source>dacă doriți să verificați plăci, incărcați întâi un program de inspecție</source>
        <translation></translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="983"/>
        <source>număr serie invalid, eroare </source>
        <translation>invalid serial number, error </translation>
    </message>
    <message>
        <location filename="testinterface.cpp" line="984"/>
        <source>număr serie invalid, rezultatul pentru placa nu se va salva</source>
        <translation>invalid serial number, board result will not be saved</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="widget.ui" line="20"/>
        <source>Widget</source>
        <translation>widget</translation>
    </message>
    <message>
        <location filename="widget.ui" line="93"/>
        <source>placa</source>
        <translation>board</translation>
    </message>
    <message>
        <location filename="widget.ui" line="144"/>
        <source>&amp;administrare</source>
        <translation>m&amp;anagement</translation>
    </message>
    <message>
        <location filename="widget.ui" line="172"/>
        <source>&amp;Sterge placa</source>
        <translation>&amp;Delete board</translation>
    </message>
    <message>
        <location filename="widget.ui" line="179"/>
        <source>&amp;Configurare</source>
        <translation>&amp;Configure</translation>
    </message>
    <message>
        <location filename="widget.ui" line="188"/>
        <source>&amp;Validator 
nr. serie</source>
        <translation>Serial number
&amp;Validator</translation>
    </message>
    <message>
        <location filename="widget.ui" line="205"/>
        <source>Expresie regulată pentru validarea numerelor de serie, cum ar fi \w{6}|(?i)TESTA</source>
        <translation>Regular expression for serial number validator, like \w{6}|(?i)TESTA</translation>
    </message>
    <message>
        <location filename="widget.ui" line="219"/>
        <source>&amp;Pornire
test</source>
        <translation>&amp;Start
test</translation>
    </message>
    <message>
        <location filename="widget.ui" line="231"/>
        <source>Imediat</source>
        <translation>Immediate</translation>
    </message>
    <message>
        <location filename="widget.ui" line="236"/>
        <source>Întârziat</source>
        <translation>Delayed</translation>
    </message>
    <message>
        <location filename="widget.ui" line="241"/>
        <source>Apăsare RUN</source>
        <translation>Press RUN</translation>
    </message>
    <message>
        <location filename="widget.ui" line="249"/>
        <source>&amp;Întârziere</source>
        <translation>&amp;Delay</translation>
    </message>
    <message>
        <location filename="widget.ui" line="290"/>
        <source>S&amp;alvare</source>
        <translation>S&amp;ave</translation>
    </message>
    <message>
        <location filename="widget.ui" line="303"/>
        <source>Placă &amp;nouă</source>
        <translation>&amp;New board</translation>
    </message>
    <message>
        <location filename="widget.ui" line="328"/>
        <source>&amp;referinta</source>
        <translation>&amp;reference</translation>
    </message>
    <message>
        <location filename="widget.ui" line="368"/>
        <source>&amp;Imagine</source>
        <translation>&amp;Image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="404"/>
        <source>&amp;Minima</source>
        <translation>&amp;Minumum</translation>
    </message>
    <message>
        <location filename="widget.ui" line="414"/>
        <source>Suprafata</source>
        <translation>Area</translation>
    </message>
    <message>
        <location filename="widget.ui" line="426"/>
        <location filename="widget.ui" line="1000"/>
        <location filename="widget.ui" line="1241"/>
        <source>Orientare</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="437"/>
        <location filename="widget.ui" line="1255"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="widget.ui" line="442"/>
        <location filename="widget.ui" line="1260"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="widget.ui" line="447"/>
        <location filename="widget.ui" line="1265"/>
        <location filename="widget.ui" line="1360"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="widget.ui" line="452"/>
        <location filename="widget.ui" line="1270"/>
        <location filename="widget.ui" line="1327"/>
        <source>V</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="widget.ui" line="462"/>
        <source>Ma&amp;xima</source>
        <translation>Ma&amp;ximum</translation>
    </message>
    <message>
        <location filename="widget.ui" line="472"/>
        <source>NumeImagine</source>
        <translation>ImageName</translation>
    </message>
    <message>
        <location filename="widget.ui" line="479"/>
        <source>Marcată</source>
        <translation>Current</translation>
    </message>
    <message>
        <location filename="widget.ui" line="508"/>
        <source>&amp;tip comp. </source>
        <translation>component &amp;type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="529"/>
        <source>Tip &amp;nou</source>
        <translation>&amp;new type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="539"/>
        <source>&amp;Sterge</source>
        <translation>&amp;Delete</translation>
    </message>
    <message>
        <location filename="widget.ui" line="546"/>
        <source>&amp;Nume Tip Componenta</source>
        <translation>Component type &amp;name</translation>
    </message>
    <message>
        <location filename="widget.ui" line="564"/>
        <source>C&amp;lasa</source>
        <translation>C&amp;lass</translation>
    </message>
    <message>
        <location filename="widget.ui" line="595"/>
        <source>Alternativa</source>
        <translation>Alternative</translation>
    </message>
    <message>
        <location filename="widget.ui" line="616"/>
        <source>&amp;Adauga</source>
        <translation>&amp;Add</translation>
    </message>
    <message>
        <location filename="widget.ui" line="626"/>
        <location filename="widget.ui" line="806"/>
        <source>Ster&amp;ge</source>
        <translation>&amp;Delete</translation>
    </message>
    <message>
        <location filename="widget.ui" line="651"/>
        <source>&amp;componente</source>
        <translation>&amp;components</translation>
    </message>
    <message>
        <location filename="widget.ui" line="657"/>
        <source>&amp;Nume</source>
        <translation>&amp;Name</translation>
    </message>
    <message>
        <location filename="widget.ui" line="683"/>
        <source>Tol. &amp;X</source>
        <translation>&amp;X tol.</translation>
    </message>
    <message>
        <location filename="widget.ui" line="699"/>
        <source>Toleranta pozitie orizontala, in procente</source>
        <translation>Horizontal position tollerance as percentage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="712"/>
        <source>Tol. &amp;Y</source>
        <translation>&amp;Y tol.</translation>
    </message>
    <message>
        <location filename="widget.ui" line="728"/>
        <source>Toleranta pozitie verticala, in procente</source>
        <translation>Vertical position tollerance as percentage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="741"/>
        <source>Tol. &amp;Scala</source>
        <translation>&amp;scale tol.</translation>
    </message>
    <message>
        <location filename="widget.ui" line="757"/>
        <source>Toleranta dimensiune, in procente</source>
        <translation>Scale tollerance, as percentage</translation>
    </message>
    <message>
        <location filename="widget.ui" line="770"/>
        <source>&amp;Orientare</source>
        <translation>&amp;Orientation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="809"/>
        <location filename="widget.ui" line="1194"/>
        <source>Del, Backspace</source>
        <translation>Del, Backspace</translation>
    </message>
    <message>
        <location filename="widget.ui" line="819"/>
        <source>Tip cod
bare</source>
        <translation>barcode
type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="838"/>
        <source>componenta</source>
        <translation>component</translation>
    </message>
    <message>
        <location filename="widget.ui" line="841"/>
        <source>Dublu click pentru analiză utilizare imagini</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="widget.ui" line="886"/>
        <source>tip componenta</source>
        <translation>component type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="916"/>
        <source>Componentă nouă</source>
        <translation>new component</translation>
    </message>
    <message>
        <location filename="widget.ui" line="942"/>
        <source>Sterge componenta</source>
        <translation>Delete component</translation>
    </message>
    <message>
        <location filename="widget.ui" line="949"/>
        <source>Configurare</source>
        <translation>Configure</translation>
    </message>
    <message>
        <location filename="widget.ui" line="964"/>
        <source>aspect ratio</source>
        <translation>aspect ratio</translation>
    </message>
    <message>
        <location filename="widget.ui" line="977"/>
        <source>Raport latime/inaltime</source>
        <translation>width/height ratio</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1028"/>
        <source>Salvează</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1050"/>
        <source>varianta</source>
        <translation>items</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1076"/>
        <source>Tip marcaj</source>
        <translation>Marking type</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1084"/>
        <location filename="widget.cpp" line="91"/>
        <source>Pozitiv</source>
        <translation>Positive</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1089"/>
        <location filename="widget.cpp" line="92"/>
        <source>Negativ</source>
        <translation>Negative</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1094"/>
        <location filename="widget.cpp" line="93"/>
        <source>Ignorare</source>
        <translation>Ignore</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1110"/>
        <source>debifeaza pentru incercare clasificator</source>
        <translation>unselect to try the classifier</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1116"/>
        <location filename="widget.cpp" line="689"/>
        <source>Imagine</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1149"/>
        <source>Sterge imaginea</source>
        <translation>Delete image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1152"/>
        <location filename="widget.ui" line="1191"/>
        <source>Sterge</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1159"/>
        <source>Adauga o imagine noua</source>
        <translation>Add a new image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1162"/>
        <source>Adauga</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1169"/>
        <source>Intreaga imagine e folosita ca exemplu negativ</source>
        <translation>The entire image can be used to extract negative samples</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1178"/>
        <source>implicit negativ</source>
        <translation>implicit negative</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1188"/>
        <source>Sterge marcajul curent</source>
        <translation>Delete current marking</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1301"/>
        <source>albastru</source>
        <translation>blue</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1304"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1311"/>
        <source>verde</source>
        <translation>green</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1317"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1324"/>
        <source>intensitate</source>
        <translation>intensity</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1334"/>
        <source>roșu</source>
        <translation>red</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1340"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1347"/>
        <source>nunșă</source>
        <translation>hue</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1350"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1357"/>
        <source>saturație</source>
        <translation>saturation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1458"/>
        <source>Analizează</source>
        <translation>Analyze</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1465"/>
        <source>Caută în imagine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1507"/>
        <source>corect</source>
        <translation>correct</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1514"/>
        <source>orientare</source>
        <translation>orientation</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1521"/>
        <source>suplimentar</source>
        <translation>extra</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1528"/>
        <source>lipsă</source>
        <translation>missing</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1535"/>
        <source>imagine</source>
        <translation>image</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1542"/>
        <source>toate</source>
        <translation>all</translation>
    </message>
    <message>
        <location filename="widget.ui" line="1549"/>
        <location filename="widget.ui" line="1556"/>
        <location filename="widget.ui" line="1563"/>
        <location filename="widget.ui" line="1570"/>
        <location filename="widget.ui" line="1577"/>
        <location filename="widget.ui" line="1584"/>
        <location filename="widget.ui" line="1591"/>
        <location filename="widget.ui" line="1598"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="56"/>
        <source>copiază placă</source>
        <translation>copy board</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="61"/>
        <source>copiază componenta</source>
        <translation>copy component</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="86"/>
        <source>indiferent</source>
        <translation>indifferent</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="87"/>
        <source>N-S,E-V</source>
        <translation>N-S,E-W</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="88"/>
        <source>N,E,S,V</source>
        <translation>N,E,S,W</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="96"/>
        <source>Componenta</source>
        <translation>Component</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="97"/>
        <source>Fiducial</source>
        <translation>Fiducial</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="99"/>
        <source>Cod Bare</source>
        <translation>Barcode</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="357"/>
        <location filename="widget.cpp" line="998"/>
        <location filename="widget.cpp" line="1017"/>
        <source>componenta: </source>
        <translation>Component:</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="366"/>
        <location filename="widget.cpp" line="1014"/>
        <source>placa: </source>
        <translation>Board:</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="456"/>
        <location filename="widget.cpp" line="480"/>
        <source>avertisment salvare definitie</source>
        <translation>component save warning
</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="457"/>
        <source>Definiția a fost modificată. Doriți salvarea acesteia?</source>
        <translation>The component definition has been changed. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="481"/>
        <source>Definitia a fost modificata. Doriti salvarea acesteia?</source>
        <translation>The component definition has been changed. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="691"/>
        <source>Fisiere Imagine(*.png *.jpg *.bmp *.jpeg);;Toate fișierele(*.*)</source>
        <translation>Image files (*.png *.jpg *.bmp *.jpeg);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1087"/>
        <source>Fisiere Imagine (*.png *.jpg *.bmp *.jpeg);;toate fișierele (*.*)</source>
        <translation>Image files (*.png *.jpg *.bmp *.jpeg);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1123"/>
        <source>Ștergere componentă</source>
        <translation>Delete component
</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1123"/>
        <location filename="widget.cpp" line="1452"/>
        <source>Sunteți sigur că doriți să ștergeți tipul de componentă?</source>
        <translation>Are you sure you want to delete the component type?</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1129"/>
        <source>Ștergere eșuată</source>
        <translation>Deletion failed
</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1129"/>
        <source>nu s-a putut șterge componenta!</source>
        <translation>the component could not be deleted!</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1387"/>
        <source>Duplicat</source>
        <translation>Duplicate</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1387"/>
        <source>Adăugare ignorată, deoarece s-a folosit deja această alternativă</source>
        <translation>Failed to add, because the alternative has already been used</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1452"/>
        <source>Ștergere tip componentă</source>
        <translation>Delete component type</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1475"/>
        <source>Ștergere placă</source>
        <translation>Delete board</translation>
    </message>
    <message>
        <location filename="widget.cpp" line="1475"/>
        <source>Sunteți sigur că doriți să ștergeți placa?</source>
        <translation>Are you sure you want to delete the board?</translation>
    </message>
</context>
</TS>
