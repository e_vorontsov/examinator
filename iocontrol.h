#ifndef IOCONTROL_H
#define IOCONTROL_H

#include <QSerialPort>

class IOControl : public QObject
{
    Q_OBJECT
public:
    explicit IOControl(QObject *parent = nullptr);
    bool setPort(QString portName);
    bool setLights(QByteArray lights);//receives 3 bytes, which should consist of 3 digits, zero meaning off.
    QByteArray getLights();
private:
    QSerialPort port;
    QByteArray readBuffer;

signals:
    void start(void);
    void newData(QByteArray data);
public slots:
    void readData();
};

#endif // IOCONTROL_H
