/*
Copyright 2018, 2019 Günter Neustädter

This file is part of examinator.

Examinator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mycamera.h"
#include <assert.h>
#include <QDir>
#include <QDateTime>
#include <QSettings>
#include <QApplication>

MyCamera::MyCamera(QObject *parent) : QObject(parent)
{
    QDir dir;
    dir.mkpath("/tmp/images");
    QSettings settings(QSettings::IniFormat,QSettings::UserScope, qApp->organizationName(),qApp->applicationName());
    //if(settings.value("cameratype").toString()=="qcamera")
    deviceName=settings.value("qcamera").toString();
    if(!getAvailableCameras().contains(deviceName))
        deviceName=QCameraInfo::defaultCamera().deviceName();
    qDebug()<<"MyCamera"<<deviceName<<getAvailableCameras();
    this->setCamera(deviceName.toLatin1());
    if(!imageCapture){
        deviceName="";
        settings.setValue("qcamera","");
        return;
    }
    if(settings.contains("qcameraresolution")){
        QSize r=settings.value("qcameraresolution").toSize();
        setResolution(r);
    }
    nImages=settings.value("qnimages",1).toInt();
}

QList<QString> MyCamera::getAvailableCameras()
{
    QList<QCameraInfo> cameras=QCameraInfo::availableCameras();
    qDebug()<<cameras;
    QList<QString> lst;
    foreach (auto info, cameras) {
       lst.append(info.deviceName());
    }
    if((!deviceName.isEmpty())&&(camera!=nullptr)){
        if(!lst.contains(deviceName)){
            lst.append(deviceName);
        }
    }
    return lst;
}

QList<QSize> MyCamera::getResolutions()
{
    qDebug()<<"resolutions";
    QList<QSize> resolutions;
    if((!camera)&&(!deviceName.isEmpty()))
        //enable camera
        setCamera("",true);
    if(!camera)
        return resolutions;
    assert(imageCapture!=nullptr);
    //qDebug()<<"CaptureToBuffer"<<imageCapture->isCaptureDestinationSupported(QCameraImageCapture::CaptureToBuffer);
    resolutions = imageCapture->supportedResolutions();
    qDebug()<<resolutions<<imageCapture<<camera->status()<<camera->state();
    return resolutions;
}

bool MyCamera::setResolution(QSize size)
{
    if((!camera)&&(!deviceName.isEmpty()))
        //enable camera
        setCamera("",true);
    if(!imageCapture)
        return false;
    QImageEncoderSettings imageSettings=imageCapture->encodingSettings();
    qDebug()<<imageSettings.codec()<<imageSettings.resolution();
    imageSettings.setResolution(size);
    imageCapture->setEncodingSettings(imageSettings);
    imageSettings=imageCapture->encodingSettings();
    qDebug()<<imageSettings.resolution();
    return imageSettings.resolution()==size;
}

QSize MyCamera::resolution()
{
    if((!camera)&&(!deviceName.isEmpty()))
        //enable camera
        setCamera("",true);
    if(!imageCapture)
        return QSize();
    QImageEncoderSettings imageSettings=imageCapture->encodingSettings();
    return imageSettings.resolution();
}

int MyCamera::getNImages()
{
    return nImages;
}

QString MyCamera::getDeviceName()
{
    return deviceName;
}

void MyCamera::setCamera(QString deviceName, bool enabled)
{
    if(!deviceName.isEmpty())
        this->deviceName=deviceName;
    if(camera)
        delete camera;
    if(imageCapture){
        delete imageCapture;
        imageCapture=nullptr;
    }
    if((!enabled)/*||(writableSettings->value("cameratype").toString()!="qcamera")*/){
        //only camera stop requested
        camera=nullptr;
        return;
    }
    if(this->deviceName.isEmpty()){
        qDebug()<<"no device name provided";
        camera=nullptr;
        return;
    }
    camera = new QCamera(this->deviceName.toLatin1());
    if(!camera){
        qDebug()<<"unable to open camera"<<this->deviceName;
        return;
    }
    if(!camera->isAvailable()){
        qDebug()<<"unable to open camera"<<this->deviceName;
        delete camera;
        camera=nullptr;
        return;
    }
    camera->load();
    camera->setCaptureMode(QCamera::CaptureStillImage);
    imageCapture = new QCameraImageCapture(camera);
    imageCapture->setCaptureDestination(QCameraImageCapture::CaptureToBuffer);
    connect(imageCapture,SIGNAL(readyForCaptureChanged(bool)), this, SLOT(cameraIsReady(bool)));
    connect(imageCapture,SIGNAL(imageCaptured(int,QImage)),this,SLOT(getImage(int,QImage)));
    camera->start();
}

void MyCamera::setNImages(int n)
{
    nImages=std::min(std::max(n,1),MAXNIMAGES);
}

void MyCamera::capture()
{
    if(camera==nullptr){
        //camera not initialized
        setCamera("");
    }
    if(imageCapture){
        qDebug()<<"isreadyforcapture"<<imageCapture->isReadyForCapture();
        if(camera->status()!=QCamera::ActiveStatus){
            pictureRequested=true;
            qDebug()<<"retry capture";
        }else {
            qDebug()<<"capture";
            //capture writes to file. There is no known way to prevent it
            QDir dir("/tmp/images/");
            dir.setNameFilters(QStringList() << "*");
            dir.setFilter(QDir::Files);
            foreach (QString entry, dir.entryList()) {
                dir.remove(entry);
            }
            imageCapture->capture("/tmp/images/");
        }
    } else {
        qDebug()<<"imageCapture isnull"<<imageCapture<<camera<<deviceName;
    }

}

void MyCamera::cameraIsReady(bool ready)
{
    qDebug()<<"statusChanged"<<ready<<pictureRequested;
    if(!pictureRequested)
        return;
    if(ready){
        capture();
    }
}
void MyCamera::getImage(int id, const QImage &img)
{
    Q_UNUSED(id);
    qDebug()<<"captured"<<img.width()<<img.height()<<img.depth()<<img.byteCount()<<img.bytesPerLine()<<
              QDateTime::currentDateTime();
    //QImage i2=img.convertToFormat(QImage::Format_RGB888);
    //qDebug()<<i2.width()<<i2.height()<<i2.depth()<<i2.byteCount()<<i2.bytesPerLine();
    //i2.save("i2.png");
    emit imageCaptured(QPixmap::fromImage(img));
    qDebug()<<QDateTime::currentDateTime();
    pictureRequested=false;
}
